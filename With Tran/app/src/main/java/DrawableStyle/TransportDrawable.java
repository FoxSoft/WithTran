package DrawableStyle;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;

import com.app.nosenko.withtran.R;
import com.baoyz.widget.PullRefreshLayout;
import com.baoyz.widget.RefreshDrawable;

import java.util.Random;

public class TransportDrawable extends RefreshDrawable {
    private static final int MAX_LEVEL = 200;

    private int rangePos = 0;

    private boolean isRunning;
    private RectF mBounds;
    private int mWidth;
    private int mHeight;
    private int mTop;
    private int mOffsetTop;
    private Paint mPaint;
    private float mAngle;
    private int[] mColorSchemeColors = new int[]{R.color.red_old, R.color.yellow_old, R.color.green_old, R.color.yellow_old, R.color.green_old};
    private int[] drawable = new int[]{R.drawable.surban, R.drawable.elka, R.drawable.icon, R.drawable.train, R.drawable.tram};
    private Handler mHandler = new Handler();
    private int mLevel;
    private Bitmap bitmap;
    private Context context;
    private PorterDuffColorFilter filter;
    private Display display;
    private int idColor = 0;

    public TransportDrawable(Context context, PullRefreshLayout layout, Display display) {
        super(context, layout);
        this.context = context;
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.RED);
        this.display = display;
    }

    @Override
    public void setPercent(float percent) {

    }

    @Override
    public void setColorSchemeColors(int[] colorSchemeColors) {

    }

    @Override
    public void offsetTopAndBottom(int offset) {
        mTop += offset;
        mOffsetTop += offset;
        float offsetTop = mOffsetTop;
        if (mOffsetTop > getRefreshLayout().getFinalOffset()) {
            offsetTop = getRefreshLayout().getFinalOffset();
        }
        mAngle = 360 * (offsetTop / getRefreshLayout().getFinalOffset());
        invalidateSelf();
    }

    @Override
    public void start() {
        isRunning = true;
        mHandler.post(mAnimationTask);
    }

    private Runnable mAnimationTask = new Runnable() {
        @Override
        public void run() {
            if (isRunning()) {
                idColor = getNextOfRange(0, mColorSchemeColors.length);
                updateLevel(getNextOfRange(0, drawable.length));

                invalidateSelf();
                mHandler.postDelayed(this, 500);
            }
        }
    };

    private void updateLevel(int level) {
        bitmap = BitmapFactory.decodeResource(context.getResources(), drawable[level]);
       filter = new PorterDuffColorFilter(context.getResources().getColor(mColorSchemeColors[level]), PorterDuff.Mode.SRC_IN);

        mPaint.setShadowLayer(170, 10, 5, R.color.red);
    }

    @Override
    public void stop() {
        isRunning = false;
        mHandler.removeCallbacks(mAnimationTask);
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        mWidth = dp2px(120);
        mHeight = mWidth;
        mBounds = new RectF(bounds.width() / 2 - mWidth / 2, bounds.top + 10, bounds.width() / 2 + mWidth / 2, bounds.top + mHeight + 50);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        drawRing2(canvas);
        canvas.restore();
    }

    private void drawRing(Canvas canvas) {
        bitmap = BitmapFactory.decodeResource(context.getResources(), drawable[rangePos]);
        mPaint = new Paint();

        filter = new PorterDuffColorFilter(ContextCompat.getColor(context, R.color.red), PorterDuff.Mode.SRC_IN);
        mPaint.setColorFilter(filter);

        //canvas.drawBitmap(bitmap, display.getWidth() / 2 - bitmap.getWidth() / 2, 50, mPaint);
        canvas.drawBitmap(bitmap, null, new Rect(display.getWidth()/2 - bitmap.getWidth(), 5, bitmap.getWidth() + display.getWidth()/2, bitmap.getHeight() + 50), mPaint);

    }

    private void drawRing2(Canvas canvas) {
        bitmap = BitmapFactory.decodeResource(context.getResources(), drawable[rangePos]);

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

// Половинки
        int halfWidth = width - 10;
        int halfHeight = height - 10;

// Выводим уменьшенную в два раза картинку во втором ImageView
        Bitmap bmHalf = Bitmap.createScaledBitmap(bitmap, halfWidth, halfHeight, false);

        mPaint = new Paint();

        filter = new PorterDuffColorFilter(context.getResources().getColor(mColorSchemeColors[idColor]), PorterDuff.Mode.SRC_IN);
        mPaint.setColorFilter(filter);
        mPaint.setShadowLayer(170, 10, 5, R.color.red);
        canvas.drawBitmap(bmHalf, display.getWidth() / 2 - bmHalf.getWidth() / 2, 5, mPaint);

    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getContext().getResources().getDisplayMetrics());
    }

    private int getRandIntRange(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    private int getNextOfRange(int min, int max) {
        for (int i = min; i < max; i++) {

            if (rangePos == i) {

                rangePos++;
                if (rangePos == max) {
                    rangePos = 0;
                }
                return rangePos;
            }
        }
        return rangePos;
    }
}
