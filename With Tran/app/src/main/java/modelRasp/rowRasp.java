package modelRasp;

/**
 * Created by eminem on 22.11.2017.
 */

public class rowRasp {
    private String nameRegion;
    private String codeRegion;
    private String nameCity;
    private String codeCity;
    private String nameStation;
    private String codeStation;
    private String typeStation;
    private String typeTransport;

    public rowRasp() {
    }

    public rowRasp(String nameRegion, String codeRegion, String nameCity,
                   String codeCity, String nameStation, String codeStation, String typeStation, String typeTransport) {
        this.nameRegion = nameRegion;
        this.codeRegion = codeRegion;
        this.nameCity = nameCity;
        this.codeCity = codeCity;
        this.nameStation = nameStation;
        this.codeStation = codeStation;
        this.typeStation = typeStation;
        this.typeTransport = typeTransport;
    }

    public String getNameRegion() {
        return nameRegion;
    }

    @Override
    public String toString() {
        return "rowRasp{" +
                "nameRegion='" + nameRegion + '\'' +
                ", codeRegion='" + codeRegion + '\'' +
                ", nameCity='" + nameCity + '\'' +
                ", codeCity='" + codeCity + '\'' +
                ", nameStation='" + nameStation + '\'' +
                ", codeStation='" + codeStation + '\'' +
                ", typeStation='" + typeStation + '\'' +
                ", typeTransport='" + typeTransport + '\'' +
                '}';
    }

    public void setNameRegion(String nameRegion) {
        this.nameRegion = nameRegion;
    }

    public String getCodeRegion() {
        return codeRegion;
    }

    public void setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
    }

    public String getNameCity() {
        return nameCity;
    }

    public void setNameCity(String nameCity) {
        this.nameCity = nameCity;
    }

    public String getCodeCity() {
        return codeCity;
    }

    public void setCodeCity(String codeCity) {
        this.codeCity = codeCity;
    }

    public String getNameStation() {
        return nameStation;
    }

    public void setNameStation(String nameStation) {
        this.nameStation = nameStation;
    }

    public String getCodeStation() {
        return codeStation;
    }

    public void setCodeStation(String codeStation) {
        this.codeStation = codeStation;
    }

    public String getTypeStation() {
        return typeStation;
    }

    public void setTypeStation(String typeStation) {
        this.typeStation = typeStation;
    }

    public String getTypeTransport() {
        return typeTransport;
    }

    public void setTypeTransport(String typeTransport) {
        this.typeTransport = typeTransport;
    }
}
