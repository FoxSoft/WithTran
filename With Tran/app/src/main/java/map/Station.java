package map;

import java.util.ArrayList;
import java.util.List;

public class Station {

    private Pagination pagination = new Pagination();
    private List<Stations> stations = new ArrayList<>();

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<Stations> getStations() {
        return stations;
    }

    public void setStations(List<Stations> stations) {
        this.stations = stations;
    }

    public static class Pagination {
        private int total;
        private int limit;
        private int offset;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }
    }

    public static class Stations {
        private double distance;
        private String code;
        private String station_type;
        private TypeChoices type_choices;
        private String title;
        private String popular_title;
        private String short_title;
        private int majority;
        private String transport_type;
        private double lat;
        private double lng;
        private String type;
        private String station_type_name;

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getStation_type() {
            return station_type;
        }

        public void setStation_type(String station_type) {
            this.station_type = station_type;
        }

        public TypeChoices getType_choices() {
            return type_choices;
        }

        public void setType_choices(TypeChoices type_choices) {
            this.type_choices = type_choices;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPopular_title() {
            return popular_title;
        }

        public void setPopular_title(String popular_title) {
            this.popular_title = popular_title;
        }

        public String getShort_title() {
            return short_title;
        }

        public void setShort_title(String short_title) {
            this.short_title = short_title;
        }

        public int getMajority() {
            return majority;
        }

        public void setMajority(int majority) {
            this.majority = majority;
        }

        public String getTransport_type() {
            return transport_type;
        }

        public void setTransport_type(String transport_type) {
            this.transport_type = transport_type;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStation_type_name() {
            return station_type_name;
        }

        public void setStation_type_name(String station_type_name) {
            this.station_type_name = station_type_name;
        }

        public static class TypeChoices {
            private Schedule schedule;

            public Schedule getSchedule() {
                return schedule;
            }

            public void setSchedule(Schedule schedule) {
                this.schedule = schedule;
            }

            public static class Schedule {
                private String desktop_url;
                private String touch_url;

                public String getDesktop_url() {
                    return desktop_url;
                }

                public void setDesktop_url(String desktop_url) {
                    this.desktop_url = desktop_url;
                }

                public String getTouch_url() {
                    return touch_url;
                }

                public void setTouch_url(String touch_url) {
                    this.touch_url = touch_url;
                }
            }
        }
    }
}
