package helper;

import android.app.Application;

import com.app.nosenko.withtran.R;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(mailTo = "pitbullstar96@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_message)
public class ReportError extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
    }
}
