package helper;

import com.app.nosenko.withtran.Format;
import com.app.nosenko.withtran.Yandex;

public class Sorting {

    public static int sort(int t1, int t2, boolean reverse)
    {
        int result = 0;
        if (reverse == true) {
            if (t1 == t2) result = 0;
            if (t1 < t2) result = -1;
            if (t1 > t2) result = 1;
        }

        if (reverse == false) {
            if (t1 == t2) result = 0;
            if (t1 > t2) result = -1;
            if (t1 < t2) result = 1;
        }
        return result;
    }

    public static int sort(String t1, String t2, boolean reverse)
    {
        int result = 0;
        if (reverse == true) {
            if (t1 == t2) result = 0;
            if (Integer.valueOf(t1) < Integer.valueOf(t2)) result = -1;
            if (Integer.valueOf(t1) > Integer.valueOf(t2)) result = 1;
        }

//        if (reverse == false) {
//            if (t1 == t2) result = 0;
//            if (t1 > t2) result = -1;
//            if (t1 < t2) result = 1;
//        }
        return result;
    }

    public static int getStatusLevel(Yandex.SearchAPI.Segments segments)
    {
        int count = 0;

        count += segments.getTransportView().getStatus().startsWith("отправка") ? 3:0;
        count += segments.getTransportView().isTrack_visible() ? 2:0;
        return count;
    }

    public static int getDeparture(Yandex.SearchAPI.Segments segments)
    {
        return Format.getSecFromTime(segments.getDeparture());
    }

    public static int getDuration(Yandex.SearchAPI.Segments segments)
    {
        return Format.FormatDurationToInt(segments.getDuration());
    }

}
