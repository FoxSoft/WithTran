package com.app.nosenko.withtran;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.bigkoo.svprogresshud.listener.OnDismissListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.polyak.iconswitch.IconSwitch;
import com.sdsmdg.tastytoast.TastyToast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilderFactory;

import Adapter.AdapterHuntStationCityAutoComplete;
import Adapter.AdapterListSearch;
import Adapter.AdapterTypeTransport;
import Model.History;
import Model.StationDB;
import Model.TypeTransport;
import helper.Sorting;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ActivityMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    public DBAdapter dbAdapter;
    private AlertDialog.Builder dialog;
    private AutoCompleteTextView editTextFrom;
    private AutoCompleteTextView editTextTo;
    public RecyclerView recycler;
    public String searchReys = null;
    private String from;
    private String to;
    public String searchReysReverse = null;
    private SVProgressHUD mSVProgressHUD;
    private IconSwitch iconSwitch;
    private AdapterHuntStationCityAutoComplete stationAutoComplete;
    private final String TAG = "ActivityMain";
    private StationDB stationDBFrom;
    private StationDB stationDBTo;
    private Download download;
    //--------------------------------
    private Yandex.TRANSPORT_TYPE transportType = null;
    private boolean isTransfers = false;

    private AdapterListSearch adapterListSearch;
    public Yandex.SearchAPI searchAPI;
    private Download d;
    private boolean isCancel = false;
    private AdView adView;
    private AdRequest adRequest;
    private RelativeLayout relativeLayout;
    private boolean isPriview = false;
    private View priview;
    private int positionItemRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        HelperBackground.setTheme(this);

        super.onCreate(savedInstanceState);
        // getWindow().setBackgroundDrawable(null);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // HelperBackground.setStyle(this, findViewById(R.id.app_bar));
        // HelperBackground.setTheme(this);

        dbAdapter = new DBAdapter(this);

        final Bitmap original = BitmapFactory.decodeResource(getResources(), R.drawable.clear);
        final Bitmap b = Bitmap.createScaledBitmap(original, 40, 28, false);
        final Drawable x = new BitmapDrawable(getResources(), b);
        x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());

        editTextFrom = findViewById(R.id.fromStationEdit);
        editTextFrom.setCompoundDrawables(null, null, editTextFrom.getText().toString().equals("") ? null : x, null);
        editTextFrom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (editTextFrom.getCompoundDrawables()[2] == null) {
                    return false;
                }
                if (event.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }
                if (event.getX() > editTextFrom.getWidth() - editTextFrom.getPaddingRight() - x.getIntrinsicWidth()) {
                    editTextFrom.setText("");
                    editTextFrom.setCompoundDrawables(null, null, null, null);
                }
                return false;
            }
        });
        editTextFrom.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextFrom.setCompoundDrawables(null, null, editTextFrom.getText().toString().equals("") ? null : x, null);
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        editTextTo = findViewById(R.id.toStationEdit);
        editTextTo.setCompoundDrawables(null, null, editTextTo.getText().toString().equals("") ? null : x, null);
        editTextTo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (editTextTo.getCompoundDrawables()[2] == null) {
                    return false;
                }
                if (event.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }
                if (event.getX() > editTextTo.getWidth() - editTextFrom.getPaddingRight() - x.getIntrinsicWidth()) {
                    editTextTo.setText("");
                    editTextTo.setCompoundDrawables(null, null, null, null);
                }
                return false;
            }
        });
        editTextTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextTo.setCompoundDrawables(null, null, editTextTo.getText().toString().equals("") ? null : x, null);
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });
        iconSwitch = findViewById(R.id.iconSwitch);
        setIconSwitch();
        creteDialog();

        stationDBFrom = new StationDB();
        stationDBTo = new StationDB();

        mSVProgressHUD = new SVProgressHUD(this);
        mSVProgressHUD.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(SVProgressHUD hud) {

            }
        });

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                boolean inputFrom = !editTextFrom.getText().toString().equals("");
                boolean inputTo = !editTextTo.getText().toString().equals("");

                if ((inputFrom & inputTo)) {
//                    searchReys = editTextFrom.getText().toString() + "_" + stationDBTo.getNameStation();
//                    searchReysReverse = stationDBTo.getNameStation() + "_" + editTextFrom.getText().toString();

                    searchReys = stationDBFrom.getNameStation() + "_" + editTextTo.getText().toString();
                    searchReysReverse = editTextTo.getText().toString() + "_" + stationDBFrom.getNameStation();

                    if (dbAdapter.hashTable(searchReys)) {
                        dialog.show();
                    } else {
                        if (isOnline3()) {
                            Log.d("SEARCHB", "=================");


                            String fromCode = stationDBFrom.getCodeStation();
                            String toCode = stationDBTo.getCodeStation();
                            Log.d("SEARCHB", fromCode);
                            Log.d("SEARCHB", toCode);

                            inputFrom = !fromCode.equals("");
                            inputTo = !toCode.equals("");
                            if ((inputFrom & inputTo)) {

                                if (isPriview)
                                {
                                    relativeLayout.removeView(priview);
                                }
                                searchAPI.clearAll();
                                download = new Download(getApplicationContext(), fromCode, toCode, transportType, false, Yandex.SEARCH_RASP.from_to);
                                download.execute();

                                // RefrashTimeV2();

                                Download dBack = new Download(getApplicationContext(), toCode, fromCode, transportType, false, Yandex.SEARCH_RASP.to_from);
                                dBack.execute();

                                // Log.d("search__", download.searchAPI.getSegments().get(0).toString());

                                mSVProgressHUD.showSuccessWithStatus("Загружено!");

                                Log.d("search", fromCode + "  " + toCode);

                            } else {

                                mSVProgressHUD.showInfoWithStatus("Упс... Возможно, вы допустили ошибку. Если все верно, тогда сорри. Такого города нет в базе...");
                                //Snackbar.make(view, "Упс... Возможно, вы допустили ошибку. Если все верно, тогда сорри. Такого города нет в базе...", 0).setAction("Action", null).show();
                            }
                        } else {
                            mSVProgressHUD.showInfoWithStatus("нет интернета...");
                        }
                    }
                } else {
                    mSVProgressHUD.showErrorWithStatus("Заполните все поля ...");
                }


            }

        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        recycler = findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        recycler.setLayoutManager(layoutManager);
        recycler.setNestedScrollingEnabled(false);

        createEdits();

        searchAPI = new Yandex.SearchAPI();
        adapterListSearch = new AdapterListSearch(getApplicationContext(), searchAPI);
        recycler.setAdapter(adapterListSearch);

        loadHistory();

        MobileAds.initialize(this, getResources().getString(R.string.YOUR_ADMOB_APP_ID));
        adView = findViewById(R.id.adView);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdClosed() {
                Log.d("admob", "закрыто");
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                adView.setTag(false);
                Log.d("admob","ошибка: "+i);
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                adView.setTag(true);

                Log.d("admob", "окончание загрузки");
                super.onAdLoaded();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
            }
        });


        adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("2276B596B2758694F89FA2AACDB23424").build();
        adView.loadAd(adRequest);
    }



    public class BannerExample extends AdListener {

        @Override
        public void onAdClicked() {
            super.onAdClicked();
            Log.d("banner", adRequest.getContentUrl());
        }

        @Override
        public void onAdOpened() {
            super.onAdOpened();
            Log.d("banner", adRequest.getContentUrl());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adView.resume();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateView();
                Log.d("update_rasp", "yes");
            }
        });
    }

    @Override
    protected void onDestroy() {
        adView.destroy();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        adView.pause();
        super.onPause();
    }

    public void RefrashTimeV2() {
//        Timer t = new Timer();
//        final Status status = new Status();
//        t.scheduleAtFixedRate(new TimerTask() {
//            public void run() {
//                ActivityMain.this.runOnUiThread(new Runnable() {
//                    public void run() {
//                        for (int i = 0; i < searchAPI.getSegments().size(); i++) {
//                            searchAPI.getSegments().get(i).setStatus(status.statusBusV2(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getThread().getTransportType()));
//                            searchAPI.getSegments().get(i).setIdImg(status.idImg);
//                            searchAPI.getSegments().get(i).setPosition(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival()));
//                            Log.d("PROVERKA_POS", String.valueOf(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival())));
//                            searchAPI.getSegments().get(i).setTrack_max(status.max);
//                            Log.d("PROVERKA", String.valueOf(status.max));
//                            searchAPI.getSegments().get(i).setTrack_visible(status.send);
//                        }
//                        adapterListSearch.notifyDataSetChanged();
//                        recycler.setAdapter(adapterListSearch);
//                    }
//                });
//            }
//        }, 0, 30000);
        Timer t = new Timer();
        final Status status = new Status();
        t.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                ActivityMain.this.runOnUiThread(new Runnable() {
                    public void run() {
                        for (int i = 0; i < searchAPI.getSegments().size(); i++) {
                            searchAPI.getSegments().get(i).getTransportView().setStatus((status.statusBusV2(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getThread().getTransportType())));
                            searchAPI.getSegments().get(i).getTransportView().setIdImg(status.statusTransportView);
                            searchAPI.getSegments().get(i).getTransportView().setPosition(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival()));
                            Log.d("PROVERKA_POS", String.valueOf(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival())));
                            searchAPI.getSegments().get(i).getTransportView().setTrack_max(status.max);
                            Log.d("PROVERKA", String.valueOf(status.max));
                            searchAPI.getSegments().get(i).getTransportView().setTrack_visible(status.send);
                            searchAPI.getSegments().get(i).getTransportView().setColorStatusTitle(status.colorStatusTitle);
                        }
                        adapterListSearch.notifyDataSetChanged();
                        recycler.setAdapter(adapterListSearch);
                    }
                });
            }
        }, 0, 30000);
    }

    public void updateView() {
//        final Status status = new Status();
//        for (int i = 0; i < searchAPI.getSegments().size(); i++) {
//            searchAPI.getSegments().get(i).setStatus(status.statusBusV2(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getThread().getTransportType()));
//            searchAPI.getSegments().get(i).setIdImg(status.idImg);
//            searchAPI.getSegments().get(i).setPosition(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival()));
//            Log.d("PROVERKA_POS", String.valueOf(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival())));
//            searchAPI.getSegments().get(i).setTrack_max(status.max);
//            Log.d("PROVERKA", String.valueOf(status.max));
//            searchAPI.getSegments().get(i).setTrack_visible(status.send);
//        }
//        adapterListSearch.notifyDataSetChanged();
//        recycler.setAdapter(adapterListSearch);
        final Status status = new Status();
        for (int i = 0; i < searchAPI.getSegments().size(); i++) {
            searchAPI.getSegments().get(i).getTransportView().setStatus(status.statusBusV2(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getThread().getTransportType()));
            searchAPI.getSegments().get(i).getTransportView().setIdImg(status.statusTransportView);
            searchAPI.getSegments().get(i).getTransportView().setPosition(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival()));
            Log.d("PROVERKA_POS", String.valueOf(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival())));
            searchAPI.getSegments().get(i).getTransportView().setTrack_max(status.max);
            Log.d("PROVERKA", String.valueOf(status.max));
            searchAPI.getSegments().get(i).getTransportView().setTrack_visible(status.send);
            searchAPI.getSegments().get(i).getTransportView().setColorStatusTitle(status.colorStatusTitle);
        }
        adapterListSearch.notifyDataSetChanged();
        recycler.setAdapter(adapterListSearch);
    }

    private void test2() {

        Timer t = new Timer();
        Date date = new Date();
        Log.d("+=+=+", date.toString());
        date.setTime(date.getTime() + 1000);
        final boolean[] bFlagForceExit = {true};
        final Handler timerHandler = new Handler();
        final Runnable timerRunnable = new Runnable() {
            @Override
            public void run() {
                if (d.isSet && dbAdapter.isRecordToRaspTable("Зарайск_Луховицы")) {
                    Log.d("search__", "1");
                    Log.d("search__", "record table" + dbAdapter.recordRaspTableV2("Зарайск_Луховицы").getSearch().toString());
                    isCancel = true;
                } else {
                    Log.d("search__", "0");
                }

                if (isCancel)
                    timerHandler.postDelayed(this, 1000);

            }
        };

        timerRunnable.run();
    }

    private void test() {
        String reys = "Рязань_Коломна";
        if (dbAdapter.isRecordToRaspTable(reys)) {
            Log.d("search__", "record yes");
            searchAPI.getSegments().clear();
            Yandex.SearchAPI lb = new Yandex.SearchAPI();
            lb = this.dbAdapter.recordRaspTableV2(reys);
            lb.getSegments().get(0).setArrival("4456");
            Log.d("search__", lb.getSegments().get(0).getArrival());
            for (int i = 0; i < lb.getSegments().size(); i++) {
                searchAPI.getSegments().add(lb.getSegments().get(i));
            }
            // RefrashTime();

            this.adapterListSearch.notifyDataSetChanged();
            this.searchReys = reys;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_search_op) {

//            CustomDialogFragment customDialogFragment = new CustomDialogFragment();
//            customDialogFragment.show(getSupportFragmentManager(), "d");
////            MyDialog myDialog = new MyDialog(this);
////            myDialog.createOneButtonAlertDialog("lol", "ddddd");
////
////            Intent intent = getIntent();
////
//            Bundle extras = getIntent().getExtras();
////
//           textView.setText(extras.getString("type")+"   "+ extras.getString("isCheck"));

            createDialogOptionSearch();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void createEdits() {
        stationAutoComplete = new AdapterHuntStationCityAutoComplete(this);
        editTextFrom.setAdapter(stationAutoComplete);
        editTextFrom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                stationDBFrom = (StationDB) adapterView.getItemAtPosition(position);
                editTextFrom.setText(stationDBFrom.getNameStation());
                Toast.makeText(getApplicationContext(), stationDBFrom.getCodeStation(), Toast.LENGTH_SHORT).show();
                Log.d("station", stationDBFrom.getCodeStation() + " " + stationDBFrom.getCodeCity() + " " + stationDBFrom.getCodeRegion() + " " + stationDBFrom.getTypeTransport());
            }
        });

        editTextTo.setAdapter(stationAutoComplete);
        editTextTo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                stationDBTo = (StationDB) adapterView.getItemAtPosition(position);
                editTextTo.setText(stationDBTo.getNameStation());
                Toast.makeText(getApplicationContext(), stationDBTo.getCodeStation(), Toast.LENGTH_SHORT).show();
                Log.d("station", stationDBTo.getCodeStation() + " " + stationDBTo.getCodeCity() + " " + stationDBTo.getCodeRegion() + " " + stationDBTo.getTypeTransport());
            }
        });
    }

    private void preview() {
        relativeLayout = findViewById(R.id.RLContent);
        priview = LayoutInflater.from(getApplicationContext()).inflate(R.layout.priview_search_station, null);
        priview.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        relativeLayout.addView(priview);
        isPriview = true;
    }

    private void creteDialog() {
        dialog = new AlertDialog.Builder(this);
        dialog.setTitle("поиск расписания");
        dialog.setMessage("Такой маршрут уже был добавлен ранее, хотите обновить?");
        dialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if (isOnline3()) {
                    dbAdapter.deleteTable(searchReys);
                    dbAdapter.createTableV2(searchReys);
                    searchAPI.clearAll();
                    download = new Download(getApplicationContext(), stationDBFrom.getCodeStation(), stationDBTo.getCodeStation(), transportType, false, Yandex.SEARCH_RASP.from_to);
                    download.execute();
                    //RefrashTimeV2();
                    dbAdapter.resetHistiryRasp(searchReys, stationDBFrom.getNameStation(), stationDBFrom.getCodeStation(),
                            stationDBTo.getNameStation(), stationDBTo.getCodeStation());

                    mSVProgressHUD.showSuccessWithStatus("Готово!");
                    TastyToast.makeText(getApplicationContext(), "Расписание " + editTextFrom.getText()
                            + "-" + editTextTo.getText() + " успешно обновлено...", Toast.LENGTH_LONG, TastyToast.SUCCESS).show();

                } else {
                    mSVProgressHUD.showInfoWithStatus("нет интернета...");
                }
            }
        });
        dialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                TastyToast.makeText(getApplicationContext(), "Расписание используется из БД...", Toast.LENGTH_SHORT, TastyToast.SUCCESS).show();
                searchAPI.clearAll();
                searchAPI = dbAdapter.recordRaspTableV2(searchReys);
                dbAdapter.resetHistiryRasp(searchReys, searchAPI.getSearch().getFromSearch().getTitle(), searchAPI.getSearch().getFromSearch().getCode(),
                        searchAPI.getSearch().getToSearch().getTitle(), searchAPI.getSearch().getToSearch().getCode());
                updateView();
                mSVProgressHUD.showSuccessWithStatus("Готово!");
            }
        });
    }

    private void setIconSwitch() {
        final IconSwitch.Checked checked = null;
        iconSwitch.setBackgroundColor(Color.parseColor("#FF1D1D1D"));

        iconSwitch.setThumbColorLeft(Color.parseColor("#00ff8a"));
        iconSwitch.setThumbColorRight(Color.parseColor("#ff0066"));

        iconSwitch.setActiveTintIconLeft(Color.parseColor("#ffffff"));
        iconSwitch.setInactiveTintIconLeft(Color.parseColor("#00ff7e"));
        iconSwitch.setActiveTintIconRight(Color.parseColor("#ffffff"));
        iconSwitch.setInactiveTintIconRight(Color.parseColor("#ff0066"));
        iconSwitch.setCheckedChangeListener(new IconSwitch.CheckedChangeListener() {
            @Override
            public void onCheckChanged(IconSwitch.Checked current) {
                switch (current.toggle()) {
                    case RIGHT:
                        if (searchReys != null) {
                            reveseHistory();
                            searchAPI.clearAll();
                            searchAPI = dbAdapter.recordRaspTableV2(searchReys);
                            adapterListSearch = new AdapterListSearch(getApplicationContext(), searchAPI);

                            //recycler.getAdapter().notifyDataSetChanged();
                            //adapterListSearch.notifyDataSetChanged();
                            //recycler.setAdapter(adapterListSearch);
                            updateView();


                            mSVProgressHUD.showInfoWithStatus(editTextFrom.getText() + "-" + editTextTo.getText());

                        }
                        break;

                    case LEFT:
                        if (searchReysReverse != null) {
                            reveseHistory();
                            searchAPI.clearAll();
                            searchAPI = dbAdapter.recordRaspTableV2(searchReysReverse);

                            adapterListSearch = new AdapterListSearch(getApplicationContext(), searchAPI);
                            // recycler.getAdapter().notifyDataSetChanged();
                            //adapterListSearch.notifyDataSetChanged();
                            //recycler.setAdapter(adapterListSearch);
                            updateView();


//                            editTextTo.setText(stationDBTo.getNameStation());
//                            editTextFrom.setText(stationDBFrom.getNameStation());

                            mSVProgressHUD.showInfoWithStatus(editTextTo.getText() + "-" + editTextFrom.getText());

                        }

                        break;

                    default:
                }
            }
        });
    }

    private void reveseHistory()
    {
        String nameFrom = stationDBFrom.getNameStation();
        String codeFrom = stationDBFrom.getCodeStation();

        stationDBFrom.setNameStation(stationDBTo.getNameStation());
        stationDBFrom.setCodeStation(stationDBTo.getCodeStation());

        stationDBTo.setNameStation(nameFrom);
        stationDBTo.setCodeStation(codeFrom);

        editTextTo.setText(stationDBTo.getNameStation());
        editTextFrom.setText(stationDBFrom.getNameStation());

        Log.d("switch", "from: "+stationDBFrom.getNameStation() + " to: " + stationDBTo.getNameStation());
    }

    public void showErrorWithStatus(View view) {
        mSVProgressHUD.showErrorWithStatus("заполните все поля", SVProgressHUD.SVProgressHUDMaskType.GradientCancel);
    }

    public boolean isNetworkOnline(Context context) {
        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return status;

    }

    public class Status {
        public Integer idImg;
        public int max = 0;
        public boolean send = false;
        // private StatusTransport statusTransport;
        private int driveImg;//активный тран
        private int driveOffImg;//уехал тран
        private int driveWillImg;// поедет тран
        private Yandex.TRANSPORT_TYPE transportType;
        private Yandex.StatusTransportView statusTransportView = null;
        private Integer colorStatusTitle = null;

        public Integer trackTransport(String fromTime, String toTime) {
            Time tFrom = new Time();
            Time tTo = new Time();
            Time time = new Time(Time.getCurrentTimezone());
            time.setToNow();
            int[] timeFrom = timeStrongToint(fromTime);
            int[] timeTo = timeStrongToint(toTime);
            tFrom.set(0, timeFrom[1], timeFrom[0], 0, 0, 0);
            tTo.set(0, timeTo[1], timeTo[0], 0, 0, 0);
            Log.d("TRACKING_TRANSPORT", String.valueOf(timeFrom[0]) + "  " + String.valueOf(timeFrom[1]));
            Log.d("TRACKING_TRANSPORT", String.valueOf(timeTo[0]) + "  " + String.valueOf(timeTo[1]));

            int minuteMinus = tTo.minute - tFrom.minute;
            this.max = ((tTo.hour * 60) + tTo.minute) - ((tFrom.hour * 60) + tFrom.minute);
            if ((tFrom.hour * 60) + tFrom.minute >= (time.hour * 60) + time.minute || (time.hour * 60) + time.minute >= (tTo.hour * 60) + tTo.minute) {
                this.send = false;
                return 0;
            }
            this.send = true;
            statusTransportView = Yandex.StatusTransportView.driveImg;
            Integer result = this.max - (((tTo.hour * 60) + tTo.minute) - ((time.hour * 60) + time.minute));
            Log.d("TIME_MINUTE", String.valueOf(result));
            if (result >= 0 && result <= this.max) {
                return result;
            }
            this.send = false;
            return result;
        }

        public String statusBus(String time) {
            Time t = new Time();
            int[] timeMas = timeToInt(time);
            t.set(0, timeMas[1], timeMas[0], 0, 0, 0);
            Time tNow = new Time(Time.getCurrentTimezone());
            tNow.setToNow();
            int hour = t.hour - tNow.hour;
            int minute = t.minute - tNow.minute;
            if ((hour < 0 || minute < 0) && (hour <= 0 || minute > 0)) {
                this.idImg = R.drawable.buses_red;
                if (hour != 0) {
                    return "ушел";
                }

                if (minute < 0) {
                    minute = -minute;
                }
                return String.valueOf(minute) + " мин. назад";
            }
            this.idImg = R.drawable.buses_green;
            String str = "отправка через " + (String.valueOf(hour) == "0" ? "" : String.valueOf(hour) + " ч. ");
            if (minute < 0) {
                minute = -minute;
            }
            return String.valueOf(minute) + " мин.";
        }

        public String statusBusV2(String time, Yandex.TRANSPORT_TYPE transportType) {
            Time t = new Time();
            int[] timeMas = timeStrongToint(time);
            t.set(0, timeMas[1], timeMas[0], 0, 0, 0);
            Time tNow = new Time(Time.getCurrentTimezone());
            //   statusTransport = new StatusTransport();
            tNow.setToNow();
            int hour = t.hour - tNow.hour;
            int result = (hour * 60) + (t.minute - tNow.minute);
            Integer pHour = FormatSecToTime(result).get(0);
            Integer pMinute = FormatSecToTime(result).get(1);
            setTransportType(transportType);
            if (result > 0) {
                String str = null;
                statusTransportView = Yandex.StatusTransportView.driveWillImg;
                colorStatusTitle = R.drawable.background_status_drive_will;
                StringBuilder append = new StringBuilder().append("отправка через ").append(pHour == 0 ? "" : String.valueOf(pHour) + " ч. ");
                if (pMinute == 0) {
                    str = "";
                } else {
                    str = String.valueOf(pMinute) + " мин.";
                }
                return append.append(str).toString();
            } else if (hour == 0) {
                statusTransportView = Yandex.StatusTransportView.driveImg;
                colorStatusTitle = R.drawable.background_status_drive;
                return "ушел " + String.valueOf(pMinute < 0 ? -pMinute : pMinute) + " мин. назад";
            } else if (result > (-this.max)) {
                this.idImg = getDriveImg();
                statusTransportView = Yandex.StatusTransportView.driveImg;
                colorStatusTitle = R.drawable.background_status_drive;
                return "ушел";
            } else {
                statusTransportView = Yandex.StatusTransportView.driveOffImg;
                colorStatusTitle = R.drawable.background_status_drive_off;
                return "ушел";
            }
        }

        public void setTransportType(Yandex.TRANSPORT_TYPE transportType) {
            this.transportType = transportType;
            switch (this.transportType) {
                case helicopter:
                    driveImg = R.drawable.helicopter_yellow;
                    driveOffImg = R.drawable.helicopter_red;
                    driveWillImg = R.drawable.helicopter_green;
                    break;
                case suburban:
                    driveImg = R.drawable.surban_yellow;
                    driveOffImg = R.drawable.surban_red;
                    driveWillImg = R.drawable.surban_green;
                    break;
                case water:
                    driveImg = R.drawable.water_yellow;
                    driveOffImg = R.drawable.water_red;
                    driveWillImg = R.drawable.water_green;
                    break;
                case train:
                    driveImg = R.drawable.train_yellow;
                    driveOffImg = R.drawable.train_red;
                    driveWillImg = R.drawable.train_green;
                    break;
                case plane:
                    driveImg = R.drawable.fly_yellow;
                    driveOffImg = R.drawable.fly_red;
                    driveWillImg = R.drawable.fly_green;
                    break;
                case bus:
                    driveImg = R.drawable.buses_yellow;
                    driveOffImg = R.drawable.buses_red;
                    driveWillImg = R.drawable.buses_green;
                    break;
                default:
                    driveImg = R.drawable.preview_logo;
                    driveOffImg = R.drawable.preview_logo;
                    driveWillImg = R.drawable.preview_logo;
                    break;
            }
        }

        public int getDriveImg() {
            return driveImg;
        }

        public int getDriveOffImg() {
            return driveOffImg;
        }

        public int getDriveWillImg() {
            return driveWillImg;
        }

        public class StatusTransport {
            private int driveImg;//активный тран
            private int driveOffImg;//уехал тран
            private int driveWillImg;// поедет тран
            private Yandex.TRANSPORT_TYPE transportType;

            public StatusTransport(Yandex.TRANSPORT_TYPE transportType) {
                this.transportType = transportType;
                setTransportType(transportType);
            }

            public StatusTransport() {
            }

            public Yandex.TRANSPORT_TYPE getTransportType() {
                return transportType;
            }

            public void setTransportType(Yandex.TRANSPORT_TYPE transportType) {
                this.transportType = transportType;
                switch (this.transportType) {
                    case helicopter:
                        driveImg = R.drawable.helicopter_yellow;
                        driveOffImg = R.drawable.helicopter_red;
                        driveWillImg = R.drawable.helicopter_green;
                        break;
                    case suburban:
                        driveImg = R.drawable.surban_yellow;
                        driveOffImg = R.drawable.surban_red;
                        driveWillImg = R.drawable.surban_green;
                        break;
                    case water:
                        driveImg = R.drawable.water_yellow;
                        driveOffImg = R.drawable.water_red;
                        driveWillImg = R.drawable.water_green;
                        break;
                    case train:
                        driveImg = R.drawable.train_yellow;
                        driveOffImg = R.drawable.train_red;
                        driveWillImg = R.drawable.train_green;
                        break;
                    case plane:
                        driveImg = R.drawable.fly_yellow;
                        driveOffImg = R.drawable.fly_red;
                        driveWillImg = R.drawable.fly_green;
                        break;
                    case bus:
                        driveImg = R.drawable.buses_yellow;
                        driveOffImg = R.drawable.buses_red;
                        driveWillImg = R.drawable.buses_green;
                        break;
                    default:
                        driveImg = R.drawable.preview_logo;
                        driveOffImg = R.drawable.preview_logo;
                        driveWillImg = R.drawable.preview_logo;
                        break;
                }
            }

            public int getDriveImg() {
                return driveImg;
            }

            public int getDriveOffImg() {
                return driveOffImg;
            }

            public int getDriveWillImg() {
                return driveWillImg;
            }
        }

    }

    public class StatusTransport {
        private int driveImg;//активный тран
        private int driveOffImg;//уехал тран
        private int driveWillImg;// поедет тран
        private Yandex.TRANSPORT_TYPE transportType;

        public StatusTransport(Yandex.TRANSPORT_TYPE transportType) {
            this.transportType = transportType;
            setTransportType(transportType);
        }

        public StatusTransport() {
        }

        public Yandex.TRANSPORT_TYPE getTransportType() {
            return transportType;
        }

        public void setTransportType(Yandex.TRANSPORT_TYPE transportType) {
            this.transportType = transportType;
            switch (this.transportType) {
                case helicopter:
                    driveImg = R.drawable.helicopter_yellow;
                    driveOffImg = R.drawable.helicopter_red;
                    driveWillImg = R.drawable.helicopter_green;
                    break;
                case suburban:
                    driveImg = R.drawable.surban_yellow;
                    driveOffImg = R.drawable.surban_red;
                    driveWillImg = R.drawable.surban_green;
                    break;
                case water:
                    driveImg = R.drawable.water_yellow;
                    driveOffImg = R.drawable.water_red;
                    driveWillImg = R.drawable.water_green;
                    break;
                case train:
                    driveImg = R.drawable.train_yellow;
                    driveOffImg = R.drawable.train_red;
                    driveWillImg = R.drawable.train_green;
                    break;
                case plane:
                    driveImg = R.drawable.fly_yellow;
                    driveOffImg = R.drawable.fly_red;
                    driveWillImg = R.drawable.fly_green;
                    break;
                case bus:
                    driveImg = R.drawable.buses_yellow;
                    driveOffImg = R.drawable.buses_red;
                    driveWillImg = R.drawable.buses_green;
                    break;
                default:
                    driveImg = R.drawable.preview_logo;
                    driveOffImg = R.drawable.preview_logo;
                    driveWillImg = R.drawable.preview_logo;
                    break;
            }
        }

        public int getDriveImg() {
            return driveImg;
        }

        public int getDriveOffImg() {
            return driveOffImg;
        }

        public int getDriveWillImg() {
            return driveWillImg;
        }
    }

    private void loadHistory() {
//        if (dbAdapter.hashHistoryRasp()) {
//            String reys = this.dbAdapter.recordhistoryRasp();
//            if (dbAdapter.isRecordToRaspTable(reys)) {
//                searchAPI.getSegments().clear();
//               // List<Bus> lb = new ArrayList();
//                Yandex.SearchAPI lb = new Yandex.SearchAPI();
//                lb = this.dbAdapter.recordRaspTableV2(reys);
//                for (int i = 0; i < lb.getSegments().size(); i++) {
//                    this.searchAPI.getSegments().add(lb.getSegments().get(i));
//                }
//                RefrashTime();
//
//                this.adapterBus.notifyDataSetChanged();
//                this.searchReys = reys;
//
//                String[] s = reys.split("_");
//                this.from = s[0];
//                this.to = s[1];
//                this.searchReysReverse = s[1] + "_" + s[0];
//                Log.d("STRINGF", this.searchReys + " " + this.searchReysReverse);
//            } else Log.d("loadHistory", "пустая табл");
//        } else {
//            preview();
//            adapterBus.notifyDataSetChanged();
//            recycler.setAdapter(adapterBus);
//            RefrashTime();
//        }

        if (dbAdapter.hashHistoryRasp()) {
            searchReys = dbAdapter.recordhistoryRasp();
            searchReysReverse = Format.getNameStatuionOfNameReys(searchReys, Yandex.SEARCH_RASP.to_from) + "_" + Format.getNameStatuionOfNameReys(searchReys, Yandex.SEARCH_RASP.from_to);

            History history = dbAdapter.recordhistoryRasp2();
            stationDBFrom.setCodeStation(history.getFromCodeStation());
            stationDBFrom.setNameStation(history.getFromStation());

            stationDBTo.setCodeStation(history.getToCodeStation());
            stationDBTo.setNameStation(history.getToStation());

            editTextFrom.setText(stationDBFrom.getNameStation());
            editTextTo.setText(stationDBTo.getNameStation());

            if (dbAdapter.isRecordToRaspTable(searchReys)) {
                searchAPI.clearAll();
                Yandex.SearchAPI searchList = dbAdapter.recordRaspTableV2(searchReys);
                searchAPI.setSearch(searchList.getSearch());
                for (int i = 0; i < searchList.getSegments().size(); i++) {
                    searchList.getSegments().get(i).getTransportView().setThumb(Yandex.getThumb(searchList.getSegments().get(i).getThread().getTransportType()));
                    searchList.getSegments().get(i).getTransportView().setStatusDrive(searchList.getSegments().get(i).getThread().getTransportType());
                    searchList.getSegments().get(i).getTransportView().setIdImg(Yandex.StatusTransportView.driveWillImg);
                }

                searchAPI.setSegments(searchList.getSegments());
                RefrashTimeV2();
            }

        } else {
            preview();
            adapterListSearch.notifyDataSetChanged();
            recycler.setAdapter(adapterListSearch);
            RefrashTimeV2();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    private void getRasp() {

    }

    public void RefrashTime() {
        Timer t = new Timer();
//        final Status status = new Status();
//        t.scheduleAtFixedRate(new TimerTask() {
//            public void run() {
//                ActivityMain.this.runOnUiThread(new Runnable() {
//                    public void run() {
//                        for (int i = 0; i < busList.size(); i++) {
//                            busList.get(i).setStatus(status.statusBusV2(busList.get(i).getFromTime(), Yandex.setTransportType(busList.get(i).getTypeTransport())));
//                            busList.get(i).setIdImg(status.idImg);
//                            busList.get(i).setPosition(status.trackTransport(busList.get(i).getFromTime(), busList.get(i).getToTime()));
//                            Log.d("PROVERKA_POS", String.valueOf(status.trackTransport(busList.get(i).getFromTime(), busList.get(i).getToTime())));
//                            busList.get(i).setTrack_max(status.max);
//                            Log.d("PROVERKA", String.valueOf(status.max));
//                            busList.get(i).setTrack_visible(status.send);
//                        }
//                        adapterBus.notifyDataSetChanged();
//                        recycler.setAdapter(adapterBus);
//                    }
//                });
//            }
//        }, 0, 5000);
    }

    protected boolean isOnline3() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        return cm.getActiveNetworkInfo() != null;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_thread:
                startActivity(new Intent(this, ActivityThread.class));
                return true;
            case R.id.action_shudle:
                startActivity(new Intent(this, ActivityShudle.class));
                return true;
            case R.id.action_bookmark:
                startActivity(new Intent(this, ActivityBookMark.class));
                break;
            case R.id.action_nearest_settlement:
                startActivity(new Intent(this, ActivityNearestSettlement.class));
                break;
            case R.id.action_nearest_stations:
                startActivity(new Intent(this, ActivityNearestStations.class));
                break;
            case R.id.action_author:
                startActivity(new Intent(this, Author.class));
                break;
            case R.id.action_help:
                startActivity(new Intent(this, ActivityHelp.class));
                break;
            case R.id.action_settings:
                startActivity(new Intent(this, ActivitySetting.class));
                break;
            case R.id.action_map:
                startActivity(new Intent(this, ActivityMaps.class));
                break;
            case R.id.action_sort:
                createDialogCustom();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    void createDialogCustom() {

        View body = LayoutInflater.from(getApplicationContext()).inflate(R.layout.sort_layout, null);
        final CheckBox checkBox = body.findViewById(R.id.checkBox);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    checkBox.setChecked(true);
                    checkBox.setText("по возрвствнию");
                } else {
                    checkBox.setChecked(false);
                    checkBox.setText("по убыванию");
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.bus_station);
        builder.setTitle("Сортировка")
                .setView(body)
                .setCancelable(false)
                .setItems(new CharSequence[]{"время отправки", "время прибытия", "время в пути", "статус транспорта"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, final int i) {
                        switch (i) {
                            case 0:
                                Collections.sort(searchAPI.getSegments(), new Comparator<Yandex.SearchAPI.Segments>() {
                                    @Override
                                    public int compare(Yandex.SearchAPI.Segments t1, Yandex.SearchAPI.Segments t2) {
                                        if (checkBox.isChecked())
                                            return Sorting.sort(Sorting.getDeparture(t1), Sorting.getDeparture(t2), false);

                                        else
                                            return Sorting.sort(Sorting.getDeparture(t1), Sorting.getDeparture(t2), true);
                                    }
                                });
                                recycler.getAdapter().notifyDataSetChanged();
                                break;
                            case 1:
                                Collections.sort(searchAPI.getSegments(), new Comparator<Yandex.SearchAPI.Segments>() {
                                    @Override
                                    public int compare(Yandex.SearchAPI.Segments t1, Yandex.SearchAPI.Segments t2) {
                                        if (checkBox.isChecked())
                                        return Sorting.sort(Sorting.getDeparture(t1), Sorting.getDeparture(t2), false);

                                        else
                                        return Sorting.sort(Sorting.getDeparture(t1), Sorting.getDeparture(t2), true);
                                    }
                                });
                                recycler.getAdapter().notifyDataSetChanged();
                                break;
                            case 2:
                                Log.d("sorting", " " + Format.FormatDurationToInt(searchAPI.getSegments().get(0).getDuration()));
                                Collections.sort(searchAPI.getSegments(), new Comparator<Yandex.SearchAPI.Segments>() {
                                    @Override
                                    public int compare(Yandex.SearchAPI.Segments t1, Yandex.SearchAPI.Segments t2) {
                                        if (checkBox.isChecked())
                                            return Sorting.sort(Sorting.getDuration(t1), Sorting.getDuration(t2), false);
                                        else
                                            return Sorting.sort(Sorting.getDuration(t1), Sorting.getDuration(t2), true);
                                    }
                                });
                                recycler.getAdapter().notifyDataSetChanged();
                                break;
                            case 3:
                                Collections.sort(searchAPI.getSegments(), new Comparator<Yandex.SearchAPI.Segments>() {
                                    @Override
                                    public int compare(Yandex.SearchAPI.Segments t1, Yandex.SearchAPI.Segments t2) {
                                        if (checkBox.isChecked())
                                            return Sorting.sort(Sorting.getStatusLevel(t1), Sorting.getStatusLevel(t2), false);

                                        else
                                            return Sorting.sort(Sorting.getStatusLevel(t1), Sorting.getStatusLevel(t2), true);

                                    }
                                });
                                recycler.getAdapter().notifyDataSetChanged();
                                break;
                            default:
                                break;
                        }
                    }
                }).setNegativeButton("отмена", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int n) {
                dialogInterface.cancel();
            }
        });
        builder.create();
        builder.show();
    }

    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        return wifiInfo != null && wifiInfo.isConnected();
    }

    public static boolean isOnline2(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public List<Integer> FormatSecToTime(Integer time) {
        List<Integer> result = new ArrayList();
        Integer hour = time / 60;
        Integer minute = time - (hour * 60);
        result.add(hour);
        result.add(minute);
        return result;
    }

    public int[] timeToInt(String time) {
        int[] result = new int[0];
        String[] str = time.split(":");
        int chour = Integer.parseInt(str[0]);
        int minute = Integer.parseInt(str[1]);
        return new int[]{chour, minute};
    }

    public int[] timeStrongToint(String time) {
        if (time.length() == 8 || time.length() == 5) {
            Integer[] result = new Integer[2];
            String[] str = time.split(":");
            int chour = Integer.parseInt(str[0]);
            int minute = Integer.parseInt(str[1]);
            Log.d("timeFormat", chour + " " + minute);
            return new int[]{chour, minute};
        } else if (time.length() > 8) {
            time = time.substring(time.indexOf(' ') + 1, time.length());
            Log.d("timeFormat", time);
            int[] result = new int[2];
            String[] str = time.split(":");
            int chour = Integer.parseInt(str[0]);
            int minute = Integer.parseInt(str[1]);
            Log.d("timeFormat", chour + " " + minute);
            return new int[]{chour, minute};
        } else {
            return new int[2];
        }
    }

    private void createDialogOptionSearch() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
        View view2 = getLayoutInflater().inflate(R.layout.dialog, null); // Получаем layout по его ID

        List<TypeTransport> transportList = new ArrayList<>();
        for (int i = 0; i < Yandex.TRANSPORT_TYPE.values().length; i++)
        {
            transportList.add(new TypeTransport(Yandex.TRANSPORT_TYPE.values()[i].getTitle(), Yandex.TRANSPORT_TYPE.values()[i].getIdImg(),
                    Yandex.TRANSPORT_TYPE.values()[i], i));
        }

//        transportList.add(new TypeTransport("автобус", R.drawable.buses_red, Yandex.TRANSPORT_TYPE.bus, 0));
//        transportList.add(new TypeTransport("поезд", R.drawable.train_red, Yandex.TRANSPORT_TYPE.train, 1));
//        transportList.add(new TypeTransport("электричка", R.drawable.surban_red, Yandex.TRANSPORT_TYPE.suburban, 2));
//        transportList.add(new TypeTransport("самолет", R.drawable.fly_red, Yandex.TRANSPORT_TYPE.plane, 3));
//        transportList.add(new TypeTransport("вертолет", R.drawable.helicopter_red, Yandex.TRANSPORT_TYPE.helicopter, 4));
//        transportList.add(new TypeTransport("водное судно", R.drawable.ship_red, Yandex.TRANSPORT_TYPE.water, 5));
//        transportList.add(new TypeTransport("весь транспорт ", R.drawable.all, null, 6));

        final AdapterTypeTransport adapterTypeTransport = new AdapterTypeTransport(getApplicationContext(), transportList);
        final Spinner spinner = view2.findViewById(R.id.typeTransportSpinner);
        spinner.setAdapter(adapterTypeTransport);
        final CheckBox checkBox = view2.findViewById(R.id.isPer);


        builder.setView(view2);
        builder.setPositiveButton("готово", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                transportType = ((TypeTransport) spinner.getSelectedItem()).getTransportType();
                isTransfers = checkBox.isChecked();
                TastyToast.makeText(getApplicationContext(), "тип транспорта: " + ((TypeTransport) spinner.getSelectedItem()).getTitle() +
                        spinner.getSelectedItemPosition() + ". \nпересадки пока недоступны", Toast.LENGTH_SHORT, TastyToast.SUCCESS).show();
                dialog.dismiss();
            }
        }).setNegativeButton("отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        });

        builder.create();
        builder.show();
    }

    private void refreshBusList() {

    }

    private class Download extends AsyncTask<String, Void, Void> {

        private ProgressDialog pr;
        private boolean isDownload = false;
        private String titleMessage;
        private Yandex.SEARCH_RASP searchRasp;
        private Context context;
        private String url = "";
        private Yandex.API api;
        private Document doc;
        public boolean isSet = false;
        private Yandex.SearchAPI searchapiD;
        private boolean isValidData = false;

        //search
        public Download(Context context, String from, String to, Yandex.TRANSPORT_TYPE transportType, boolean transfers, Yandex.SEARCH_RASP searchRasp) {
            this.context = context;
            this.api = Yandex.API.search;
            this.searchRasp = searchRasp;
            url = "https://api.rasp.yandex.net/v3.0/search/?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml&limit=5000&from=" + from + "&to=" + to;
            if (transportType != null)
                url += "&transport_types=" + transportType;
            if (transfers)
                url += "&transfers=" + transfers;
            Log.d("url", url);
            searchapiD = new Yandex.SearchAPI();
            dbAdapter = new DBAdapter(context);
        }

        protected void onPreExecute() {
            pr = new ProgressDialog(ActivityMain.this);
            pr.setTitle("Загрузка рассписания");
            pr.setMessage("Соединение с сервером...");
            pr.setIndeterminate(false);
            pr.setCancelable(false);
            pr.show();
            super.onPreExecute();
        }

        protected Void doInBackground(String... Url) {
            try {
                doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new URL(url).openStream()));
                doc.getDocumentElement().normalize();
                doc.getDocumentElement().getTagName();
                isDownload = true;
            } catch (Exception e) {
                Log.d("download", "error network");
                isDownload = false;
            }
            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Void aVoid) {

            switch (searchRasp) {
                case from_to:
                    if (isDownload) {

                        Log.d("search_parse", doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());
                        searchapiD.getPagination().setTotal(doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());

                        Element search = (Element) doc.getDocumentElement().getElementsByTagName("search").item(0);
                        Log.d("search_parse", "============to===============");
                        Log.d("search_parse", search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setCode(search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue()));
                        try {
                            Log.d("search_parse", search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getTextContent());
                            searchapiD.getSearch().getToSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("search_parse", search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setTitle(search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                        try {
                            Log.d("search_parse", search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                            searchapiD.getSearch().getToSearch().setShortTitle(search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Log.d("search_parse", "============from===============");
                        Log.d("search_parse", search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setCode(search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue()));
                        try {
                            Log.d("search_parse", search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                            searchapiD.getSearch().getFromSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("search_parse", search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setTitle(search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                        try {
                            Log.d("search_parse", search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());
                            searchapiD.getSearch().getFromSearch().setShortTitle(search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        NodeList scheduleList = doc.getElementsByTagName("segment");
                        if (scheduleList.getLength() > 0) {
                            for (int i = 0; i < scheduleList.getLength(); i++) {
                                Yandex.SearchAPI.Segments segmen = new Yandex.SearchAPI.Segments();

                                Element segment = (Element) scheduleList.item(i);
                                Log.d("search_parse", segment.getElementsByTagName("except_days").item(0).getTextContent());
                                segmen.setExceptDays(segment.getElementsByTagName("except_days").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("arrival").item(0).getTextContent());
                                segmen.setArrival(Format.getShortTime(segment.getElementsByTagName("arrival").item(0).getTextContent()));
                                Log.d("search_parse_depart", Format.getShortTime(segment.getElementsByTagName("departure").item(0).getTextContent()));
                                segmen.setDeparture(Format.getShortTime(segment.getElementsByTagName("departure").item(0).getTextContent()));
                                Log.d("search_parse", segment.getElementsByTagName("duration").item(0).getTextContent());
                                segmen.setDuration(Format.FormatDurationTotime(segment.getElementsByTagName("duration").item(0).getTextContent()));
                                Log.d("search_parse", segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                                segmen.setArrivalTerminal(segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                                segmen.setArrivalPlatform(segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                                segmen.setDeparturePlatform(segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("days").item(0).getTextContent());
                                segmen.setDays(segment.getElementsByTagName("days").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("start_date").item(0).getTextContent());
                                segmen.setStartDate(segment.getElementsByTagName("start_date").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("stops").item(0).getTextContent());
                                segmen.setStops(segment.getElementsByTagName("stops").item(0).getTextContent());

                                Element from = (Element) segment.getElementsByTagName("from").item(0);
                                Log.d("search_parse", from.getElementsByTagName("code").item(0).getTextContent());
                                segmen.getFrom().setCode(from.getElementsByTagName("code").item(0).getTextContent());
                                Log.d("search_parse", from.getElementsByTagName("station_type").item(0).getTextContent());
                                segmen.getFrom().setStationType(Yandex.setTypeStation(from.getElementsByTagName("station_type").item(0).getTextContent()));
                                Log.d("search_parse", from.getElementsByTagName("title").item(0).getTextContent());
                                segmen.getFrom().setTitle(from.getElementsByTagName("title").item(0).getTextContent());
                                Log.d("search_parse", from.getElementsByTagName("popular_title").item(0).getTextContent());
                                segmen.getFrom().setPopularTitle(from.getElementsByTagName("popular_title").item(0).getTextContent());
                                Log.d("search_parse", from.getElementsByTagName("transport_type").item(0).getTextContent());
                                segmen.getFrom().setTransportType(Yandex.setTransportType(from.getElementsByTagName("transport_type").item(0).getTextContent()));
                                Log.d("search_parse", from.getElementsByTagName("station_type_name").item(0).getTextContent());
                                segmen.getFrom().setStationTypeName(from.getElementsByTagName("station_type_name").item(0).getTextContent());
                                Log.d("search_parse", from.getElementsByTagName("type").item(0).getTextContent());
                                segmen.getFrom().setType(Yandex.setType(from.getElementsByTagName("type").item(0).getTextContent()));

                                Element thread = (Element) segment.getElementsByTagName("thread").item(0);

                                Log.d("search_parse_thread", thread.getElementsByTagName("transport_type").item(0).getTextContent());
                                segmen.getThread().setTransportType(Yandex.setTransportType(thread.getElementsByTagName("transport_type").item(0).getTextContent()));
                                Log.d("search_parse_thread", thread.getElementsByTagName("uid").item(0).getTextContent());
                                segmen.getThread().setUid(thread.getElementsByTagName("uid").item(0).getTextContent());
                                Log.d("search_parse_thread", "#" + i + " non     " + thread.getChildNodes().item(4).getTextContent());
                                segmen.getThread().setTitle(thread.getChildNodes().item(4).getTextContent());
                                Log.d("search_parse_thread", thread.getElementsByTagName("vehicle").item(0).getTextContent());
                                segmen.getThread().setVehicle(thread.getElementsByTagName("vehicle").item(0).getTextContent());
                                Log.d("search_parse_thread", thread.getElementsByTagName("number").item(0).getTextContent());
                                segmen.getThread().setNumber(thread.getElementsByTagName("number").item(0).getTextContent());
                                Log.d("search_parse_thread", thread.getElementsByTagName("express_type").item(0).getTextContent());
                                segmen.getThread().setExpressType(Yandex.setExpressType(thread.getElementsByTagName("express_type").item(0).getTextContent()));
                                Log.d("search_parse_thread", thread.getElementsByTagName("thread_method_link").item(0).getTextContent());
                                segmen.getThread().setThreadMethodLink(thread.getElementsByTagName("thread_method_link").item(0).getTextContent());

                                Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                                segmen.getThread().getTransportSubtype().setColor(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                                Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                                segmen.getThread().getTransportSubtype().setCode(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                                Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());
                                segmen.getThread().getTransportSubtype().setTitle(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());

                                try {
                                    Element carrier = (Element) thread.getElementsByTagName("carrier").item(0);
                                    Log.d("search_parse", carrier.getElementsByTagName("code").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setCode(carrier.getElementsByTagName("code").item(0).getTextContent());
                                    Log.d("search_parse_title_carrier", carrier.getElementsByTagName("title").item(0).getChildNodes().item(0).getTextContent());
                                    segmen.getThread().getCarrier().setTitle(carrier.getElementsByTagName("title").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("url").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setUrl(carrier.getElementsByTagName("url").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("phone").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setPhone(carrier.getElementsByTagName("phone").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("address").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setAddress(carrier.getElementsByTagName("address").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("email").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setEmail(carrier.getElementsByTagName("email").item(0).getTextContent());
                                    Log.d("search_parse", "http:" + carrier.getElementsByTagName("logo").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setLogo("http:" + carrier.getElementsByTagName("logo").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("contacts").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setContacts(carrier.getElementsByTagName("contacts").item(0).getTextContent());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("search_parse", "carrier пустое поле");
                                }

                                Element to = (Element) segment.getElementsByTagName("to").item(0);
                                Log.d("search_parse", to.getElementsByTagName("code").item(0).getTextContent());
                                segmen.getTo().setCode(to.getElementsByTagName("code").item(0).getTextContent());
                                Log.d("search_parse", to.getElementsByTagName("station_type").item(0).getTextContent());
                                segmen.getTo().setStationType(Yandex.setTypeStation(to.getElementsByTagName("station_type").item(0).getTextContent()));
                                Log.d("search_parse", to.getElementsByTagName("title").item(0).getTextContent());
                                segmen.getTo().setTitle(to.getElementsByTagName("title").item(0).getTextContent());
                                Log.d("search_parse", to.getElementsByTagName("popular_title").item(0).getTextContent());
                                segmen.getTo().setPopularTitle(to.getElementsByTagName("popular_title").item(0).getTextContent());
                                Log.d("search_parse", to.getElementsByTagName("transport_type").item(0).getTextContent());
                                segmen.getTo().setTransportType(Yandex.setTransportType(to.getElementsByTagName("transport_type").item(0).getTextContent()));
                                Log.d("search_parse", to.getElementsByTagName("station_type_name").item(0).getTextContent());
                                segmen.getTo().setStationTypeName(to.getElementsByTagName("station_type_name").item(0).getTextContent());
                                Log.d("search_parse", to.getElementsByTagName("type").item(0).getTextContent());
                                segmen.getTo().setType(Yandex.setType(to.getElementsByTagName("type").item(0).getTextContent()));

                                segmen.setId(i);
                                segmen.setStatus("в пути");
                                segmen.getTransportView().setThumb(Yandex.getThumb(segmen.getThread().getTransportType()));
                                segmen.getTransportView().setStatusDrive(segmen.getThread().getTransportType());

                                searchapiD.getSegments().add(segmen);
                                // searchAPI.getSegments().add(segmen);
                            }

                            searchAPI.setSearch(searchapiD.getSearch());
                            searchAPI.setSegments(searchapiD.getSegments());
                            adapterListSearch.notifyDataSetChanged();

                            dbAdapter.resetHistiryRasp(searchReys, stationDBFrom.getNameStation(), stationDBFrom.getCodeStation(),
                                    stationDBTo.getNameStation(), stationDBTo.getCodeStation());
                            dbAdapter.createTableV2(searchReys);
                            dbAdapter.insertRaspTableV2(searchReys, searchapiD);
                            titleMessage = "готово";
                            isSet = true;
                        } else {
                            isSet = false;
                            titleMessage = "нет такого маршрута";
                        }
                    } else {
                        isSet = false;
                        titleMessage = "ошибка...";
                    }

                    pr.dismiss();
                    break;
                case to_from:
                    if (isDownload) {

                        Log.d("search_parse", doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());
                        searchapiD.getPagination().setTotal(doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());

                        Element search = (Element) doc.getDocumentElement().getElementsByTagName("search").item(0);
                        Log.d("search_parse", "============to===============");
                        Log.d("search_parse", search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setCode(search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue()));
                        try {
                            Log.d("search_parse", search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getTextContent());
                            searchapiD.getSearch().getToSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("search_parse", search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setTitle(search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                        try {
                            Log.d("search_parse", search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                            searchapiD.getSearch().getToSearch().setShortTitle(search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Log.d("search_parse", "============from===============");
                        Log.d("search_parse", search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setCode(search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue()));
                        try {
                            Log.d("search_parse", search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                            searchapiD.getSearch().getFromSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("search_parse", search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setTitle(search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                        try {
                            Log.d("search_parse", search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());
                            searchapiD.getSearch().getFromSearch().setShortTitle(search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        NodeList scheduleList = doc.getElementsByTagName("segment");
                        if (scheduleList.getLength() > 0) {
                            for (int i = 0; i < scheduleList.getLength(); i++) {
                                Yandex.SearchAPI.Segments segmen = new Yandex.SearchAPI.Segments();

                                Element segment = (Element) scheduleList.item(i);
                                Log.d("search_parse", segment.getElementsByTagName("except_days").item(0).getTextContent());
                                segmen.setExceptDays(segment.getElementsByTagName("except_days").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("arrival").item(0).getTextContent());
                                segmen.setArrival(Format.getShortTime(segment.getElementsByTagName("arrival").item(0).getTextContent()));
                                Log.d("search_parse", segment.getElementsByTagName("departure").item(0).getTextContent());
                                segmen.setDeparture(Format.getShortTime(segment.getElementsByTagName("departure").item(0).getTextContent()));
                                Log.d("search_parse", segment.getElementsByTagName("duration").item(0).getTextContent());
                                segmen.setDuration(Format.FormatDurationTotime(segment.getElementsByTagName("duration").item(0).getTextContent()));
                                Log.d("search_parse", segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                                segmen.setArrivalTerminal(segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                                segmen.setArrivalPlatform(segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                                segmen.setDeparturePlatform(segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("days").item(0).getTextContent());
                                segmen.setDays(segment.getElementsByTagName("days").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("start_date").item(0).getTextContent());
                                segmen.setStartDate(segment.getElementsByTagName("start_date").item(0).getTextContent());
                                Log.d("search_parse", segment.getElementsByTagName("stops").item(0).getTextContent());
                                segmen.setStops(segment.getElementsByTagName("stops").item(0).getTextContent());

                                Element from = (Element) segment.getElementsByTagName("from").item(0);
                                Log.d("search_parse", from.getElementsByTagName("code").item(0).getTextContent());
                                segmen.getFrom().setCode(from.getElementsByTagName("code").item(0).getTextContent());
                                Log.d("search_parse", from.getElementsByTagName("station_type").item(0).getTextContent());
                                segmen.getFrom().setStationType(Yandex.setTypeStation(from.getElementsByTagName("station_type").item(0).getTextContent()));
                                Log.d("search_parse", from.getElementsByTagName("title").item(0).getTextContent());
                                segmen.getFrom().setTitle(from.getElementsByTagName("title").item(0).getTextContent());
                                Log.d("search_parse", from.getElementsByTagName("popular_title").item(0).getTextContent());
                                segmen.getFrom().setPopularTitle(from.getElementsByTagName("popular_title").item(0).getTextContent());
                                Log.d("search_parse", from.getElementsByTagName("transport_type").item(0).getTextContent());
                                segmen.getFrom().setTransportType(Yandex.setTransportType(from.getElementsByTagName("transport_type").item(0).getTextContent()));
                                Log.d("search_parse", from.getElementsByTagName("station_type_name").item(0).getTextContent());
                                segmen.getFrom().setStationTypeName(from.getElementsByTagName("station_type_name").item(0).getTextContent());
                                Log.d("search_parse", from.getElementsByTagName("type").item(0).getTextContent());
                                segmen.getFrom().setType(Yandex.setType(from.getElementsByTagName("type").item(0).getTextContent()));

                                Element thread = (Element) segment.getElementsByTagName("thread").item(0);

                                Log.d("search_parse_thread", thread.getElementsByTagName("transport_type").item(0).getTextContent());
                                segmen.getThread().setTransportType(Yandex.setTransportType(thread.getElementsByTagName("transport_type").item(0).getTextContent()));
                                Log.d("search_parse_thread", thread.getElementsByTagName("uid").item(0).getTextContent());
                                segmen.getThread().setUid(thread.getElementsByTagName("uid").item(0).getTextContent());
                                Log.d("search_parse_thread", "#" + i + " non     " + thread.getChildNodes().item(4).getTextContent());
                                segmen.getThread().setTitle(thread.getChildNodes().item(4).getTextContent());
                                Log.d("search_parse_thread", thread.getElementsByTagName("vehicle").item(0).getTextContent());
                                segmen.getThread().setVehicle(thread.getElementsByTagName("vehicle").item(0).getTextContent());
                                Log.d("search_parse_thread", thread.getElementsByTagName("number").item(0).getTextContent());
                                segmen.getThread().setNumber(thread.getElementsByTagName("number").item(0).getTextContent());
                                Log.d("search_parse_thread", thread.getElementsByTagName("express_type").item(0).getTextContent());
                                segmen.getThread().setExpressType(Yandex.setExpressType(thread.getElementsByTagName("express_type").item(0).getTextContent()));
                                Log.d("search_parse_thread", thread.getElementsByTagName("thread_method_link").item(0).getTextContent());
                                segmen.getThread().setThreadMethodLink(thread.getElementsByTagName("thread_method_link").item(0).getTextContent());

                                Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                                segmen.getThread().getTransportSubtype().setColor(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                                Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                                segmen.getThread().getTransportSubtype().setCode(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                                Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());
                                segmen.getThread().getTransportSubtype().setTitle(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());

                                try {
                                    Element carrier = (Element) thread.getElementsByTagName("carrier").item(0);
                                    Log.d("search_parse", carrier.getElementsByTagName("code").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setCode(carrier.getElementsByTagName("code").item(0).getTextContent());
                                    Log.d("search_parse_title_carrier", carrier.getElementsByTagName("title").item(0).getChildNodes().item(0).getTextContent());
                                    segmen.getThread().getCarrier().setTitle(carrier.getElementsByTagName("title").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("url").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setUrl(carrier.getElementsByTagName("url").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("phone").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setPhone(carrier.getElementsByTagName("phone").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("address").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setAddress(carrier.getElementsByTagName("address").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("email").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setEmail(carrier.getElementsByTagName("email").item(0).getTextContent());
                                    Log.d("search_parse", "http:" + carrier.getElementsByTagName("logo").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setLogo("http:" + carrier.getElementsByTagName("logo").item(0).getTextContent());
                                    Log.d("search_parse", carrier.getElementsByTagName("contacts").item(0).getTextContent());
                                    segmen.getThread().getCarrier().setContacts(carrier.getElementsByTagName("contacts").item(0).getTextContent());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("search_parse", "carrier пустое поле");
                                }

                                Element to = (Element) segment.getElementsByTagName("to").item(0);
                                Log.d("search_parse", to.getElementsByTagName("code").item(0).getTextContent());
                                segmen.getTo().setCode(to.getElementsByTagName("code").item(0).getTextContent());
                                Log.d("search_parse", to.getElementsByTagName("station_type").item(0).getTextContent());
                                segmen.getTo().setStationType(Yandex.setTypeStation(to.getElementsByTagName("station_type").item(0).getTextContent()));
                                Log.d("search_parse", to.getElementsByTagName("title").item(0).getTextContent());
                                segmen.getTo().setTitle(to.getElementsByTagName("title").item(0).getTextContent());
                                Log.d("search_parse", to.getElementsByTagName("popular_title").item(0).getTextContent());
                                segmen.getTo().setPopularTitle(to.getElementsByTagName("popular_title").item(0).getTextContent());
                                Log.d("search_parse", to.getElementsByTagName("transport_type").item(0).getTextContent());
                                segmen.getTo().setTransportType(Yandex.setTransportType(to.getElementsByTagName("transport_type").item(0).getTextContent()));
                                Log.d("search_parse", to.getElementsByTagName("station_type_name").item(0).getTextContent());
                                segmen.getTo().setStationTypeName(to.getElementsByTagName("station_type_name").item(0).getTextContent());
                                Log.d("search_parse", to.getElementsByTagName("type").item(0).getTextContent());
                                segmen.getTo().setType(Yandex.setType(to.getElementsByTagName("type").item(0).getTextContent()));

                                segmen.setId(i);
                                segmen.setStatus("в пути");
                                segmen.getTransportView().setThumb(Yandex.getThumb(segmen.getThread().getTransportType()));
                                segmen.getTransportView().setStatusDrive(segmen.getThread().getTransportType());

                                searchapiD.getSegments().add(segmen);
                                // searchAPI.getSegments().add(segmen);
                            }

                            dbAdapter.createTableV2(searchReysReverse);
                            dbAdapter.insertRaspTableV2(searchReysReverse, searchapiD);

                            titleMessage = "готово";
                            Log.d("SearchAPI", searchapiD.getSegments().toString());
                        } else titleMessage = "нет такого маршрута";
                    } else
                        titleMessage = "ошибка...";
                    pr.dismiss();
                    break;
            }

            updateView();

            TastyToast.makeText(context, titleMessage, Toast.LENGTH_SHORT, TastyToast.SUCCESS).show();
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int position = -1;
        try {
            position = ((AdapterListSearch) recycler.getAdapter()).getPosition();
        } catch (Exception e) {
            Log.d(TAG, e.getLocalizedMessage(), e);
            return super.onContextItemSelected(item);
        }
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_thread_link:
                intent = new Intent(getApplicationContext(), ActivityThread.class);
                intent.putExtra("threadLink", searchAPI.getSegments().get(position).getThread().getThreadMethodLink());
                startActivity(intent);
                //Toast.makeText(getApplicationContext(), "threadLink " + position + searchAPI.getSegments().get(position).getThread().getTitle(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_options_reys:
                intent = new Intent(getApplicationContext(), DialogInfoReys.class);
                intent.putExtra("contact", searchAPI.getSegments().get(position).getThread().getCarrier().getContacts());
                intent.putExtra("threadTitle", searchAPI.getSegments().get(position).getThread().getTitle());
                intent.putExtra("logo", searchAPI.getSegments().get(position).getThread().getCarrier().getLogo());
                intent.putExtra("searchToType", searchAPI.getSearch().getToSearch().getType());
                intent.putExtra("searchToTitle", searchAPI.getSearch().getToSearch().getTitle());
                intent.putExtra("searchFromTitle", searchAPI.getSearch().getFromSearch().getTitle());
                intent.putExtra("searchFromType", searchAPI.getSearch().getFromSearch().getType());

                intent.putExtra("toStationStationType", searchAPI.getSegments().get(position).getTo().getStationType());
                intent.putExtra("toStationStationTypeName", searchAPI.getSegments().get(position).getTo().getStationTypeName());

                Log.d("stationstationtype", "" + searchAPI.getSegments().get(position).getTo().getStationType());

                intent.putExtra("toStationTitle", searchAPI.getSegments().get(position).getTo().getTitle());
                intent.putExtra("toStationStationTransportType", searchAPI.getSegments().get(position).getTo().getTransportType());

                intent.putExtra("fromStationStationType", searchAPI.getSegments().get(position).getFrom().getStationType());
                intent.putExtra("fromStationStationTypeName", searchAPI.getSegments().get(position).getFrom().getStationTypeName());
                intent.putExtra("fromStationTitle", searchAPI.getSegments().get(position).getFrom().getTitle());
                intent.putExtra("fromStationStationTransportType", searchAPI.getSegments().get(position).getFrom().getTransportType());

                intent.putExtra("transportTitle", searchAPI.getSegments().get(position).getThread().getTransportSubtype().getTitle());
                intent.putExtra("transportColor", searchAPI.getSegments().get(position).getThread().getTransportSubtype().getColor());
                intent.putExtra("transportType", searchAPI.getSegments().get(position).getThread().getTransportType());

                intent.putExtra("days", searchAPI.getSegments().get(position).getDays());
                intent.putExtra("url", searchAPI.getSegments().get(position).getThread().getCarrier().getUrl());
                intent.putExtra("adress", searchAPI.getSegments().get(position).getThread().getCarrier().getAddress());
                intent.putExtra("titleCarrier", searchAPI.getSegments().get(position).getThread().getCarrier().getTitle());
                intent.putExtra("vehicle", searchAPI.getSegments().get(position).getThread().getVehicle());
                intent.putExtra("threadTitle", searchAPI.getSegments().get(position).getThread().getTitle());

                startActivity(intent.setFlags(FLAG_ACTIVITY_NEW_TASK));
            break;

            case R.id.action_options_alarm_clock:

                final int pos = position;
                final Dialog dialog = new Dialog(this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                View v = LayoutInflater.from(getApplicationContext()).inflate(R.layout.alarmclock, null);
                final TextView info = v.findViewById(R.id.info);
                final NumberPicker numberPicker = v.findViewById(R.id.picker);
                numberPicker.setMinValue(1);
                numberPicker.setMaxValue(60);
                numberPicker.setClickable(true);
                numberPicker.setWrapSelectorWheel(false);
                numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                        info.setText("Сообщить за " + numberPicker.getValue() + " мин. до отправления?");
                    }
                });

                Button button = v.findViewById(R.id.but);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("AlarmClock", "piker : " + numberPicker.getValue());
                        setAlarmClock(searchAPI.getSegments().get(pos), pos, numberPicker.getValue());
                        dialog.dismiss();
                    }
                });

                dialog.setContentView(v);
                dialog.show();
                break;
        }
        return super.onContextItemSelected(item);
    }

    void setAlarmClock(Yandex.SearchAPI.Segments segments, int position, int valuePost)
    {
        int[] time = timeStrongToint(segments.getDeparture());

        Calendar notifyTime = Calendar.getInstance();

        notifyTime.set(Calendar.SECOND, Format.getTimeAlarmClock(time, valuePost));;

        Intent in = new Intent(this, NotificationReceiver.class);

        in.putExtra("AlarmClock_NameReys", segments.getThread().getTitle());
        in.putExtra("AlarmClock_StatusReys", "отправление через " + valuePost + " мин.");
        in.putExtra("positionItemRecycler", position);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, in, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, notifyTime.getTimeInMillis(), notifyTime.getTimeInMillis(), pendingIntent);

        TastyToast.makeText(getApplicationContext(), "Уведомление предупредит за " + valuePost + " мин. до отправления", TastyToast.LENGTH_SHORT, TastyToast.INFO);
    }
}