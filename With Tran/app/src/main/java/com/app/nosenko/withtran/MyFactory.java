package com.app.nosenko.withtran;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;
import java.util.List;

import Model.Widget;

/**
 * Created by eminem on 31.12.2017.
 */

public class MyFactory implements RemoteViewsService.RemoteViewsFactory {
    public static ArrayList<Widget> data;
    private Context context;
    private int widgetID;
    private DBAdapter dbAdapter;
    private boolean isGetDataDb = true;
    public static List<Widget> widgets;
    public static MyProvider.Status status;
    private ImageView swt;
    private LinearLayout LLswt;

    public MyFactory() {
    }

    MyFactory(Context ctx, Intent intent) {
        context = ctx;
        widgetID = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    @Override
    public void onCreate() {

        data = new ArrayList<>();
        dbAdapter = new DBAdapter(context);
        status = new MyProvider.Status();

        if (isGetDataDb) {
            getDataDb();
        }
    }

    public void setData(ArrayList<Widget> data) {
        MyFactory.data = data;
    }

    public List<Widget> getWidgets() {
        return widgets;
    }

    public void setWidgets(List<Widget> widgets) {
        MyFactory.widgets = widgets;
    }

    public void clearData()
    {
        data.clear();
        widgets.clear();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static ArrayList<Widget> getData() {
        return data;
    }

    @Override
    public RemoteViews getLoadingView() {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.default_item_widget);
        return remoteViews;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews rView = new RemoteViews(context.getPackageName(), R.layout.item_widget);
        rView.setTextViewText(R.id.nameStation, data.get(position).getNameReys());
        rView.setTextViewText(R.id.fromTime, data.get(position).getFromTime());
        rView.setTextViewText(R.id.toTime, data.get(position).getToTime());
        rView.setTextViewText(R.id.status, data.get(position).getStatus());
        rView.setTextColor(R.id.status, data.get(position).getColorStatus());
        rView.setTextViewText(R.id.duration, data.get(position).getDuration());
        rView.setImageViewResource(R.id.img, data.get(position).getIdImg());

        Intent clickIntent = new Intent();
        clickIntent.putExtra(MyProvider.ITEM_POSITION, data.get(position).getNameReys() + "\nОтправления: " + data.get(position).getDays());
        rView.setOnClickFillInIntent(R.id.LinerLMain, clickIntent);
        return rView;
    }

    public void updateStatusOneItem() {
        if (!data.isEmpty())
            data.get(0).setColorStatus(genericColor(MyProvider.Status.secondFromTime(data.get(0).getFromTime())));
    }

    public int genericColor(int time) {//510


//        int endColor = Color.rgb(255, 0, 0);
//        int resultColor = Color.rgb(0, 255, 0);
//        int colorStatus = R.color.red;

        int resultColor = context.getResources().getColor(R.color.colorWstatusGreen);
        if (time <= 30) {
            resultColor = context.getResources().getColor(R.color.colorWstatusGreen);
        }

        if ((time <= 20)) {
            resultColor = context.getResources().getColor(R.color.colorWstatusOrange);
        }

        if (time <= 10) {
            resultColor = context.getResources().getColor(R.color.colorWStatusRed);
        }

        return resultColor;
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    public void getDataDb() {
        widgets = new ArrayList<>();
        try {
            widgets = dbAdapter.getRecordRaspTable(dbAdapter.recordhistoryRasp());

            for (int i = 0; i < widgets.size(); i++) {
                data.add(widgets.get(i));
                data.get(i).setColorStatus(context.getResources().getColor(R.color.colorWstatusGreen));
            }
            isGetDataDb = false;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("widget", "non getDB");
        }


    }

    public static void getDataDb2(Context context) {
        try {
            data.clear();
            for (int i = 0; i < widgets.size(); i++) {
                data.add(widgets.get(i));
                data.get(i).setColorStatus(context.getResources().getColor(R.color.colorWstatusGreen));
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("widget", "non getDB");
        }


    }

    public void sort() {

    }

    @Override
    public void onDataSetChanged() {
        data.clear();

        for (int i = 0; i < widgets.size(); i++) {
            widgets.get(i).setStatus(status.statusBusV2(widgets.get(i).getFromTime()));
            Log.d("MyFactory", String.valueOf(status.isAdd));
            if (status.isAdd)
                data.add(widgets.get(i));
        }
        updateStatusOneItem();
    }

    public static void onChangeNotify()
    {
        data.clear();

        for (int i = 0; i < widgets.size(); i++) {
            widgets.get(i).setStatus(status.statusBusV2(widgets.get(i).getFromTime()));
            Log.d("MyFactory", String.valueOf(status.isAdd));
            if (status.isAdd)
                data.add(widgets.get(i));
        }

    }

    @Override
    public void onDestroy() {

    }
}
