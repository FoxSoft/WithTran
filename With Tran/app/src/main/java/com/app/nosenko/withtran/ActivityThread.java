package com.app.nosenko.withtran;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.sdsmdg.tastytoast.TastyToast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import Adapter.AdapterThread;

public class ActivityThread extends AppCompatActivity {

    public String[] checkCatsName;
    Context context;
    private DBAdapter dbAdapter;
    private Download download;
    private AutoCompleteTextView editNumber;
    private List<InfoUidReys> infoUidReysList;
    private ExpandableListView expandableListView;
    private Yandex.ThreadAPI threadAPI;
    private AdapterThread adapterThread;
    public boolean[] mCheckedItems;

    private String threadLink = null;
    private String constAtriuteUrl = "&apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml&limit=5000";
    private Thread myThread;

    private TextView nameReys;
    private TextView departure;
    private TextView data;
    private TextView days;
    private TextView number;
    private ProgressDialog pr;
    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbAdapter = new DBAdapter(getApplicationContext());
        editNumber = findViewById(R.id.searchNumberReys);

        final Bitmap original = BitmapFactory.decodeResource(getResources(), R.drawable.clear);
        final Bitmap b = Bitmap.createScaledBitmap(original, 40, 28, false);
        final Drawable x = new BitmapDrawable(getResources(), b);
        x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());

        editNumber.setCompoundDrawables(null, null, editNumber.getText().toString().equals("") ? null : x, null);
        editNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (editNumber.getCompoundDrawables()[2] == null) {
                    return false;
                }
                if (event.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }
                if (event.getX() > editNumber.getWidth() - editNumber.getPaddingRight() - x.getIntrinsicWidth()) {
                    editNumber.setText("");
                    editNumber.setCompoundDrawables(null, null, null, null);
                }
                return false;
            }
        });
        editNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editNumber.setCompoundDrawables(null, null, editNumber.getText().toString().equals("") ? null : x, null);
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        nameReys = findViewById(R.id.nameStation);
        departure = findViewById(R.id.departure);
        data = findViewById(R.id.arrival);
        days = findViewById(R.id.days);
        number = findViewById(R.id.number);

        expandableListView = findViewById(R.id.expan);
        //    stationThreads = new ArrayList<>(expListDetail.keySet());

        pr = new ProgressDialog(ActivityThread.this);
        pr.setTitle("Загрузка станций");
        pr.setMessage("Соединение с сервером...");
        pr.setCancelable(false);
        pr.setIndeterminate(false);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TastyToast.makeText(getApplicationContext(), "Поиск скоро будет доступен!", TastyToast.LENGTH_SHORT,
                        TastyToast.INFO).show();
//                String string2 = editNumber.getText().toString();
//                if (!string2.isEmpty()) {
//                    infoUidReysList = dbAdapter.findeUIDandNameReys(string2);
//                    if (!infoUidReysList.isEmpty()) {
//                        mCheckedItems = new boolean[infoUidReysList.size()];
//                        checkCatsName = new String[infoUidReysList.size()];
//                        for (int i = 0; i < infoUidReysList.size(); ++i) {
//                            checkCatsName[i] = (infoUidReysList.get(i)).getNameReys();
//                            mCheckedItems[i] = false;
//                        }
//                        showDialog(3);
//                        return;
//                    }
//                    Snackbar.make(view, "\u041d\u0435\u0442 \u0442\u0430\u043a\u043e\u0433\u043e \u043d\u043e\u043c\u0435\u0440\u0430 \u0432 \u0431\u0430\u0437\u0435...",
//                            Snackbar.LENGTH_SHORT).setAction((CharSequence) "Action", null).show();
//                    return;
//                }
//                StatusStation statusStation = new StatusStation();
//                Log.d("timeThread", statusStation.getStatusStation("09:10", "60.0"));
//                Log.d("timeThread", String.valueOf(statusStation.getIntDuration("0.0")));
//                Snackbar.make(view, "\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043d\u043e\u043c\u0435\u0440 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430",
//                        Snackbar.LENGTH_SHORT).setAction((CharSequence) "Action", null).show();
            }
        });


//        if (Download.isOnline2(getApplicationContext())) {
//            download.execute();
//            refresh();
//        } else Log.d("schedule_parse", "no net");

//        getBundle();

        Log.d("thread_db", dbAdapter.getRaspTables().toString());

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                setListViewHeight(expandableListView, i);
                return false;
            }
        });


        threadAPI = new Yandex.ThreadAPI();
        //init();

        adapterThread = new AdapterThread(getApplicationContext(), threadAPI);
        expandableListView.setAdapter(adapterThread);
        registerForContextMenu(expandableListView);

//        MobileAds.initialize(this, getResources().getString(R.string.YOUR_ADMOB_APP_ID));
//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("2276B596B2758694F89FA2AACDB23424").build();
//        mAdView.loadAd(adRequest);

        getBundle();


        // threadNotifyRecycler();
    }

    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void init() {
        if (threadAPI != null) {
            departure.setText(threadAPI.getStartTime());
            number.setText(threadAPI.getNumber());
            nameReys.setText(threadAPI.getTitle());
            days.setText(threadAPI.getDays());
            data.setText(threadAPI.getStartDate());
        }

    }

    private void getBundle() {
        threadLink = getIntent().getStringExtra("threadLink");
        if (threadLink != null) {
            // threadLink += constAtriuteUrl;
            final Download download = new Download(getApplicationContext(), threadLink, constAtriuteUrl);

//            final ThreadDownload threadDownload = new ThreadDownload(download);
//            new Thread(threadDownload,"MyThread").start();
//            Handler handler = new Handler();
//            Runnable runnable = new Runnable() {
//                @Override
//                public void run() {
//                    Log.d("thread_list", "+");
//                    if (download.isSet)
//                    {
//                        Log.d("thread_list", download.threadAPI.getStops().toString());
//                    }
//
//                }
//            };
//
//
//            runnable.run();
//            handler.postDelayed(runnable, 1000);
//            //handler.removeCallbacks(runnable);

            if (Download.isOnline2(getApplicationContext())) {
                download.execute();
                pr.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (true) {
                            Log.d("thread_list", "" + 1);
                            if (download.isSet) {
                                threadAPI.clearAll();
                                threadAPI = download.threadAPI;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapterThread = new AdapterThread(getApplicationContext(), threadAPI);
                                        expandableListView.setAdapter(adapterThread);
                                        init();
                                        refresh();
                                        pr.dismiss();
                                    }
                                });
                                break;
                            }
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        Log.d("thread_list", "runable");
                    }
                }).start();
                Log.d("thread_list", "runable");
            } else
                TastyToast.makeText(getApplicationContext(), "нет интернета...", Toast.LENGTH_SHORT, TastyToast.ERROR).show();
        }
    }

    private static class StatusStation {
        private String statusTitle;
        private String colorBackground;


        public String getStatusStation(String startTime, String duration) {
            Time t = new Time();
            t.setToNow();
            int contrTime = (getIntDuration(duration) + Format.getSecFromTime(startTime));
            int[] timeFrom = Format.getSecFromTime(contrTime);
            t.set(0, timeFrom[0], timeFrom[1], 0, 0, 0);

            Time tNow = new Time(Time.getCurrentTimezone());
            tNow.setToNow();

            int result = (t.minute + t.hour * 60) - (tNow.hour * 60 + tNow.minute);
            int hour = Format.FormatSecToTime(result).get(0);
            int minute = Format.FormatSecToTime(result).get(1);

            Log.d("timeThread", String.valueOf(result));
            Log.d("timeThread", String.valueOf(hour) + " " + minute);

            if (result > 0) {
                String str;
                colorBackground = "#ff7676";

                StringBuilder append = new StringBuilder().append("прибудет через ").append(hour == 0 ? "" : String.valueOf(hour) + " ч. ");
                append.append(minute == 0 ? "" : String.valueOf(minute) + " мин.");

                if (hour == 0) {
                    if (minute <= 1 && minute >= 1) {
                        colorBackground = "#feff76";
                        return "прибудет через " + String.valueOf(minute) + " мин.";
                    }
                }
                return append.toString();
            } else {

                if (result >= -1) {
                    colorBackground = "#feff76";
                    return "проехали " + String.valueOf(minute < 0 ? -minute : minute) + " мин. назад";
                } else {
                    colorBackground = "#76ff99";
                    return "проехали";
                }
            }
        }

        private int getIntDuration(String duration) {

            if (duration.lastIndexOf(".") != -1) {
                //  Log.d("timeThread", duration.substring(0, duration.indexOf(".")));
                return Integer.valueOf(duration.substring(0, duration.indexOf(".")));
            } else return Integer.valueOf(duration);
        }

        private int getMilSecFromTime(String time) {
            int[] result = new int[0];
            String[] str = time.split(":");
            int chour = Integer.parseInt(str[0]);
            int minute = Integer.parseInt(str[1]);
            return chour * 3600000 + minute * 60000;
        }

        public String getColorBackground() {
            return colorBackground;
        }
    }


    private void threadNotifyRecycler() {
        threadLink = getIntent().getStringExtra("threadLink");
        if (threadLink != null) {
            // threadLink += constAtriuteUrl;
            final Download download = new Download(getApplicationContext(), threadLink, constAtriuteUrl);
            download.execute();
            myThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!Thread.currentThread().isInterrupted()) {
                        Log.d("thread_list+", "Привет из потока " + Thread.currentThread().getName());

                        if (download.isSet) {
                            Log.d("thread_list+", String.valueOf(download.threadAPI.getStops().size()));

                            threadAPI = download.threadAPI;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapterThread = new AdapterThread(getApplicationContext(), threadAPI);
                                    expandableListView.setAdapter(adapterThread);
                                    init();

                                    Log.d("thread_list+", "Привет из потока runOnUiThread");
                                }
                            });

                            myThread.interrupt();

                            Log.d("thread_list+", "сдохни " + Thread.currentThread().getName());
                        }

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            return;
                        }
                    }
                }
            });
            myThread.start();
        }
    }

    protected Dialog onCreateDialog(int n) {
        switch (n) {
            default: {
                return null;
            }
            case 3:
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.bus_station);
        builder.setTitle("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u043c\u0430\u0440\u0448\u0440\u0443\u0442")
                .setCancelable(false).setMultiChoiceItems(checkCatsName, mCheckedItems, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialogInterface, int n, boolean bl) {
                mCheckedItems[n] = bl;
                infoUidReysList.get(n).setCheck(bl);
            }
        }).setPositiveButton("\u0413\u043e\u0442\u043e\u0432\u043e", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int n) {
                StringBuilder stringBuilder = new StringBuilder();
                int n2 = 0;
                do {
                    if (n2 >= infoUidReysList.size()) {
                        TastyToast.makeText(getApplicationContext(), stringBuilder.toString(), Toast.LENGTH_SHORT, TastyToast.INFO).show();
                        return;
                    }
                    stringBuilder.append("" + infoUidReysList.get(n2).getUid());
                    if (infoUidReysList.get(n2).isCheck()) {
                        //      download = new Download(infoUidReysList.get(n2).getUid());
                        //      download.execute(new String[]{""});
                        stringBuilder.append(" \u0432\u044b\u0431\u0440\u0430\u043d\n");
                    } else {
                        stringBuilder.append(" \u043d\u0435 \u0432\u044b\u0431\u0440\u0430\u043d\n");
                    }
                    ++n2;
                } while (true);
            }
        }).setNegativeButton("\u041e\u0442\u043c\u0435\u043d\u0430", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int n) {
                dialogInterface.cancel();
            }
        });
        return builder.create();
    }

    private void refresh() {

        Timer t = new Timer();
        Timer tim = new Timer();
        final StatusStation statusStation = new StatusStation();
        final DBThread thread = new DBThread(getApplicationContext(), tim);
        tim.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {

                        for (int i = 0; i < adapterThread.expListTitle.size(); i++) {

                            Log.d("timeThread", ((Yandex.ThreadAPI.Stops) adapterThread.getChild(i, 0)).getDuration());
                            adapterThread.expListTitle.get(i).setStatus(statusStation.getStatusStation(adapterThread.getStartTime(),
                                    ((Yandex.ThreadAPI.Stops) adapterThread.getChild(i, 0)).getDuration()));
                            adapterThread.expListTitle.get(i).setColorBackground(statusStation.getColorBackground());
                        }
                        Parcelable parcelable = expandableListView.onSaveInstanceState();
                        adapterThread.notifyDataSetChanged();
                        expandableListView.setAdapter(adapterThread);
                        expandableListView.onRestoreInstanceState(parcelable);
                    }
                });
            }
        }, 0, 30000);

    }

    private void updateView() {
        StatusStation statusStation = new StatusStation();
        for (int i = 0; i < adapterThread.expListTitle.size(); i++) {

            Log.d("timeThread", ((Yandex.ThreadAPI.Stops) adapterThread.getChild(i, 0)).getDuration());
            adapterThread.expListTitle.get(i).setStatus(statusStation.getStatusStation(adapterThread.getStartTime(),
                    ((Yandex.ThreadAPI.Stops) adapterThread.getChild(i, 0)).getDuration()));
            adapterThread.expListTitle.get(i).setColorBackground(statusStation.getColorBackground());
        }
        Parcelable parcelable = expandableListView.onSaveInstanceState();
        adapterThread.notifyDataSetChanged();
        expandableListView.setAdapter(adapterThread);
        expandableListView.onRestoreInstanceState(parcelable);
    }

    @Override
    protected void onPostResume() {
        if (threadAPI != null)
        {
            updateView();
        }
        super.onPostResume();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;
        int idGroup = ExpandableListView.getPackedPositionGroup(info.packedPosition);

        MenuInflater menuInflater = new MenuInflater(getApplicationContext());
        menuInflater.inflate(R.menu.popup_menu_thread, menu);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View header = inflater.inflate(R.layout.title_popmenu_thread, null);

        TextView nameStation = header.findViewById(R.id.nameStation);
        TextView typeStation = header.findViewById(R.id.typeStation);
        nameStation.setText(adapterThread.expListTitle.get(idGroup).getNameStation());
        //number.setBackground(TStatus.getBackground());
        typeStation.setText(adapterThread.expListTitle.get(idGroup).getTypeStation());
        menu.setHeaderView(header);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();

        int idGroup = ExpandableListView.getPackedPositionGroup(info.packedPosition);
        Log.e("clickd", " " + idGroup);
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_shedule_station:
                intent = new Intent(getApplicationContext(), ActivityShudle.class);
                intent.putExtra("scheduleCodeStation", adapterThread.expListTitle.get(idGroup).getCodeStation());
                startActivity(intent);
               // TastyToast.makeText(getApplicationContext(), "threadLink " + adapterThread.expListTitle.get(idGroup).getNameStation(), Toast.LENGTH_SHORT).show();
                break;

            default:
                return super.onContextItemSelected(item);
        }

        return super.onContextItemSelected(item);
    }
}
