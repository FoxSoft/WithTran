package com.app.nosenko.withtran;

import android.content.Intent;
import android.graphics.Point;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActivitySplash extends AppCompatActivity {

    private ImageView myLogo;
    private ImageView myTitle;
    private ImageView footer;
    private Animation mAnimationMyLogo, mAnimationMyTitle;
    private Animation mAnimationFooter;
    private LinearLayout linearLayout;
    private TextView copyright;
    private ImageView bus;
    private int height, width;
    private Thread myThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        myTitle = findViewById(R.id.myTitle);
        myLogo = findViewById(R.id.myLogo);
        footer = findViewById(R.id.footertitle);
        copyright = findViewById(R.id.copyright);
        bus = findViewById(R.id.bus);

        myLogo.setVisibility(View.INVISIBLE);
        myTitle.setVisibility(View.INVISIBLE);
        copyright.setVisibility(View.INVISIBLE);
        bus.setVisibility(View.INVISIBLE);

        linearLayout = findViewById(R.id.ll);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(ActivitySplash.this, ActivityMain.class);
                ActivitySplash.this.startActivity(mainIntent);
                ActivitySplash.this.finish();
            }
        });

        // подключаем файл анимации
        mAnimationMyLogo = AnimationUtils.loadAnimation(this, R.anim.anim_logo);
        mAnimationMyTitle = AnimationUtils.loadAnimation(this, R.anim.anim_title);
        mAnimationFooter = AnimationUtils.loadAnimation(this, R.anim.anim_yandex);

        mAnimationMyLogo.setAnimationListener(animationFadeMyLogoListener);
        mAnimationMyTitle.setAnimationListener(animationFadeMyTitleListener);
        mAnimationFooter.setAnimationListener(animationFadeFooterListener);

        getDisplay();
        // при запуске начинаем с анимации исчезновения
        myLogo.startAnimation(mAnimationMyLogo);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // По истечении времени, запускаем главный активити, а Splash Screen закрываем
                if (!ActivitySplash.this.isFinishing()) {
                    Intent mainIntent = new Intent(ActivitySplash.this, ActivityMain.class);
                    ActivitySplash.this.startActivity(mainIntent);
                    ActivitySplash.this.finish();
                }
            }
        }, 7500);
    }

    @Override
    protected void onPause() {
        super.onPause();
        myLogo.clearAnimation();
    }

    Animation.AnimationListener animationFadeFooterListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
//            myLogo.setVisibility(View.INVISIBLE);
//            myTitle.setVisibility(View.INVISIBLE);
            footer.startAnimation(mAnimationFooter);
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            footer.setVisibility(View.INVISIBLE);
            footer.clearAnimation();
            // myLogo.startAnimation(mAnimationMyLogo);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    Animation.AnimationListener animationFadeMyTitleListener = new Animation.AnimationListener() {

        @Override
        public void onAnimationEnd(Animation animation) {
            myTitle.clearAnimation();


            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) bus.getLayoutParams();
            params.width = 110;
            params.height = 110;
            params.weight = 0f;
            bus.setLayoutParams(params);

            TranslateAnimation translateAnimation = new TranslateAnimation(-120, width+10, 0, 0);
            translateAnimation.setDuration(2000);
            translateAnimation.setFillAfter(true);
            bus.setAnimation(translateAnimation);

            myThread = new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Thread.sleep(1400);
                    } catch (InterruptedException e) {
                        return;
                    }
                    Log.d("myThread+", "Привет из потока " + Thread.currentThread().getName());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                footer.startAnimation(mAnimationFooter);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Log.d("myThread+", "Привет из потока runOnUiThread");

                        }
                    });


                    Log.d("myThread+", "сдохни " + Thread.currentThread().getName());
                    myThread.interrupt();
                }
            });

            myThread.start();

        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onAnimationStart(Animation animation) {

        }
    };

    Animation.AnimationListener animationFadeMyLogoListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            //  myLogo.startAnimation(mAnimationMyTitle);

            myTitle.startAnimation(mAnimationMyTitle);
            myTitle.setVisibility(View.VISIBLE);
            myLogo.clearAnimation();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onAnimationStart(Animation animation) {
            // TODO Auto-generated method stub
            footer.setVisibility(View.INVISIBLE);
            copyright.setVisibility(View.VISIBLE);
            myLogo.setVisibility(View.VISIBLE);
        }
    };

    private void getDisplay() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }
}
