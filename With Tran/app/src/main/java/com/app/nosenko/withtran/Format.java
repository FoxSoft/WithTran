package com.app.nosenko.withtran;

import android.text.format.Time;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by eminem on 22.10.2017.
 */

public class Format {
    public static String FormatDurationTotime(String duration) {
        int sec = (int) Float.parseFloat(duration);
        int hour = sec / 3600;
        int minute = (int) (0.016666666667D * (double) (sec - hour * 3600));
        return hour == 0 ? minute + " мин." : hour + " ч. " + minute + " мин.";
    }

    public static int FormatDurationToInt(String duration) {

        int hour = 0, minute = 0;
        int index = duration.indexOf(" ч. ");
        if (index != -1) {
            hour = Integer.valueOf(duration.substring(0, index));
            duration = duration.replace(duration.substring(0, index + 4), "");
        }

        Log.d("sorting", " " + duration);
        index = duration.indexOf(" мин.");

        if (index != -1) {
            minute = Integer.valueOf(duration.substring(0, index));
        }
        Log.d("sorting", " " + duration);
        return hour * 3600 + minute * 60;
    }

    public static List<Integer> FormatSecToTime(Integer time) {
        List<Integer> result = new ArrayList();
        Integer hour = time / 60;
        Integer minute = time - (hour * 60);
        result.add(hour);
        result.add(minute);
        return result;
    }

    public static int getSeconds(int[] time) {
        return time[0] * 3600 + time[1] * 60;
    }


    public static int getSecFromTime(String time) {
        int[] result = new int[0];
        String[] str = time.split(":");
        int chour = Integer.parseInt(str[0]);
        int minute = Integer.parseInt(str[1]);
        return chour * 3600 + minute * 60;
    }

    public static int[] getSecFromTime(int timeSeconds) {
        int[] result = new int[2];
        Integer hour = timeSeconds / 60;
        Integer minute = timeSeconds - (hour * 60);
        result[0] = hour;
        result[1] = minute;
        return result;
    }

    public static int getTimeAlarmClock(int[] time, int valuePost) {
        Time timeAlarm = new Time(Time.getCurrentTimezone());
        timeAlarm.setToNow();
        timeAlarm.set(0, time[1], time[0], 0, 0, 0);

        Calendar calendar = Calendar.getInstance();

        int result = (timeAlarm.hour * 3600 + timeAlarm.minute * 60);
        return result - (calendar.getTime().getHours() * 3600 + (calendar.getTime().getMinutes() * 60)) - valuePost * 60;
    }

    public static String getShortTime(String time) {
        int[] result = new int[0];
        String[] str = time.split(":");
        int chour = Integer.parseInt(str[0]);
        int minute = Integer.parseInt(str[1]);
        return str[0] + ":" + str[1];
    }

    public static String getNameStatuionOfNameReys(String nameReys, Yandex.SEARCH_RASP searchRasp) {
        String[] str = nameReys.split("_");
        String result = "";
        switch (searchRasp) {
            case to_from:
                result = str[1];
                break;
            case from_to:
                result = str[0];
                break;
        }

        return result;
    }

    public static String getCompareNameReys(String nameReys, String separator) {
        String[] str = nameReys.split(separator);
        String result = "";
        result = str[0] + "_" + str[1];
        return result;
    }

    public static boolean isEmptyOrNull(String s) {
        boolean result = false;
        try {
            if (s == null | s.isEmpty())
                result = true;
        } catch (Exception e) {
            result = true;
            e.printStackTrace();
        }

        return result;
    }
}