package com.app.nosenko.withtran;

/**
 * Created by eminem on 01.03.2018.
 */

public class ThreadDownload implements Runnable {

    public boolean isActive;
    private Download download;

    void disable(){
        isActive=false;
    }

    ThreadDownload(Download download){
        this.download = download;
     //   isActive = download.isSet;
    }

    public void run(){

        System.out.printf("Поток %s начал работу... \n", Thread.currentThread().getName());
        int counter=1; // счетчик циклов
        while(!download.isSet){
            System.out.println("Цикл " + counter++);
            try{
                Thread.sleep(500);
            }
            catch(InterruptedException e){
                System.out.println("Поток прерван");
            }
        }
        isActive = true;
        System.out.printf("Поток %s завершил работу... \n", Thread.currentThread().getName());
    }
}