package com.app.nosenko.withtran;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.sdsmdg.tastytoast.TastyToast;
import com.xw.repo.BubbleSeekBar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import Adapter.AdapterSettlement;
import DrawableStyle.TransportDrawable;
import Model.Settlement;

public class ActivityNearestSettlement extends AppCompatActivity implements LocationListener {

    private TextView lan;
    private TextView lng;
    private TextView provider;
    private ListView listView;
    private List<Settlement> settlements;
    private AdapterSettlement adapterSettlement;
    private LocationManager locationManager;
    private long tim;
    private double longitude = 0;
    private double latitude = 0;
    private String nameProvider;
    private PullRefreshLayout mPullToRefreshView;
    private BubbleSeekBar bubbleSeekBar;
    private Download download;
    private boolean isCancel = false;
    private final int FINE_LOCATION_PERMISSION = 9999;
    private AdView mAdView;
    private NestedScrollView nestedScrollView;
    private LinearLayout linearLayout;
    private AutoCompleteTextView autoCompleteTextView;
    private boolean isSearchAdressEdit = false;
    private LinearLayout linearLayoutHeader;
    private LinearLayout.LayoutParams params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearest_settlement);

        lan = findViewById(R.id.myLan);
        lng = findViewById(R.id.myLng);
        provider = findViewById(R.id.provider);
        bubbleSeekBar = findViewById(R.id.seekBar);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mPullToRefreshView = findViewById(R.id.pull_to_refresh);
        mPullToRefreshView.setRefreshDrawable(new TransportDrawable(getApplicationContext(), mPullToRefreshView, getWindowManager().getDefaultDisplay()));
        mPullToRefreshView.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        settlements.clear();
                        if (!isSearchAdressEdit) {
                            if (isOnline3() & (longitude != 0) & (latitude != 0)) {
                                download = new Download(getApplicationContext(), latitude, longitude, bubbleSeekBar.getProgress());
                                download.execute();
                                refresh();
                            }
                            mPullToRefreshView.setRefreshing(false);
                        } else {
                            if (isOnline3()) {
                                Geocoder geocoder = new Geocoder(getApplicationContext());
                                try {
                                    List<Address> addresses = new ArrayList<>();
                                    addresses = geocoder.getFromLocationName(autoCompleteTextView.getText().toString(), 100);
                                    Log.d("addresses", "" + addresses.size() + " " + addresses.get(0).getCountryName()
                                            + " " + addresses.get(0).getAddressLine(0)
                                            + " " + addresses.get(0).getAdminArea());

                                    if (!addresses.isEmpty()) {
                                        download = new Download(getApplicationContext(), addresses.get(0).getLatitude(), addresses.get(0).getLongitude(), bubbleSeekBar.getProgress());
                                        download.execute();
                                        refresh();
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
//                                DownloadLocale downloadLocale = new DownloadLocale(autoCompleteTextView.getText().toString());
//                                downloadLocale.execute();
                            } else {
                                TastyToast.makeText(getApplicationContext(), "нет интернета...", Toast.LENGTH_SHORT, TastyToast.ERROR).show();
                            }
                            mPullToRefreshView.setRefreshing(false);
                        }
                    }
                }, 3000);

                linearLayout.removeAllViews();
                linearLayout.addView(listView);
            }
        });

        nestedScrollView = findViewById(R.id.NSV);

        linearLayout = findViewById(R.id.LL);
        linearLayoutHeader = findViewById(R.id.LLHeader);

        listView = findViewById(R.id.listview);
        settlements = new ArrayList<>();
        adapterSettlement = new AdapterSettlement(getApplicationContext(), settlements);
        listView.setAdapter(adapterSettlement);

        bubbleSeekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListenerAdapter() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                bubbleSeekBar.setBubbleColor(Color.HSVToColor(new float[]{57 - (progress + 5), 10, 100}));
                bubbleSeekBar.setSecondTrackColor(Color.HSVToColor(new float[]{57 - (progress + 10), 100, 100}));
            }
        });

        createInputSearch();
        priview();
//        MobileAds.initialize(this, getResources().getString(R.string.YOUR_ADMOB_APP_ID));
//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("2276B596B2758694F89FA2AACDB23424").build();
//        mAdView.loadAd(adRequest);
    }

    private void createInputSearch() {
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        autoCompleteTextView = new AutoCompleteTextView(getApplicationContext());
        autoCompleteTextView.setLayoutParams(params);
        autoCompleteTextView.setHint("Введите адрес");
        autoCompleteTextView.setBackground(getResources().getDrawable(R.drawable.background_search));
        autoCompleteTextView.setTextColor(getResources().getColor(R.color.black));
        autoCompleteTextView.setHintTextColor(getResources().getColor(R.color.silver));
        autoCompleteTextView.setPadding(10, 5, 10, 5);

        final Bitmap original = BitmapFactory.decodeResource(getResources(), R.drawable.clear);
        final Bitmap b = Bitmap.createScaledBitmap(original, 40, 28, false);
        final Drawable x = new BitmapDrawable(getResources(), b);
        x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());

        autoCompleteTextView.setCompoundDrawables(null, null, autoCompleteTextView.getText().toString().equals("") ? null : x, null);
        autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (autoCompleteTextView.getCompoundDrawables()[2] == null) {
                    return false;
                }
                if (event.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }
                if (event.getX() > autoCompleteTextView.getWidth() - autoCompleteTextView.getPaddingRight() - x.getIntrinsicWidth()) {
                    autoCompleteTextView.setText("");
                    autoCompleteTextView.setCompoundDrawables(null, null, null, null);
                }
                return false;
            }
        });
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                autoCompleteTextView.setCompoundDrawables(null, null, autoCompleteTextView.getText().toString().equals("") ? null : x, null);
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });
//        LinearLayout.LayoutParams params = null;
//        params.setMargins(0, 10, 0, 10);
//        autoCompleteTextView.setLayoutParams(params);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_geocoder:
                if (item.isChecked()) {
                    item.setChecked(false);
                    isSearchAdressEdit = true;
                    linearLayoutHeader.addView(autoCompleteTextView, 2);
                } else {
                    item.setChecked(true);
                    isSearchAdressEdit = false;
                    linearLayoutHeader.removeView(autoCompleteTextView);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_nearest_settlement, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void priview() {
        linearLayout.removeAllViews();
        View instructionView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.priview_instructions, null);
        linearLayout.addView(instructionView);
    }

    private void refresh() {
        Timer t = new Timer();
        Date date = new Date();
        Log.d("+=+=+", date.toString());
        date.setTime(date.getTime()+ 2000);
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (download.isSet & settlements.isEmpty()) {
                    settlements.add(download.settlement);
                    adapterSettlement.notifyDataSetChanged();
                    Helper.getListViewSize(listView);
                    mPullToRefreshView.setRefreshing(false);
                    isCancel = true;
                }
                Log.d("+=+=+", "1");
            }
        };
            t.schedule(new TimerTask() {
                public void run() {
                     runOnUiThread(runnable);
                }
            }, date);
    }

    private void refresh2() {
        Runnable doBackgroundThreadProcessing = new Runnable() {
            public void run() {
                if (download.isSet & settlements.isEmpty()) {
                    settlements.add(download.settlement);
                    adapterSettlement.notifyDataSetChanged();
                    Helper.getListViewSize(listView);
                    mPullToRefreshView.setRefreshing(false);
                }
            }
        };
        Thread thread = new Thread(null, doBackgroundThreadProcessing,
                "Background");
        thread.start();
        try {
            thread.join(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    protected boolean isOnline3() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        return cm.getActiveNetworkInfo() != null;
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},FINE_LOCATION_PERMISSION);
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
    }

    @Override
    public void onLocationChanged(Location location) {

        getLocation(location);
    }

    void getLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            formatLocation(location);
        } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
            formatLocation(location);
        }

    }

    private void formatLocation(Location location) {
        longitude = location.getLongitude();
        Log.d("locale_lon", String.valueOf(location.getLongitude()));
        lng.setText(String.valueOf(location.getLongitude()));
        latitude = location.getLatitude();
        Log.d("locale_lan", String.valueOf(location.getLatitude()));
        lan.setText(String.valueOf(location.getLatitude()));
        nameProvider = location.getProvider();
        provider.setText(nameProvider);
        provider.setTextColor(getResources().getColor(R.color.lime));
        tim = location.getTime();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        provider.setText(nameProvider);
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
