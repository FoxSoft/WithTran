package com.app.nosenko.withtran;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import Adapter.AdapterHelp;
import Model.Help;

public class ActivityHelp extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AdapterHelp adapterHelp;
    private List<Help> helps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        init();
        recyclerView = findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        adapterHelp = new AdapterHelp(getApplicationContext(), helps);
        recyclerView.setAdapter(adapterHelp);
    }

    private void init() {
        helps = new ArrayList<>();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 180);

        helps.add(new Help("Транспорт", "транспорт такого цвета означает, что он будет отъезжать от станции", R.drawable.buses_green, params));
        helps.add(new Help("Транспорт", "транспорт такого цвета означает, что он уже ушёл и сейчас в пути", R.drawable.buses_yellow, params));
        helps.add(new Help("Транспорт", "транспорт такого цвета означает, что он уже ушёл", R.drawable.buses_red, params));

        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        helps.add(new Help("Поиск расписания", "На окне приложения поиска распиний есть 2 поля:\n1-откуда едите.\n2-куда приедите." +
                "\nПри выборе пунктов отправления и прибытия опредитляется их коды. Например, если код с буквой \"c\": c454654 - это код населенного пункта. " +
                "Код с буквой \"s\" - это станция. Если Вы хотитенайти все возможные варианты добрать из города А в город B, то используйте коды с буквой \"c\"." +
                " Или можете использовать код с буквой \"s\" - если вам нужен конкретный тип транспорта и конкретная остановка. Например из Автовокзала А в Автовокзал B, " +
                "у каждого пункта будет код с буквой \"s\".", R.drawable.gif_search, params));

        helps.add(new Help("Поиск расписания", "Когда Вы ищите нужный маршрут, необходимо указать откуда едите и куда. При наборе названия вы можете указать " +
                "конкретное название отсановки, если название знаете. Или же выбрать остановку из предложенных, наприимер, \"Москва\", будет большой выбор станций и их тпы будут различны. " +
                "Если выбирите станцию \"Москва\" без типа, то вам будут предоставлены все варанты добрать до этой станции (автобусом, поездом и тд.).", R.drawable.type_station, params));

        helps.add(new Help("Ближайшие станции, населённый пункт", "В главном окне приложения есть боковое меню, справа наверху. " +
                "В этом меню есть пукты: \"ближайший населённый пункт\", \"ближайшие станции\". Их функционал относительно одинаков, после выставления в " +
                "нужном виде управляющих элементов необходимо выполнить на дисплее смартфона жест: \"потянуть вниз\", тогда Вы увидите соотвествующую информацию " +
                "по вашему запросу.", R.drawable.gif_gps, params));


    }
}
