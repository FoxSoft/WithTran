package com.app.nosenko.withtran;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.ui.IconGenerator;
import com.sdsmdg.tastytoast.TastyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import Model.NearestStation;
import map.Station;
import map.*;


public class ActivityMaps extends FragmentActivity implements RoutingListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private GoogleMap mMap;
    private GoogleApiClient googleAPIClient;
    private double lat, lng;
    private Location location;
    public Marker markerMy;
    private LocationRequest locationRequest;
    private Station stationPack;
    private List<Station.Stations> stationsList;


    private static final int MY_PERMISSION_CODE = 1000;
    private BottomNavigationView bottonNavigation;
    private LatLng latLngMy;
    private LatLng latLngTo;
    private MarkerOptions markerOptionsMy;
    private Polyline lineDistance;
    private PolylineOptions polylineOptionsMyMoveTo;
    private List<LatLng> listPoint = new ArrayList<>();
    private List<Marker> markersStations = new ArrayList<>();
    private Polyline polylineRide;
    private Thread myThread;
    private PolylineOptions polylineOptionsMyMoveToMarkStation;
    private Polyline lineDistanceMarkStation;
    private Marker markerStation;
    private MarkerOptions markerOptionsStation;
    private PolylineOptions polylineOptionsMark = null;
    private Marker markerSelected = null;
    private List<Polyline> polylines = new ArrayList<>();
    private Polyline lineDistanceRadius;
    //============================
    private View viewInfoStation;
    private ImageView imgTypeStation;
    private TextView titleLine1;
    private TextView titleLine3;
    private TextView titleLine2;
    private IconGenerator iconFactory;
    private String nameStation;
    private String typeStation;
    private String typeTransport;
    private AlertDialog.Builder dialogStyleMap;
    private String[] styleMap = {"карта", "спутник", "гибрд", "местность"};
    private String distanceRadius;
    private TextView titleLineRad;
    private ProgressDialog progressDialog;
    private Thread allStThread;
    private boolean isRote = false;

    private enum COLORS_ROUTE {
        line_1("зелёный", R.color.line_1), line_2("оранжевый", R.color.line_2), line_3("красный", R.color.line_3);
        private String title;
        private int idColor;

        COLORS_ROUTE(String title, int idColor) {
            this.title = title;
            this.idColor = idColor;
        }

        public String getTitle() {
            return title;
        }

        public int getIdColor() {
            return idColor;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        bottonNavigation = findViewById(R.id.bottonNavig);
        bottonNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.actionBliz:
                        nearByPlace("bus_station");
                        Log.d("tab", "all station");
                        break;
                    case R.id.actionMarks:
                        route();
                        break;
                    case R.id.actionStyleMap:
                        dialogStyleMap.show();
                        break;
                }
                return true;
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Поиск маршрута");
        progressDialog.setMessage("соединение...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(true);

        dialogStyleMap = new AlertDialog.Builder(this).setTitle("Стиль карты").setCancelable(true)
                .setIcon(getResources().getDrawable(R.drawable.icon_map))
                .setItems(styleMap, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                break;
                            case 1:
                                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                                break;
                            case 2:
                                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                                break;
                            case 3:
                                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                                break;
                        }
                    }
                })
                .setNeutralButton("ок", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.cancel();
                    }
                });

        intiView();
        progressDialog.show();

        getBundels();
    }

    private void getBundels() {
        Bundle extras = getIntent().getExtras();

        if (extras.containsKey("nearestAllStation")){
            allStThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!Thread.currentThread().isInterrupted()) {
                        Log.d("myThreadAllS", "Привет из потока " + Thread.currentThread().getName());

                        if (!progressDialog.isShowing() & latLngMy != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        nearByPlace("bus_station");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Log.d("myThreadAllS", "Привет из потока runOnUiThread");
                                }
                            });
                            allStThread.interrupt();

                            Log.d("myThreadAllS", "сдохни " + Thread.currentThread().getName());
                        }
                        else {

                        }

                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            return;
                        }
                    }
                }
            });

            allStThread.start();
        }
    }

    private void intiView() {
        iconFactory = new IconGenerator(this);
        viewInfoStation = getLayoutInflater().inflate(R.layout.marker, null);
        imgTypeStation = viewInfoStation.findViewById(R.id.imgTypeStation);
        titleLineRad = viewInfoStation.findViewById(R.id.titleLineRad);
        titleLine1 = viewInfoStation.findViewById(R.id.titleLine1);
        titleLine2 = viewInfoStation.findViewById(R.id.titleLine2);
        titleLine3 = viewInfoStation.findViewById(R.id.titleLine3);
    }

    private void createMarkerMy() {
        markerOptionsMy = new MarkerOptions();
        latLngMy = new LatLng(location.getLatitude(), location.getLongitude());
        markerOptionsMy.position(latLngMy);
        markerOptionsMy.title("вы здесь");
        markerOptionsMy.icon(BitmapDescriptorFactory.fromResource(R.mipmap.placeholder));
        markerMy = mMap.addMarker(markerOptionsMy);
    }

    private GoogleMap.OnMarkerClickListener createOnClickMarker() {
        GoogleMap.OnMarkerClickListener onMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker != markerMy) {
                    listPoint.clear();
                    listPoint.add(latLngMy);
                    listPoint.add(marker.getPosition());

                    try {
                        polylineOptionsMyMoveTo.getPoints().clear();
                        lineDistance.remove();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    polylineOptionsMyMoveTo = new PolylineOptions()
                            .addAll(listPoint).width(1).clickable(true)
                            .color(Color.GREEN).clickable(true).geodesic(true);

                    Log.d("GPS locale", polylineOptionsMyMoveTo.getPoints().size() + "");

                    lineDistance = mMap.addPolyline(polylineOptionsMyMoveTo);

                    TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
                    taskRequestDirections.setMarker(marker);
                    taskRequestDirections.execute(getRequesrURL(listPoint.get(0), listPoint.get(1)));
                }
                return false;
            }
        };
        return onMarkerClickListener;
    }

    private void creteLineTrack() {
        createMarkerMy();

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                try {
                    polylineOptionsMyMoveTo.getPoints().clear();
                    polylineRide.remove();
                    lineDistance.remove();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                TastyToast.makeText(getApplicationContext(), "Маршрут очищен", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show();
            }
        });

        mMap.setOnMarkerClickListener(createOnClickMarker());
    }

    private void createMarkerStation() {
        LinearLayout linearLayout = viewInfoStation.findViewById(R.id.LL);
        switch (polylines.size()) {
            case 1:
                linearLayout.removeView(viewInfoStation.findViewById(R.id.tr2));
                linearLayout.removeView(viewInfoStation.findViewById(R.id.tr3));
                break;
            case 2:
                linearLayout.removeView(viewInfoStation.findViewById(R.id.tr3));
                break;
            default:
        }

        switch (Yandex.setTypeStation(typeStation)) {
            case station:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_train_old));
                break;
            case bus_stop:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_bus));
                break;
            case platform:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_train_old));
                break;
            case stop:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_train_old));
                break;
            case crossing:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_surbun));
                break;
            case bus_station:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_bus));
                break;
            case train_station:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_train_old));
                break;
            case unknown:
            default:

        }

        iconFactory.setRotation(0);
        iconFactory.setContentView(viewInfoStation);
        iconFactory.setBackground(getResources().getDrawable(R.drawable.back_marker));
        addIcon(iconFactory, nameStation, latLngTo);
    }

    private void getTypeTransportImgV2(String typeStation) {
        switch (Yandex.setTypeStation(typeStation))
        {
            case station:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_train_old));
                break;
            case bus_stop:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_bus));
                break;
            case platform:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_train_old));
                break;
            case stop:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_train_old));
                break;
            case crossing:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_surbun));
                break;
            case bus_station:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_bus));
                break;
            case train_station:
                imgTypeStation.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.station_train_old));
                break;
            case unknown:
            default:

        }
    }

    private int getTypeTransportImg(String typeStation) {
        int reesult = R.mipmap.station_bus;
        switch (Yandex.setTypeStation(typeStation)) {
            case station:
                reesult = R.mipmap.station_train_old;
                break;
            case bus_stop:
                reesult = R.mipmap.station_bus;
                break;
            case platform:
                reesult = R.mipmap.station_train_old;
                break;
            case stop:
                reesult = R.mipmap.station_train_old;
                break;
            case crossing:
                reesult = R.mipmap.station_surbun;
                break;
            case bus_station:
                reesult = R.mipmap.station_bus;
                break;
            case train_station:
                reesult = R.mipmap.station_train_old;
                break;
            case unknown:
            default:
        }
        return reesult;
    }

    private void addIcon(IconGenerator iconFactory, String nameStation, LatLng position) {
        markerOptionsStation = new MarkerOptions().
                position(position).
                icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(""))).
                anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

        markerStation = mMap.addMarker(markerOptionsStation);
        markerStation.setTitle(nameStation);
        markerStation.setSnippet(Yandex.getTypeStation(typeStation));

    }

    private String getRequesrURL(LatLng latLngFrom, LatLng latLngTo) {
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latLngFrom.latitude + "," + latLngFrom.longitude + "&destination=" + latLngTo.latitude + "," + latLngTo.longitude +
                "&sensor=false" +
                "mode=driving" +
                "&key=AIzaSyChW3RAC9ux8sou_qdavKen4ihCPvWcEbc";
        //https://maps.googleapis.com/maps/api/directions/json?origin=54.758471,38.883707&destination=55.091838,38.758404&key=AIzaSyChW3RAC9ux8sou_qdavKen4ihCPvWcEbc
        Log.d("distanceURL", url);
        return url;
    }

    private String requestDirection(String reqUrl) throws IOException {
        String responseString = "";
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL(reqUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            //Get the response result
            inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuffer stringBuffer = new StringBuffer();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }

            responseString = stringBuffer.toString();
            bufferedReader.close();
            inputStreamReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            httpURLConnection.disconnect();
        }
        return responseString;
    }

    public class TaskRequestDirections extends AsyncTask<String, Void, String> {

        private TaskParser taskParser;
        private Marker marker;

        public void setMarker(Marker marker) {
            this.marker = marker;
        }

        private void setMarkerInToTask() {
            if (marker != null) {
                    taskParser.setMarker(marker);
            }
        }

        public TaskRequestDirections() {
            taskParser = new TaskParser();
        }

        @Override
        protected String doInBackground(String... strings) {
            String responseString = "";
            try {
                responseString = requestDirection(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Parse json here
            setMarkerInToTask();
            taskParser.clearPolyLine();
            try {
                taskParser.execute(s);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public List<LatLng> getPoints() {
            return taskParser.getPoints();
        }

        public TaskParser getTaskParser() {
            return taskParser;
        }
    }

    public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>>> {
        private List<LatLng> points = null;
        private DirectionsParser directionsParser;
        private String distance;
        private Marker marker;

        public List<LatLng> getPoints() {
            return points;
        }

        public String getDistance() {
            return distance;
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jsonObject = null;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jsonObject = new JSONObject(strings[0]);
                directionsParser = new DirectionsParser();
                routes = directionsParser.parse(jsonObject);
                distance = directionsParser.getDistance();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            for (List<HashMap<String, String>> path : lists) {
                points = new ArrayList();
                polylineOptionsMark = new PolylineOptions();

                for (HashMap<String, String> point : path) {
                    double lat = Double.parseDouble(point.get("lat"));
                    double lon = Double.parseDouble(point.get("lon"));
                    points.add(new LatLng(lat, lon));
                }

                polylineOptionsMark.addAll(points);
                polylineOptionsMark.width(7);
                polylineOptionsMark.color(Color.RED);
                polylineOptionsMark.geodesic(true);
            }

            if (polylineOptionsMark != null) {
                polylineRide = mMap.addPolyline(polylineOptionsMark);
                double d = SphericalUtil.computeLength(getPoints());

                (new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (marker != null & (marker != markerMy)) {
                                        if (marker.getSnippet() != null) {
                                            String s = "";
                                            if (marker.getSnippet().contains(getDistance())) {
                                                s = marker.getSnippet();
                                            } else {
                                                s = marker.getSnippet() + ", " + getDistance();
                                            }
                                            marker.setSnippet(s);
                                      //      Toast.makeText(getApplicationContext(), "дистанция: " + distance, Toast.LENGTH_SHORT).show();
                                            TastyToast.makeText(getApplicationContext(),"дистанция: " + distance, Toast.LENGTH_SHORT, TastyToast.DEFAULT).show();
                                        }
                                    }
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })).start();

//                Toast.makeText(getApplicationContext(), "дистанция: " + distance, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Direction not found!", Toast.LENGTH_SHORT).show();
            }
        }

        public void clearPolyLine() {
            try {
                polylineRide.remove();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void setMarker(Marker marker) {
            this.marker = marker;
        }

    }

    private void nearByPlace(String placeType) {
        Bundle extras = getIntent().getExtras();
        mMap.clear();

        if (extras != null & extras.containsKey("nearestAllStation")) {
            List<NearestStation> stations = (List<NearestStation>) getIntent().getSerializableExtra("nearestAllStation");
            for (int i = 0; i < stations.size(); i++) {
                double lat = Double.valueOf(stations.get(i).getLat());
                double lng = Double.valueOf(stations.get(i).getLng());

                MarkerOptions markerOptionsAllStation = new MarkerOptions();
                markerOptionsAllStation.position(new LatLng(lat, lng)).title(stations.get(i).getTitle())
                        .snippet(Yandex.getTypeStation(stations.get(i).getStationType()))
                        .icon(BitmapDescriptorFactory.fromResource(getTypeTransportImg(stations.get(i).getStationType())));

                mMap.addMarker(markerOptionsAllStation);

                Log.d("allstation", stations.get(i).getTransportType());
            }
            creteLineTrack();
            progressDialog.dismiss();
            Log.d("allstation", stations.size() + "");
        }
    }

    private String getUrl(double lat, double lng, String placeType) {

        StringBuilder gooleUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        gooleUrl.append("location=" + lat + "," + lng + "radius=" + 500 + "&types=" + placeType + "&sensor=true" + "&key=AIzaSyArYjiF0ZqR7T1IwcyCQlKS3yRmn1br6oI");
        Log.d("url", gooleUrl.toString());

        return gooleUrl.toString();
    }

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION))
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_CODE);
            else
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_CODE);
            return false;
        } else
            return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleClien();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleClien();
            mMap.setMyLocationEnabled(true);
        }
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//                while (true)
//                {
//                    if (location != null) {
//                        getBundle();
//                        break;
//                    }
//
//                    try {
//                        Thread.sleep(1500);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//        });


        myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    Log.d("myThread+", "Привет из потока " + Thread.currentThread().getName());

                    if (latLngMy != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    getBundle();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Log.d("myThread+", "Привет из потока runOnUiThread");
                            }
                        });
                        myThread.interrupt();

                        Log.d("myThread+", "сдохни " + Thread.currentThread().getName());
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        });

        myThread.start();
    }

    public void getBundle() {

        Bundle extras = getIntent().getExtras();
        if (extras != null && Download.isOnline2(getApplicationContext())) {
            latLngTo = (LatLng) extras.get("lanlngMap");
            nameStation = extras.getString("nameStationMap");
            typeStation = extras.getString("typeStationMap");
            distanceRadius = extras.getString("distanceRadius");
            titleLineRad.setText("дистанция: " + distanceRadius);
            Log.d("getBundle", " " + nameStation + " " + typeStation);
            route();
//            markerOptionsStation = new MarkerOptions().title("Станция №").icon(BitmapDescriptorFactory.fromResource(R.mipmap.station_bus))
//                    .snippet("автобусная остановка, 3 м")
//                    .flat(false).position(latLngTo);
//
//            markerStation = mMap.addMarker(markerOptionsStation);
//
//            listPoint.clear();
//            listPoint.add(latLngMy);
//            listPoint.add(latLngTo);
//
//            try {
//                polylineOptionsMyMoveToMarkStation.getPoints().clear();
//                lineDistanceMarkStation.remove();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            polylineOptionsMyMoveToMarkStation = new PolylineOptions()
//                    .addAll(listPoint)
//                    .width(1)
//                    .clickable(true)
//                    .color(Color.GREEN).clickable(true)
//                    .geodesic(true);
//
//            Log.d("GPS locale", polylineOptionsMyMoveToMarkStation.getPoints().size() + "");
//
//            lineDistanceMarkStation = mMap.addPolyline(polylineOptionsMyMoveToMarkStation);
//
//            TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
//            taskRequestDirections.setMarker(markerStation);
//            taskRequestDirections.execute(getRequesrURL(listPoint.get(0), listPoint.get(1)));
//
//            double z = SphericalUtil.computeDistanceBetween(listPoint.get(0), listPoint.get(1));
//            // double r = SphericalUtil.computeArea(taskRequestDirections.getTaskParser().getPoints());
//            int i = z < 0 ? (int) (z - 0.5) : (int) (z + 0.5);

            //   marker.setSnippet("автобусная станция, " + i + " м.");

        }
        else progressDialog.dismiss();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        //progressDialog.dismiss();
        if (e != null) {
            route();
            // TastyToast.makeText(this, "возникла проблема, подождите...", Toast.LENGTH_LONG, TastyToast.CONFUSING).show();
        } else {
            route();
            TastyToast.makeText(this, "что-то пошло не так, повторный запрос...", Toast.LENGTH_SHORT, TastyToast.CONFUSING).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        mMap.clear();
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return false;
            }
        });

        CameraUpdate center = CameraUpdateFactory.newLatLng(latLngMy);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

        mMap.moveCamera(center);

        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();

        Collections.sort(route, new Comparator<Route>() {
            @Override
            public int compare(Route route, Route t1) {
                if (route.getDistanceValue() > t1.getDistanceValue())
                    return 1;
                if (route.getDistanceValue() < t1.getDistanceValue())
                    return -1;
                else return 0;
            }
        });


        for (int i = 0; i < route.size(); i++) {
            Log.d("route", route.get(i).getDistanceText());

            switch (i) {
                case 0:
                    titleLine1.setText("дистанция: " + route.get(0).getDistanceText());
                    break;
                case 1:
                    titleLine2.setText("дистанция: " + route.get(1).getDistanceText());
                    break;
                case 2:
                    titleLine3.setText("дистанция: " + route.get(2).getDistanceText());
                    break;
                default:
            }


            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS_ROUTE.values()[i].getIdColor()));
            polyOptions.width(10);
            polyOptions.addAll(route.get(i).getPoints());
            polyOptions.clickable(true);
            Polyline polyline = mMap.addPolyline(polyOptions);
            polyline.setVisible(true);
            polylines.add(polyline);

            //Toast.makeText(getApplicationContext(), "Маршрут: " + COLORS_ROUTE.values()[i].getTitle() + " - растояние: " + route.get(i).getDistanceValue() + " в пути: " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
        }

        // Start marker
        createMarkerMy();
//        MarkerOptions options = new MarkerOptions();

        // End marker
        createMarkerStation();
//        options = new MarkerOptions();
//        options.position(latLngTo);
//        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.station_bus));
//        mMap.addMarker(options);

        listPoint.clear();
        listPoint.add(latLngMy);
        listPoint.add(latLngTo);

        polylineOptionsMyMoveTo = new PolylineOptions()
                .addAll(listPoint)
                .width(1)
                .clickable(true)
                .color(getResources().getColor(R.color.line_radius)).clickable(true)
                .geodesic(true);
        lineDistanceRadius = mMap.addPolyline(polylineOptionsMyMoveTo);
        progressDialog.dismiss();
        mMap.animateCamera(zoom);

    }

    @Override
    public void onRoutingCancelled() {

    }

    public void route() {
        mMap.clear();
        if (latLngMy != null & latLngTo != null) {
            progressDialog.show();
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(latLngMy, latLngTo)
                    .build();
            routing.execute();

        } else progressDialog.dismiss();

    }

    private synchronized void buildGoogleClien() {
        googleAPIClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        googleAPIClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        if (markerMy != null)
            markerMy.remove();
        lat = location.getLatitude();
        lng = location.getLongitude();

        latLngMy = new LatLng(lat, lng);
        createMarkerMy();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngMy));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));

        if (googleAPIClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleAPIClient, this);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (googleAPIClient == null)
                            buildGoogleClien();
                        mMap.setMyLocationEnabled(true);
                    }
                } else
                    Toast.makeText(getApplicationContext(), "permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleAPIClient, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleAPIClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
