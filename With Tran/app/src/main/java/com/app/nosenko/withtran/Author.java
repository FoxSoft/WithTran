package com.app.nosenko.withtran;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Author extends AppCompatActivity {

    private TextView developer;
    private ImageView copYa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author);

        developer = findViewById(R.id.developer);
        copYa = findViewById(R.id.cop_ya);
        copYa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ActivityWebView.class);
                intent.putExtra("nearestLink", "https://rasp.yandex.ru/");
                startActivity(intent);
            }
        });

    }
}
