package com.app.nosenko.withtran;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.sdsmdg.tastytoast.TastyToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapter.AdapterExpandelList;
import Model.GroupItem;

/**
 * Created by eminem on 30.12.2017.
 */

public class Tab1 extends Fragment {

    private final String TAG = "MainActivity";
    private List<GroupItem> parentItems;
    private HashMap<GroupItem, List<Yandex.SearchAPI.Segments>> childItems = new HashMap<>();
    private LayoutInflater inflater;
    private AdapterExpandelList adapter;
    private DBAdapter dbAdapter;
    private List<String> nameReyses;
    private FloatingGroupExpandableListView list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab1_fragment, container, false);

        list = view.findViewById(R.id.EXPlistview);
        inflater = getLayoutInflater();

        View header = inflater.inflate(R.layout.sample_activity_list_header, list, false);
        list.addHeaderView(header);

        dbAdapter = new DBAdapter(getContext());
        nameReyses = new ArrayList<>();
        nameReyses = dbAdapter.getBookMark();

        View footer = inflater.inflate(R.layout.sample_activity_list_footer, list, false);
        footer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/"));
                startActivity(intent);
            }
        });
        list.addFooterView(footer);
        list.setChildDivider(new ColorDrawable(Color.BLACK));

        setGroupParents();

        //test();
       // parentItems = new ArrayList<>(childItems.keySet());

        adapter = new AdapterExpandelList(getContext(), parentItems, childItems);
        WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(adapter);
        list.setAdapter(wrapperAdapter);
//        for (int i = 0; i < wrapperAdapter.getGroupCount(); i++) {
//            list.expandGroup(i);
//        }
        list.setOnScrollFloatingGroupListener(new FloatingGroupExpandableListView.OnScrollFloatingGroupListener() {

            @Override
            public void onScrollFloatingGroupListener(View floatingGroupView, int scrollY) {
                float interpolation = -scrollY / (float) floatingGroupView.getHeight();

                // Changing from RGB(162,201,85) to RGB(255,255,255)
                final int greenToWhiteRed = (int) (162 + 93 * interpolation);
                final int greenToWhiteGreen = (int) (201 + 54 * interpolation);
                final int greenToWhiteBlue = (int) (85 + 170 * interpolation);
                final int greenToWhiteColor = Color.argb(255, greenToWhiteRed, greenToWhiteGreen, greenToWhiteBlue);

                // Changing from RGB(255,255,255) to RGB(0,0,0)
                final int whiteToBlackRed = (int) (255 - 255 * interpolation);
                final int whiteToBlackGreen = (int) (255 - 255 * interpolation);
                final int whiteToBlackBlue = (int) (255 - 255 * interpolation);
                final int whiteToBlackColor = Color.argb(255, whiteToBlackRed, whiteToBlackGreen, whiteToBlackBlue);

                final ImageView image = floatingGroupView.findViewById(R.id.sample_activity_list_group_item_image);
                image.setBackgroundColor(greenToWhiteColor);

                final Drawable imageDrawable = image.getDrawable().mutate();
                imageDrawable.setColorFilter(whiteToBlackColor, PorterDuff.Mode.SRC_ATOP);

                final View background = floatingGroupView.findViewById(R.id.sample_activity_list_group_item_background);
                background.setBackgroundColor(greenToWhiteColor);

                final TextView text = floatingGroupView.findViewById(R.id.sample_activity_list_group_item_text);
                text.setTextColor(whiteToBlackColor);

            }
        });
        registerForContextMenu(list);

//        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//            @Override
//            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
//                setListViewHeight(expandableListView, i);
//                return false;
//            }
//        });
        return view;
    }

    private void setGroupParents() {
        Yandex.SearchAPI searchAPI;
        GroupItem groupItem;
        parentItems = new ArrayList<>();

        for (int i = 0; i < nameReyses.size(); i++) {
            searchAPI = new Yandex.SearchAPI();
            dbAdapter.createDataBaseInstence();
            searchAPI = dbAdapter.recordRaspTableV2(nameReyses.get(i));

            groupItem = new GroupItem(Format.getNameStatuionOfNameReys(nameReyses.get(i), Yandex.SEARCH_RASP.from_to) + " - " +
                    Format.getNameStatuionOfNameReys(nameReyses.get(i), Yandex.SEARCH_RASP.to_from), searchAPI.getPagination().getTotal(),
                    Yandex.getTransportType(searchAPI.getSegments().get(0).getThread().getTransportType()), R.drawable.reys, i);

            childItems.put(groupItem, searchAPI.getSegments());
            parentItems.add(groupItem);
        }
    }

    void test() {
        List<Yandex.SearchAPI.Segments> segmentsList = new ArrayList<>();
        Yandex.SearchAPI.Segments segments = new Yandex.SearchAPI.Segments();
        segments.setDuration("3 min");
        segments.setDeparture("20:20");
        segments.setArrival("12:30");
        segments.getThread().setNumber("31");
        segments.getThread().setTransportType(Yandex.TRANSPORT_TYPE.bus);
        segmentsList.add(segments);

        Yandex.SearchAPI.Segments segments2 = new Yandex.SearchAPI.Segments();
        segments2.setDuration("23 min");
        segments2.setDeparture("1:20");
        segments2.setArrival("6:30");
        segments2.getThread().setNumber("20");
        segments2.getThread().setTransportType(Yandex.TRANSPORT_TYPE.bus);
        segmentsList.add(segments2);

        childItems.put(new GroupItem("Z_K", "2",
                "bus", R.drawable.reys, 1), segmentsList);

        List<Yandex.SearchAPI.Segments> segmentsList1 = new ArrayList<>();
        Yandex.SearchAPI.Segments segments3 = new Yandex.SearchAPI.Segments();
        segments3.setDuration("23 min");
        segments3.setDeparture("1:20");
        segments3.setArrival("6:30");
        segments3.getThread().setNumber("20");
        segments3.getThread().setTransportType(Yandex.TRANSPORT_TYPE.bus);
        segmentsList1.add(segments3);

        childItems.put(new GroupItem("Z_K", "2",
                "bus", R.drawable.reys, 1), segmentsList1);
    }

    private void writeData() {
        final boolean b = false;
        final Yandex.SearchAPI[] searchAPI = {new Yandex.SearchAPI()};
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Log.d("thread_list", "" + 1);
                    if (!b) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                setGroupParents();
                            }
                        });
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                Log.d("thread_list", "runable");
            }
        }).start();


        Log.d("thread_list", "runable");
    }

    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        FloatingGroupExpandableListView.ExpandableListContextMenuInfo info = (FloatingGroupExpandableListView.ExpandableListContextMenuInfo) contextMenuInfo;
        int idGroup = FloatingGroupExpandableListView.getPackedPositionGroup(info.packedPosition);

        MenuInflater menuInflater = new MenuInflater(getContext());
        menuInflater.inflate(R.menu.menu_bookmarks_reys, contextMenu);

        LayoutInflater inflater =  LayoutInflater.from(getContext());
        View header = inflater.inflate(R.layout.title_menu_bookmarks, null);
        TextView titleReys = header.findViewById(R.id.nameReys);
        titleReys.setText(adapter.parentItems.get(idGroup).getNameReys());

        contextMenu.setHeaderView(header);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        FloatingGroupExpandableListView.ExpandableListContextMenuInfo info = (FloatingGroupExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();

        int idGroup = FloatingGroupExpandableListView.getPackedPositionGroup(info.packedPosition);
        Log.e("clickd", " " + idGroup);
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_delete_reys:

                String nameTable = adapter.parentItems.get(idGroup).getNameReys();
                dbAdapter.deleteTable(Format.getCompareNameReys(nameTable, " - "));

                adapter.removeGroup(idGroup);
                Log.d("delete_table", Format.getCompareNameReys(nameTable, " - "));

                TastyToast.makeText(getContext(), "рейс: " + nameTable, Toast.LENGTH_SHORT, TastyToast.SUCCESS).show();
                break;

            default:
                return super.onContextItemSelected(item);
        }

        return super.onContextItemSelected(item);
    }
}
