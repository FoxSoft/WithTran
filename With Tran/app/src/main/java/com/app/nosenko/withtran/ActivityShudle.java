package com.app.nosenko.withtran;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.sdsmdg.tastytoast.TastyToast;

import Adapter.AdapterHuntStationAutoComplete;
import Adapter.AdapterSchedule;
import Model.StationDB;

public class ActivityShudle extends AppCompatActivity {
    private Download download;
    private TextView nameStation;
    private String intentCodeStation = null;
    private Yandex.ScheduleAPI scheduleAPI;
    private ExpandableListView expandableListView;
    private AutoCompleteTextView autoComplete;
    private AdapterSchedule adapterSchedule;
    private TextView typeTransportStation;
    private TextView event;
    private TextView typeStation;
    private String tag = "api.rasp.yandex.net/v3.0/thread/?uid=";
    private AdapterHuntStationAutoComplete stationAutoComplete;
    private StationDB stationSearch;
    private ProgressDialog pr;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //      super.setTheme(R.style.BlackAndSalat);
        setContentView(R.layout.activity_shudle);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!stationSearch.getCodeStation().isEmpty() && !autoComplete.getText().toString().isEmpty()) {
                    downloadReyses();
                    setResizeList();
                }else {
                    Log.d("stationSearch", "no code");
                }
            }
        });

        final Bitmap original = BitmapFactory.decodeResource(getResources(), R.drawable.clear);
        final Bitmap b = Bitmap.createScaledBitmap(original, 40, 28, false);
        final Drawable x = new BitmapDrawable(getResources(), b);
        x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());

        nameStation = findViewById(R.id.nameStation);
        typeTransportStation = findViewById(R.id.typeTransport);
        event = findViewById(R.id.event);
        typeStation = findViewById(R.id.typeStation);
        autoComplete = findViewById(R.id.searchStation);

        autoComplete.setCompoundDrawables(null, null, autoComplete.getText().toString().equals("") ? null : x, null);
        autoComplete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (autoComplete.getCompoundDrawables()[2] == null) {
                    return false;
                }
                if (event.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }
                if (event.getX() > autoComplete.getWidth() - autoComplete.getPaddingRight() - x.getIntrinsicWidth()) {
                    autoComplete.setText("");
                    autoComplete.setCompoundDrawables(null, null, null, null);
                }
                return false;
            }
        });
        autoComplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                autoComplete.setCompoundDrawables(null, null, autoComplete.getText().toString().equals("") ? null : x, null);
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        stationAutoComplete = new AdapterHuntStationAutoComplete(this);
        autoComplete.setAdapter(stationAutoComplete);
        stationSearch = new StationDB();
        autoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                stationSearch = (StationDB) adapterView.getItemAtPosition(position);
                autoComplete.setText(stationSearch.getNameStation());
                TastyToast.makeText(getApplicationContext(), stationSearch.getCodeStation(), Toast.LENGTH_SHORT, TastyToast.SUCCESS).show();
            }
        });

        pr = new ProgressDialog(ActivityShudle.this);
        pr.setTitle("Загрузка рейсов");
        pr.setMessage("Соединение с сервером...");
        pr.setCancelable(false);
        pr.setIndeterminate(false);

        expandableListView = findViewById(R.id.expan);
        scheduleAPI = new Yandex.ScheduleAPI();
        adapterSchedule = new AdapterSchedule(getApplicationContext(), scheduleAPI, expandableListView);
        expandableListView.setAdapter(adapterSchedule);

        registerForContextMenu(expandableListView);
        setResizeList();

//        MobileAds.initialize(this, getResources().getString(R.string.YOUR_ADMOB_APP_ID));
//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("2276B596B2758694F89FA2AACDB23424").build();
//        mAdView.loadAd(adRequest);

        getBundle();
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;
        int idGroup = ExpandableListView.getPackedPositionGroup(info.packedPosition);

        MenuInflater menuInflater = new MenuInflater(getApplicationContext());
        menuInflater.inflate(R.menu.popup_menu_schedule, menu);

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View header = inflater.inflate(R.layout.title_popmenu_schedule, null);

        TextView nameStation = header.findViewById(R.id.nameStation);
        TextView number = header.findViewById(R.id.number);
        nameStation.setText(adapterSchedule.expListTitle.get(idGroup).getNameReys());
        //number.setBackground(TStatus.getBackground());
        number.setText(" №: " + adapterSchedule.expListTitle.get(idGroup).getNumber());
        menu.setHeaderView(header);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();

        int idGroup = ExpandableListView.getPackedPositionGroup(info.packedPosition);
        Log.e("clickd", " " + idGroup);
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_thread_link:
                intent = new Intent(getApplicationContext(), ActivityThread.class);
                intent.putExtra("threadLink", tag + adapterSchedule.getChild(idGroup, 0).getThread().getUid());
                startActivity(intent);
                TastyToast.makeText(getApplicationContext(), adapterSchedule.getChild(idGroup, 0).getThread().getTitle(), Toast.LENGTH_SHORT, TastyToast.INFO).show();
                break;

            default:
                return super.onContextItemSelected(item);
        }

        return super.onContextItemSelected(item);
    }

    private void getBundle() {
        intentCodeStation = getIntent().getStringExtra("scheduleCodeStation");
        if (intentCodeStation != null) {
            Log.d("schedule_list", "intent");
            final Download download = new Download(getApplicationContext(), intentCodeStation, null, null);

//            final ThreadDownload threadDownload = new ThreadDownload(download);
//            new Thread(threadDownload,"MyThread").start();
//            Handler handler = new Handler();
//            Runnable runnable = new Runnable() {
//                @Override
//                public void run() {
//                    Log.d("thread_list", "+");
//                    if (download.isSet)
//                    {
//                        Log.d("thread_list", download.threadAPI.getStops().toString());
//                    }
//
//                }
//            };
//
//
//            runnable.run();
//            handler.postDelayed(runnable, 1000);
//            //handler.removeCallbacks(runnable);
            if (isOnline3()) {
                download.execute();
                pr.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (true) {
                            Log.d("schedule_list", "" + 1);
                            if (download.isSet) {
                                scheduleAPI = download.getScheduleAPI();
                                Log.d("schedule_list", scheduleAPI.getSchedule().toString());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapterSchedule = new AdapterSchedule(getApplicationContext(), scheduleAPI, expandableListView);
                                        expandableListView.setAdapter(adapterSchedule);

                                        init();
                                        refresh();
                                        pr.dismiss();
                                    }
                                });
                                break;
                            }
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("schedule_list", "runable");
                    }
                }).start();
                Log.d("schedule_list", "runable");
            } else
                TastyToast.makeText(getApplicationContext(), "нет интернета...", Toast.LENGTH_SHORT, TastyToast.ERROR).show();
        } else Log.d("schedule_list", "no intent");
    }

    private void init() {
        nameStation.setText(scheduleAPI.getStation().getTitle());
        typeTransportStation.setText(Yandex.getTransportType(scheduleAPI.getStation().getTransportType()));
        event.setText(Yandex.getEvent(scheduleAPI.getEvent()));
        typeStation.setText(scheduleAPI.getStation().getStationTypeName());
    }

    private void expan()
    {
        WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(adapterSchedule);
        expandableListView.setAdapter(wrapperAdapter);
        if(wrapperAdapter.getGroupCount()> 0)
        expandableListView.expandGroup(1);
    }

    private void setResizeList() {
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                setListViewHeight(expandableListView, i);

                return false;
            }
        });
    }

    private void t()
    {
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                setListViewHeight(expandableListView, 0);
                return false;
            }
        });
        expandableListView.expandGroup(0);
    }

    protected boolean isOnline3() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        return cm.getActiveNetworkInfo() != null;
    }

    private void refresh() {

//        Timer t = new Timer();
//        Timer tim = new Timer();
//        final StatusStation statusStation = new StatusStation();
//        final DBThread thread = new DBThread(getApplicationContext(), tim);
//        tim.scheduleAtFixedRate(new TimerTask() {
//            public void run() {
//                runOnUiThread(new Runnable() {
//                    public void run() {
//
//                        for (int i = 0; i < adapterThread.expListTitle.size(); i++) {
//
//                            Log.d("timeThread", ((Yandex.ThreadAPI.Stops) adapterSchedule.getChild(i, 0)).getDuration());
//                            adapterThread.expListTitle.get(i).setStatus(statusStation.getStatusStation(adapterSchedule.getStartTime(),
//                                    ((Yandex.ThreadAPI.Stops) adapterSchedule.getChild(i, 0)).getDuration()));
//                            adapterThread.expListTitle.get(i).setColorBackground(statusStation.getColorBackground());
//                        }
//                        Parcelable parcelable = expandableListView.onSaveInstanceState();
//                        adapterSchedule.notifyDataSetChanged();
//                        expandableListView.setAdapter(adapterSchedule);
//                        expandableListView.onRestoreInstanceState(parcelable);
//                    }
//                });
//            }
//        }, 0, 30000);

    }

    private void downloadReyses() {

            download = new Download(getApplicationContext(), stationSearch.getCodeStation(), null, null);
            if (isOnline3()) {
                pr.show();
                download.execute();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (true) {
                            Log.d("schedule_list", "" + 1);
                            if (download.isSet & download.isDownload) {
                                scheduleAPI = download.getScheduleAPI();
                                Log.d("schedule_list", scheduleAPI.getSchedule().toString());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapterSchedule = new AdapterSchedule(getApplicationContext(), scheduleAPI, expandableListView);
                                        expandableListView.setAdapter(adapterSchedule);
                                        init();
                                        refresh();
                                        pr.dismiss();
                                    }
                                });
                                break;
                            } else pr.dismiss();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("schedule_list", "runable");
                    }
                }).start();
              //  pr.dismiss();
                Log.d("schedule_list", "runable");
            } else
                TastyToast.makeText(getApplicationContext(), "нет интернета...", Toast.LENGTH_SHORT, TastyToast.ERROR).show();

    }

    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
