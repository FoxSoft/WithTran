package com.app.nosenko.withtran;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.app.nosenko.withtran.NotificationWithTran;

public class NotificationReceiver extends BroadcastReceiver {
    private String nameReys, statusReys;

    @Override
    public void onReceive(Context context, Intent intent) {


        if (intent != null) {
            nameReys = intent.getStringExtra("AlarmClock_NameReys");
            statusReys = intent.getStringExtra("AlarmClock_StatusReys");
        }

        Log.d("AlarmClock", "test");
        NotificationWithTran.notify(context, statusReys, nameReys, 0);
    }
}
