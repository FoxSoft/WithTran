package com.app.nosenko.withtran;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.widget.RemoteViews;


/**
 * Created by eminem on 31.12.2017.
 */

public class ServiceWidget  extends Service {
    public ServiceWidget() {

    }
    @Override
    public void onCreate()
    {
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        updateInfoWidget();
        return super.onStartCommand(intent, flags, startId);
    }
    private void updateInfoWidget()
    {//Обновление виджета
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        int ids[] = appWidgetManager.getAppWidgetIds(new ComponentName(this.getApplicationContext().getPackageName(),
                MyProvider.class.getName()));

        ComponentName thisWidget = new ComponentName(this, MyProvider.class);
        RemoteViews remoteViews = new RemoteViews(this.getPackageName(), R.id.lvList);

        for (int i = 0; i < ids.length; i++) {
            MyProvider.updateAppWidget(this.getApplicationContext(), appWidgetManager, ids[i]);
            MyProvider.setList(remoteViews, this.getApplicationContext(), ids[i]);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}