package com.app.nosenko.withtran;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DialogInfoReys extends AppCompatActivity {
    private float x;
    private float y;
    private String sDown;
    private String sMove;
    private String sUp;
    private float heightFrame;
    private float heightFrame2;
    private float widthFrame;
    private float widthFrame2;
    private FrameLayout frameLayout;
    private RelativeLayout relativeLayout;
    private TextView contacts;
    private TextView threadTitle;
    private TextView searchToType;
    private TextView searchToTitle;
    private TextView searchFromTitle;
    private TextView searchFromType;
    private TextView toStationStationType;
    private TextView toStationName;
    private TextView toStationStationTransportType;
    private TextView fromStationStationType;
    private TextView fromStationName;
    private TextView fromStationStationTransportType;
    private ImageView logoCompany;
    private TextView transportTitle;
    private ImageView transportColor;
    private TextView transportType;
    private TextView transportColorTitle;
    private TextView days;
    private TextView adress, url, titleCarrier, vehicle;
    /////////////////////////
    private String linkLogo = null;
    private String contactsS = null;
    private String fromStationStationTransportTypeS = null;
    private String searchFromTitleS = null;
    private String searchFromTypeS = null;
    private String searchToTitleS = null;
    private String searchToTypeS = null;
    private String toStationStationTransportTypeS = null;
    private String toStationNameS = null;
    private String toStationStationTypeS = null;
    private String fromStationStationTypeS = null;
    private String fromStationNameS = null;
    private String transportTitleS = null;
    private String transportColorS = null;
    private String transportTypeS = null;
    private String threadTitleS;
    private String daysDeparture = null;
    private String titleCarrierS = null, urlS = null, adressS = null, vehicleS = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_info_reys);

        frameLayout = findViewById(R.id.frame);
        relativeLayout = findViewById(R.id.RL);

        contacts = findViewById(R.id.contacts);
        contacts.setLinksClickable(true);

        logoCompany = findViewById(R.id.logoCompany);
        fromStationStationTransportType = findViewById(R.id.fromStationStatigonTransportType);
        fromStationName = findViewById(R.id.fromStationTitle);
        fromStationStationType = findViewById(R.id.fromStationStationType);
        toStationStationTransportType = findViewById(R.id.toStationStationTransportType);
        toStationName = findViewById(R.id.toStationTitle);
        toStationStationType = findViewById(R.id.toStationStationType);
        searchFromTitle = findViewById(R.id.searchFromTitle);
        searchFromType = findViewById(R.id.searchFromType);
        searchToTitle = findViewById(R.id.searchToTitle);
        searchToType = findViewById(R.id.searchToType);
        transportColor = findViewById(R.id.transportColor);
        transportTitle = findViewById(R.id.transportTitle);
        transportType = findViewById(R.id.transportType);
        transportColorTitle = findViewById(R.id.transportColorTitle);
        threadTitle = findViewById(R.id.threadTitle);
        days = findViewById(R.id.days);
        adress = findViewById(R.id.adress);
        url = findViewById(R.id.url);
        titleCarrier = findViewById(R.id.titleCarrier);
        vehicle = findViewById(R.id.vehicle);

        frameLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                x = event.getX();
                y = event.getY();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: // нажатие
                        sDown = "Down: x=" + x + "; y=" + y;
                        break;
                }
                testCoordinate();
                Log.d("coordinate", sDown);
                return true;
            }
        });

        getBundle();
    }

    private void getBundle() {

        contactsS = (String) getIntent().getSerializableExtra("contact");
        linkLogo = (String) getIntent().getSerializableExtra("logo");
        searchToTypeS = Yandex.getType((Yandex.TYPE) getIntent().getSerializableExtra("searchToType"));
        searchToTitleS = (String) getIntent().getSerializableExtra("searchToTitle");
        searchFromTitleS = (String) getIntent().getSerializableExtra("searchFromTitle");
        searchFromTypeS = Yandex.getType((Yandex.TYPE) getIntent().getSerializableExtra("searchFromType"));
        try {
            toStationStationTypeS = Yandex.getTypeStation((Yandex.STATION_TYPE) getIntent().getSerializableExtra("toStationStationType"));
        } finally {
            toStationStationTypeS = (String) getIntent().getSerializableExtra("toStationStationTypeName");
            Log.d("typeStationTo", toStationStationTypeS);
        }

        toStationNameS = (String) getIntent().getSerializableExtra("toStationTitle");
        toStationStationTransportTypeS = Yandex.getTransportType((Yandex.TRANSPORT_TYPE) getIntent().getSerializableExtra("toStationStationTransportType"));
        try {
            fromStationStationTypeS = Yandex.getTypeStation((Yandex.STATION_TYPE) getIntent().getSerializableExtra("fromStationStationType"));
        } finally {
            fromStationStationTypeS = (String) getIntent().getSerializableExtra("fromStationStationTypeName");
            Log.d("typeStationFrom", fromStationStationTypeS);
        }

        fromStationNameS = (String) getIntent().getSerializableExtra("fromStationTitle");
        fromStationStationTransportTypeS = Yandex.getTransportType((Yandex.TRANSPORT_TYPE) getIntent().getSerializableExtra("fromStationStationTransportType"));
        transportColorS = (String) getIntent().getSerializableExtra("transportColor");
        transportTitleS = (String) getIntent().getSerializableExtra("transportTitle");
        transportTypeS = Yandex.getTransportType((Yandex.TRANSPORT_TYPE) getIntent().getSerializableExtra("transportType"));
        threadTitleS = (String) getIntent().getSerializableExtra("threadTitle");
        daysDeparture = (String) getIntent().getSerializableExtra("days");
        urlS = (String) getIntent().getSerializableExtra("url");
        adressS = (String) getIntent().getSerializableExtra("adress");
        titleCarrierS = (String) getIntent().getSerializableExtra("titleCarrier");
        vehicleS = (String) getIntent().getSerializableExtra("vehicle");
        threadTitle.setText(threadTitleS);

        if (linkLogo != null | linkLogo != "" | linkLogo != "xsi:nil=\"true\"") {
            Glide.with(getApplicationContext()).load(linkLogo).into(logoCompany)
                    .onLoadFailed(getApplicationContext().getResources().getDrawable(R.drawable.preview_logo));
        } else {
            logoCompany.setImageResource(R.drawable.preview_logo);
        }

        if (contactsS == null) {
            contactsS = "<br/>нет данных</i><br/><br/>";
        } else contacts.setText(Html.fromHtml(contactsS));

        if (searchToTypeS != null){
            searchToType.setText(searchToTypeS);
        } else searchToType.setText("");

        if (searchToTitleS != null){
            searchToTitle.setText(searchToTitleS);
        } else searchToTitle.setText("");

        if (searchFromTitleS != null){
            searchFromTitle.setText(searchFromTitleS);
        } else searchFromTitle.setText("");

        if (searchFromTypeS != null){
            searchFromType.setText(searchFromTypeS);
        } else searchFromType.setText("");

        if (toStationStationTransportTypeS != null){
            toStationStationTransportType.setText(toStationStationTransportTypeS);
        } else toStationStationTransportType.setText("");

        if (toStationNameS != null){
            toStationName.setText(toStationNameS);
        } else toStationName.setText("");

        if (toStationStationTypeS != null){
            toStationStationType.setText(toStationStationTypeS);
        } else toStationStationType.setText("");

        if (fromStationStationTransportTypeS != null){
            fromStationStationTransportType.setText(fromStationStationTransportTypeS);
        } else fromStationStationTransportType.setText("");

        if (fromStationNameS != null){
            fromStationName.setText(fromStationNameS);
        } else fromStationName.setText("");

        if (fromStationStationTypeS != null){
            fromStationStationType.setText(fromStationStationTypeS);
        } else fromStationStationType.setText("");

        if (transportTypeS != null){
            transportType.setText(transportTypeS);
        } else transportType.setText("");

        if (transportTitleS != null){
            transportTitle.setText(transportTitleS);
        } else transportTitle.setText("");

        if (transportColorS != null){
            try {
                transportColor.setBackgroundColor(Color.parseColor(transportColorS));
            } catch (Exception e){
                e.printStackTrace();
                transportColorTitle.setText("");
            }
        }

        if (daysDeparture != null){
            days.setText(daysDeparture);
        } else days.setText("---");
        if (titleCarrierS != null){
            titleCarrier.setText(titleCarrierS);
        } else {
            titleCarrier.setText("---");
        }
        if (urlS != null){
            url.setText(urlS);
        } else {
            titleCarrier.setText("---");
        }
        if (adressS != null){
            adress.setText(adressS);
        } else {
            adress.setText("---");
        }
        if (vehicleS != null){
            vehicle.setText("Модель: " + vehicleS);
        } else {
            vehicle.setText("Модель: ---");
        }
    }

    private void testCoordinate() {
        float resultY;
        //float resultX;

        //resultX = widthFrame/2 - widthFrame2/2;
        resultY = heightFrame / 2 - heightFrame2 / 2;

        if (y <= resultY | y >= resultY + heightFrame2) {
            finish();
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

//        heightFrame = frameLayout.getMeasuredHeightAndState();
//        heightFrame2 = frameLayout2.getMeasuredHeightAndState();
//
//        widthFrame = frameLayout.getMeasuredWidthAndState();
//        widthFrame2 = frameLayout2.getMeasuredWidthAndState();
//
//        Log.d("coordinate", "1: " + heightFrame + " " + widthFrame);
//
//        Log.d("coordinate", "2: " + heightFrame2 + " " + widthFrame2);
        heightFrame = frameLayout.getMeasuredHeightAndState();
        heightFrame2 = relativeLayout.getMeasuredHeightAndState();

        widthFrame = frameLayout.getMeasuredWidthAndState();
        widthFrame2 = relativeLayout.getMeasuredWidthAndState();

        Log.d("coordinate", "1: " + heightFrame + " " + widthFrame);

        Log.d("coordinate", "2: " + heightFrame2 + " " + widthFrame2);

        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }
}
