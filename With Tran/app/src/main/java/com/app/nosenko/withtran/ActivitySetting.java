package com.app.nosenko.withtran;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;

import com.sdsmdg.tastytoast.TastyToast;

public class ActivitySetting extends AppCompatActivity {

    private RadioButton rbBlack;
    private RadioButton rbLight;
    private int theme;
    private DBAdapter dbAdapter;
    private int colorBackLiner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        rbLight = findViewById(R.id.rbLight);
        rbBlack = findViewById(R.id.rbBlack);

        dbAdapter = new DBAdapter(getApplicationContext());

        rbBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rbBlack.setChecked(true);
                rbLight.setChecked(false);
                theme = R.style.Black;
                colorBackLiner = R.color.black;
                dbAdapter.setIdTheme(theme, colorBackLiner);
                TastyToast.makeText(getApplicationContext(), "Выбранная тема будет прменена после перезапуска приложения", TastyToast.LENGTH_SHORT,
                        TastyToast.INFO).show();
                Log.d("theme_id", String.valueOf(theme));
            }
        });

        rbLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rbLight.setChecked(true);
                rbBlack.setChecked(false);
                theme = R.style.Light;
                colorBackLiner = R.color.white;
                dbAdapter.setIdTheme(theme, colorBackLiner);
                toast();
                Log.d("theme_id", String.valueOf(theme));
            }
        });
    }

private void toast()
{
    TastyToast.makeText(getApplicationContext(), "Выбранная тема будет прменена после перезапуска приложения", TastyToast.LENGTH_SHORT,
            TastyToast.INFO).show();
}


   private void clickCheked() {
        if (rbBlack.isChecked()) {
            rbLight.setChecked(false);
        }

        if (rbLight.isChecked()) {
            rbBlack.setChecked(false);
        }
    }
}
