package com.app.nosenko.withtran;

import android.content.Context;
import android.view.View;

/**
 * Created by eminem on 18.03.2018.
 */

public class HelperBackground {
    private View view;
    private Integer color;
    private Context context;
    private static DBAdapter dbAdapter;
    private static Integer theme = null;
    private static Integer colorBLL = null;

    public static void setBackgroudColor(Context context, View view)
    {
        getTheme(context);
        if (colorBLL != null)
        view.setBackgroundColor(context.getResources().getColor(colorBLL));
    }

    public static void setTheme(Context context)
    {
        getTheme(context);
        if (theme != null)
            context.setTheme(theme);
    }

    private static  void getTheme(Context context)
    {
        dbAdapter = new DBAdapter(context);
        if (dbAdapter.getIdTheme() != null) {
            theme = dbAdapter.getIdTheme().get(0);
            colorBLL = dbAdapter.getIdTheme().get(1);
        }
    }

    public static void setStyle(Context context, View view)
    {
        getTheme(context);
        if (theme != null)
            context.getApplicationContext().setTheme(theme);
        if (colorBLL != null)
            view.setBackgroundColor(context.getResources().getColor(colorBLL));

    }
}
