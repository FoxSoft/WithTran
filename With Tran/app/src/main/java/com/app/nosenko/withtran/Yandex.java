package com.app.nosenko.withtran;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eminem on 24.12.2017.
 */

public class Yandex {
    public enum API {nearest_settlement, nearest_stations, search, schedule, thread}

    public enum STATION_TYPE {
        station, platform, stop, checkpoint, post,
        crossing, overtaking_point, train_station, airport, bus_station, bus_stop,
        unknown, port, wharf, river_port, marine_station, port_point
    }

    //    station — станция;
//    platform — платформа;
//    stop — остановочный пункт;
//    checkpoint — блок-пост;
//    post — пост;
//    crossing — разъезд;
//    overtaking_point — обгонный пункт;
//    train_station — вокзал;
//    airport — аэропорт;
//    bus_station — автовокзал;
//    bus_stop — автобусная остановка;
//    unknown — станция без типа;
//    port — порт;
//    port_point — портпункт;
//    wharf — пристань;
//    river_port — речной вокзал;
//    marine_station — морской вокзал.
    public enum TYPE_CHOICES {
        schedule, tablo, train, suburban, aeroex
    }

    //    schedule — вид расписания по умолчанию;
//    tablo — табло аэропорта;
//    train — расписание железнодорожного вокзала;
//    suburban — расписание электричек;
//    aeroex — расписание аэроэкспрессов.
    public enum TRANSPORT_TYPE {
        bus("автобус", R.drawable.buses_red), suburban("электричка", R.drawable.surban_red), train("поезд", R.drawable.train_red),
        water("водный транспорт", R.drawable.water_red), helicopter("вертолёт", R.drawable.helicopter_red), plane("самолёт", R.drawable.fly_red);
        private String title;
        private int idImg;

        TRANSPORT_TYPE(String title, int idImg) {
            this.title = title;
            this.idImg = idImg;
        }

        public int getIdImg() {
            return idImg;
        }

        public String getTitle() {
            return title;
        }

    }

//    plane — самолет;
//    train — поезд;
//    suburban — электричка;
//    bus — автобус;
//    water — водный транспорт;
//    helicopter — вертолет.

    public enum EVENT {
        departure, arrival
    }
    // departure — включить в ответ только отправляющиеся со станции нитки (по умолчанию);
    // arrival — включить в ответ только прибывающие на станцию нитки.

    //признак запроса маршрутов с пересадками
    public static boolean TRANSFERS;

    //    Признак экспресса или аэроэкспресса. Значение по умолчанию — xsi:nil=“true“.
//    Если тип транспорта — электричка (элемент transport_type возвращен со значением suburban), принимает одно из значений:
//    express — экспресс-рейс;
//    aeroexpress — рейс, курсирующий между городом и аэропортом.
    public enum EXPRESS_TYPE {
        express, aeroexpress
    }

    //    Вид пункта отправления.
//    Возможные значения:
//    station — станция;
//    settlement — поселение.
    public enum TYPE {
        station, settlement
    }

    public enum SEARCH_RASP {
        from_to, to_from
    }

    public enum StatusTransportView {
        driveImg,
        driveOffImg,
        driveWillImg
    }

    //--------------------------Запросы к API--------------------------------//
    //Расписание рейсов между станциями
    public static class SearchAPI {

        /**
         * interval_segments : []
         * pagination : {"total":160,"limit":100,"offset":0}
         * segments : [{"except_days":"","arrival":"02:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6154_7_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6154","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-24&uid=SU-6154_7_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"00:01:00","stops":"","days":"чт с 24.05 по 27.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9240,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-24","arrival_platform":""},{"except_days":"","arrival":"04:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6152_6_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6152","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-21&uid=SU-6152_6_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"01:40:00","stops":"","days":"пн с 21.05 по 01.10","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-21","arrival_platform":""},{"except_days":"","arrival":"05:00:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1627_1_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1627","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-01&uid=SU-1627_1_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"02:25:00","stops":"","days":"ежедневно с 01.05 по 01.06","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-01","arrival_platform":""},{"except_days":"","arrival":"05:00:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1627_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1627","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-02&uid=SU-1627_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"02:25:00","stops":"","days":"ежедневно с 02.06 по 19.10","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-02","arrival_platform":""},{"except_days":"","arrival":"05:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-268_0_c260_547","title":"Симферополь \u2014 Москва","number":"S7 268","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-02&uid=S7-268_0_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"03:05:00","stops":"","days":"ежедневно с 02.06 по 24.09","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-02","arrival_platform":""},{"except_days":"","arrival":"05:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-268_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 268","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-02&uid=GH-268_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"03:05:00","stops":"","days":"ежедневно с 02.06 по 24.09","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-02","arrival_platform":""},{"except_days":"","arrival":"06:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6158_0_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6158","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-21&uid=SU-6158_0_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Airbus A319","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"03:35:00","stops":"","days":"пн с 21.05 по 24.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-21","arrival_platform":""},{"except_days":"","arrival":"07:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1633_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1633","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-02&uid=SU-1633_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"05:05:00","stops":"","days":"ежедневно с 02.06 по 19.10","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-02","arrival_platform":""},{"except_days":"","arrival":"11:15:00","from":{"code":"s9654608","title":"Симферополь, автостанция-1 «Центральный»","station_type":"bus_station","popular_title":"Автовокзал Центральный","short_title":"","transport_type":"bus","station_type_name":"автовокзал","type":"station"},"thread":{"uid":"empty_1_f9692613t9744862_413","title":"Ялта \u2014 Москва, м. Щёлковская","number":"","short_title":"Ялта \u2014 Москва, м. Щёлковская","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=empty_1_f9692613t9744862_413","carrier":null,"transport_type":"bus","vehicle":null,"transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"06:10:00","stops":"","days":"ежедневно","to":{"code":"s9744862","title":"Москва, м. Щёлковская","station_type":"","popular_title":"м. Щёлковская","short_title":"","transport_type":"bus","station_type_name":"автостанция","type":"station"},"duration":104700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"09:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6152_8_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6152","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-26&uid=SU-6152_8_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"06:30:00","stops":"","days":"сб с 26.05 по 29.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-26","arrival_platform":""},{"except_days":"","arrival":"11:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-264_0_c260_547","title":"Симферополь \u2014 Москва","number":"S7 264","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-26&uid=S7-264_0_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"08:40:00","stops":"","days":"ежедневно с 26.05 по 24.09","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-26","arrival_platform":""},{"except_days":"","arrival":"11:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-264_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 264","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-26&uid=GH-264_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"08:40:00","stops":"","days":"ежедневно с 26.05 по 24.09","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-26","arrival_platform":""},{"except_days":"","arrival":"12:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6154_0_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6154","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-09-11&uid=SU-6154_0_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"09:35:00","stops":"","days":"только 11, 18, 25 сентября","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-09-11","arrival_platform":""},{"except_days":"","arrival":"12:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1637_1_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1637","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-04-30&uid=SU-1637_1_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"09:50:00","stops":"","days":"ежедневно с 30.04 по 27.09","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-04-30","arrival_platform":""},{"except_days":"","arrival":"12:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_14_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=WZ-308_14_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"12:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_5_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=5N-6308_5_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"12:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-270_1_c260_547","title":"Симферополь \u2014 Москва","number":"S7 270","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=S7-270_1_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:05:00","stops":"","days":"ежедневно, кроме ср с 25.03 по 21.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"12:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-270_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 270","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=GH-270_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:05:00","stops":"","days":"ежедневно, кроме ср с 25.03 по 21.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"12:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-526_6_c30_547","title":"Симферополь \u2014 Москва","number":"U6 526","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-04&uid=U6-526_6_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus A319","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:10:00","stops":"","days":"только 4 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-04","arrival_platform":""},{"except_days":"","arrival":"12:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-526_2_c30_547","title":"Симферополь \u2014 Москва","number":"U6 526","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=U6-526_2_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:10:00","stops":"","days":"только 26, 29, 30 декабря, 5, 8 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"12:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-526_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 526","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=U6-526_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:10:00","stops":"","days":"6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"12:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6150_3_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6150","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-21&uid=SU-6150_3_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 777-300","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:15:00","stops":"","days":"пн, вт, чт, сб с 21.05 по 29.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-21","arrival_platform":""},{"except_days":"","arrival":"13:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-36_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 36","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-29&uid=U6-36_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:40:00","stops":"","days":"только 29 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-29","arrival_platform":""},{"except_days":"","arrival":"13:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-534_1_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 534","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=N4-534_1_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"только 26, 29 декабря, 5 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9900,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"13:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-534_0_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 534","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-09&uid=N4-534_0_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"вт, пт с 02.01 по 23.03, кроме 05.01","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9900,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-09","arrival_platform":""},{"except_days":"","arrival":"13:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1645_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1645","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-1645_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"13:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_2_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-07&uid=WZ-306_2_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"только 7 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-07","arrival_platform":""},{"except_days":"","arrival":"13:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_8_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-07&uid=5N-6306_8_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"только 7 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-07","arrival_platform":""},{"except_days":"","arrival":"13:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-262_0_c260_547","title":"Симферополь \u2014 Москва","number":"S7 262","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=S7-262_0_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:15:00","stops":"","days":"5, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26, 27 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"13:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-262_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 262","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=GH-262_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:15:00","stops":"","days":"5, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26, 27 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"14:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1637_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1637","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=SU-1637_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:25:00","stops":"","days":"пн, ср, пт, вс по 23.03","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"14:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1637_2_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1637","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=SU-1637_2_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:25:00","stops":"","days":"вт, чт, сб по 24.03","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"14:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-34_3_c30_547","title":"Симферополь \u2014 Москва","number":"U6 34","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=U6-34_3_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:40:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"14:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-124_1_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 124","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=N4-124_1_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:55:00","stops":"","days":"ежедневно по 24.03, кроме 01.02","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10200,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"14:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-124_0_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 124","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-02-01&uid=N4-124_0_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:55:00","stops":"","days":"только 1 февраля","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10200,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-02-01","arrival_platform":""},{"except_days":"","arrival":"14:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-924_1_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 924","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=N4-924_1_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10500,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-2951_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 2951","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=SU-2951_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:00:00","stops":"","days":"только 1 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"14:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_7_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-27&uid=WZ-306_7_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:00:00","stops":"","days":"только 27 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-27","arrival_platform":""},{"except_days":"","arrival":"14:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_4_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-27&uid=5N-6306_4_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:00:00","stops":"","days":"только 27 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-27","arrival_platform":""},{"except_days":"","arrival":"14:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-440_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 440","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=U6-440_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:05:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_13_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-28&uid=5N-6308_13_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"ТУ-204","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 28 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-28","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_4_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-28&uid=WZ-308_4_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"ТУ-204","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 28 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-28","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_2_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-29&uid=5N-6308_2_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 29 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-29","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_1_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-29&uid=WZ-308_1_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 29 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-29","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_5_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=WZ-306_5_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 2, 5, 8 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_7_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=5N-6306_7_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 2, 5, 8 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-934_0_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 934","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=N4-934_0_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 1 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10500,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"14:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_15_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-13&uid=WZ-308_15_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 13 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-13","arrival_platform":""},{"except_days":"","arrival":"14:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_12_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-13&uid=5N-6308_12_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 13 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-13","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-36_3_c30_547","title":"Симферополь \u2014 Москва","number":"U6 36","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=U6-36_3_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 6, 7 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-36_4_c30_547","title":"Симферополь \u2014 Москва","number":"U6 36","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-08&uid=U6-36_4_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus A319","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 8 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-08","arrival_platform":""},{"except_days":"","arrival":"14:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-164_1_c260_547","title":"Симферополь \u2014 Москва","number":"S7 164","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=S7-164_1_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:20:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"14:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-164_1_c260_12","title":"Симферополь \u2014 Москва","number":"GH 164","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=GH-164_1_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:20:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"15:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_14_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-04&uid=5N-6308_14_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:35:00","stops":"","days":"только 4 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-04","arrival_platform":""},{"except_days":"","arrival":"15:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_11_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-04&uid=WZ-308_11_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:35:00","stops":"","days":"только 4 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-04","arrival_platform":""},{"except_days":"","arrival":"15:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-524_2_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 524","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=N4-524_2_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:40:00","stops":"","days":"только 29 декабря, 5 января, 8, 9 февраля","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10200,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-524_0_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 524","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-11&uid=N4-524_0_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:40:00","stops":"","days":"только 11, 12, 18, 19, 25, 26 января, 1, 2, 15, 16, 22, 23 февраля, 1, 2, 8, 9, 15, 16, 22, 23 марта","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10200,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-11","arrival_platform":""},{"except_days":"","arrival":"15:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-164_0_c260_547","title":"Симферополь \u2014 Москва","number":"S7 164","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=S7-164_0_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:45:00","stops":"","days":"ежедневно по 24.03","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-164_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 164","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=GH-164_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:45:00","stops":"","days":"ежедневно по 24.03","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1621_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1621","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-1621_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:55:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"15:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-34_0_c30_547","title":"Симферополь \u2014 Москва","number":"U6 34","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-28&uid=U6-34_0_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:55:00","stops":"","days":"только 28 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-28","arrival_platform":""},{"except_days":"","arrival":"15:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_1_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-6144_1_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:05:00","stops":"","days":"ежедневно с 25.03 по 19.05","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"15:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-2832_3_c30_547","title":"Симферополь \u2014 Москва","number":"U6 2832","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=U6-2832_3_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:20:00","stops":"","days":"5, 6, 7, 9, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 24, 26, 27, 28, 29, 30 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-2832_0_c30_547","title":"Симферополь \u2014 Москва","number":"U6 2832","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-08&uid=U6-2832_0_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:20:00","stops":"","days":"только 1, 8, 12, 15, 18, 21, 25 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-08","arrival_platform":""},{"except_days":"","arrival":"16:00:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1621_1_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1621","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=SU-1621_1_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:25:00","stops":"","days":"ежедневно по 24.03","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-2832_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 2832","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=U6-2832_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:25:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"16:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_6_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=SU-6144_6_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:40:00","stops":"","days":"только 31 декабря","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"20:30:00","from":{"code":"s9654608","title":"Симферополь, автостанция-1 «Центральный»","station_type":"bus_station","popular_title":"Автовокзал Центральный","short_title":"","transport_type":"bus","station_type_name":"автовокзал","type":"station"},"thread":{"uid":"empty_1_f9654609t9744862_413","title":"Севастополь \u2014 Москва, м. Щёлковская","number":"","short_title":"Севастополь \u2014 Москва, м. Щёлковская","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=empty_1_f9654609t9744862_413","carrier":null,"transport_type":"bus","vehicle":null,"transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:05:00","stops":"","days":"ежедневно","to":{"code":"s9744862","title":"Москва, м. Щёлковская","station_type":"","popular_title":"м. Щёлковская","short_title":"","transport_type":"bus","station_type_name":"автостанция","type":"station"},"duration":109500,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"16:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_0_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=SU-6144_0_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:05:00","stops":"","days":"только 1 января","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"16:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_3_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=SU-6144_3_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:05:00","stops":"","days":"ежедневно по 24.03, кроме 31.12, 01.01","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"16:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_2_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-21&uid=SU-6144_2_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 777-300","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:10:00","stops":"","days":"пн, вт, чт, сб с 21.05 по 29.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-21","arrival_platform":""},{"except_days":"","arrival":"16:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_4_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-23&uid=SU-6144_4_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 747-400","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:10:00","stops":"","days":"ср, пт, вс с 23.05 по 30.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-23","arrival_platform":""},{"except_days":"","arrival":"17:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-558_0_c80_547","title":"Симферополь \u2014 Москва","number":"5N 558","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-29&uid=5N-558_0_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-500","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:25:00","stops":"","days":"только 29 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-29","arrival_platform":""},{"except_days":"","arrival":"17:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_3_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-09&uid=WZ-306_3_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:40:00","stops":"","days":"только 9 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-09","arrival_platform":""},{"except_days":"","arrival":"17:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_9_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-09&uid=5N-6306_9_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:40:00","stops":"","days":"только 9 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-09","arrival_platform":""},{"except_days":"","arrival":"17:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1787_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1787","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=SU-1787_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:00:00","stops":"","days":"только 1 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"17:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_3_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=U6-522_3_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:05:00","stops":"","days":"5, 9, 10, 11, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 27, 30, 31 января, 1, 3 февраля, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"17:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_2_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=U6-522_2_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus A319","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:05:00","stops":"","days":"только 31 декабря, 6 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"17:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_5_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-08&uid=U6-522_5_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:05:00","stops":"","days":"только 8, 12, 13, 19, 26, 28, 29 января, 2, 9, 16, 23 февраля, 2, 9, 16, 23 марта","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-08","arrival_platform":""},{"except_days":"","arrival":"17:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"6R-540_6_c156_547","title":"Симферополь \u2014 Москва","number":"6R 540","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-02&uid=6R-540_6_c156_547","carrier":{"code":156,"contacts":"678170, Саха (Якутия), г. Мирный, Аэропорт","url":"http://www.alrosa.aero/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/alrosa_avia.svg","title":"Алроса","phone":"+7 (411) 364-71-70","codes":{"icao":"DRU","sirena":"ЯМ","iata":"6R"},"address":"респ. Саха, г. Мирный, Аэропорт","logo":null,"email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:15:00","stops":"","days":"только 2 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-02","arrival_platform":""},{"except_days":"","arrival":"17:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_5_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-27&uid=WZ-308_5_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:20:00","stops":"","days":"только 24, 27 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-27","arrival_platform":""},{"except_days":"","arrival":"17:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_0_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-27&uid=5N-6308_0_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:20:00","stops":"","days":"только 24, 27 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-27","arrival_platform":""},{"except_days":"","arrival":"17:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_3_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-10&uid=WZ-308_3_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:20:00","stops":"","days":"10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-10","arrival_platform":""},{"except_days":"","arrival":"17:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_1_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-10&uid=5N-6308_1_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:20:00","stops":"","days":"10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-10","arrival_platform":""},{"except_days":"","arrival":"18:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1623_2_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1623","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-1623_2_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:35:00","stops":"","days":"ежедневно с 25.03 по 31.05","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"18:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1623_3_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1623","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-01&uid=SU-1623_3_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:35:00","stops":"","days":"ежедневно с 01.06 по 19.10","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-01","arrival_platform":""},{"except_days":"","arrival":"19:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6152_7_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6152","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-24&uid=SU-6152_7_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:35:00","stops":"","days":"чт с 24.05 по 27.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-24","arrival_platform":""},{"except_days":"","arrival":"19:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1649_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1649","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-01&uid=SU-1649_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:35:00","stops":"","days":"ежедневно с 01.06 по 19.10","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-01","arrival_platform":""},{"except_days":"","arrival":"19:00:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_9_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-07&uid=U6-522_9_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:35:00","stops":"","days":"только 7 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-07","arrival_platform":""},{"except_days":"","arrival":"19:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_0_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=U6-522_0_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:40:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"19:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-166_1_c23_547","title":"Симферополь \u2014 Москва","number":"S7 166","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=S7-166_1_c23_547","carrier":{"code":23,"contacts":"Телефон: 8-800-200-000-7 (звонок по России бесплатный)","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/S7.svg","title":"S7 Airlines","phone":"","codes":{"icao":null,"sirena":"С7","iata":"S7"},"address":"г. Москва, Новая пл., 3/4, Политехнический музей, подъезд 4, м. Китай-город","logo":"//yastatic.net/rasp/media/data/company/logo/logo_2_Yandex.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:45:00","stops":"","days":"ежедневно по 24.03","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"19:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-34_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 34","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=U6-34_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:50:00","stops":"","days":"только 5 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"19:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-34_4_c30_547","title":"Симферополь \u2014 Москва","number":"U6 34","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=U6-34_4_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:50:00","stops":"","days":"только 6 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"19:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1789_1_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1789","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=SU-1789_1_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Сухой Суперджет 100","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"только 1 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9900,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"19:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_4_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=WZ-306_4_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"19:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_6_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=5N-6306_6_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"19:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-166_0_c23_547","title":"Симферополь \u2014 Москва","number":"S7 166","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=S7-166_0_c23_547","carrier":{"code":23,"contacts":"Телефон: 8-800-200-000-7 (звонок по России бесплатный)","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/S7.svg","title":"S7 Airlines","phone":"","codes":{"icao":null,"sirena":"С7","iata":"S7"},"address":"г. Москва, Новая пл., 3/4, Политехнический музей, подъезд 4, м. Китай-город","logo":"//yastatic.net/rasp/media/data/company/logo/logo_2_Yandex.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"19:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_8_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-30&uid=U6-522_8_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"только 30 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-30","arrival_platform":""},{"except_days":"","arrival":"19:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1629_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1629","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-1629_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:10:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"20:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_9_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-26&uid=WZ-306_9_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:30:00","stops":"","days":"только 23, 24, 25, 26 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-26","arrival_platform":""}]
         * search : {"date":null,"to":{"code":"c213","type":"settlement","popular_title":"Москва","short_title":"Москва","title":"Москва"},"from":{"code":"c146","type":"settlement","popular_title":"Симферополь","short_title":"Симферополь","title":"Симферополь"}}
         */

        private Pagination pagination = new Pagination();
        private Search search = new Search();
        private List<Segments> intervalSegments = new ArrayList<>();
        private List<Segments> segments = new ArrayList<>();

        public SearchAPI(Pagination pagination, Search search, List<Segments> intervalSegments, List<Segments> segments) {
            this.pagination = pagination;
            this.search = search;
            this.intervalSegments = intervalSegments;
            this.segments = segments;
        }

        public SearchAPI() {
            this.pagination = new Pagination();
            this.search = new Search();
            this.intervalSegments = new ArrayList<>();
            this.segments = new ArrayList<>();
        }

        public Pagination getPagination() {
            return pagination;
        }

        public void setPagination(Pagination pagination) {
            this.pagination = pagination;
        }

        public Search getSearch() {
            return search;
        }

        public void setSearch(Search search) {
            this.search = search;
        }

        public List<Segments> getIntervalSegments() {
            return intervalSegments;
        }

        public void setIntervalSegments(List<Segments> intervalSegments) {
            this.intervalSegments = intervalSegments;
        }

        public List<Segments> getSegments() {
            return segments;
        }

        public void setSegments(List<Segments> segments) {
            this.segments = segments;
        }

        public static class Search {

            /**
             * date : null
             * to : {"code":"c213","type":"settlement","popular_title":"Москва","short_title":"Москва","title":"Москва"}
             * from : {"code":"c146","type":"settlement","popular_title":"Симферополь","short_title":"Симферополь","title":"Симферополь"}
             */

            private String date = null;
            private ToSearch toSearch = new ToSearch();
            private FromSearch fromSearch = new FromSearch();

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public ToSearch getToSearch() {
                return toSearch;
            }

            public void setToSearch(ToSearch toSearch) {
                this.toSearch = toSearch;
            }

            public FromSearch getFromSearch() {
                return fromSearch;
            }

            public void setFromSearch(FromSearch fromSearch) {
                this.fromSearch = fromSearch;
            }

            @Override
            public String toString() {
                return "Search{" +
                        "date='" + date + '\'' +
                        ", toSearch=" + toSearch +
                        ", fromSearch=" + fromSearch +
                        '}';
            }

            public void clear() {
                this.setDate(null);
                this.getToSearch().clear();
                this.getFromSearch().clear();
            }

            public static class ToSearch {
                /**
                 * code : c213
                 * type : settlement
                 * popular_title : Москва
                 * short_title : Москва
                 * title : Москва
                 */

                private String code = null;
                private TYPE type = null;
                private String popularTitle = null;
                private String shortTitle = null;
                private String title = null;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public TYPE getType() {
                    return type;
                }

                public void setType(TYPE type) {
                    this.type = type;
                }

                public String getPopularTitle() {
                    return popularTitle;
                }

                public void setPopularTitle(String popularTitle) {
                    this.popularTitle = popularTitle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                @Override
                public String toString() {
                    return "ToSearch{" +
                            "code='" + code + '\'' +
                            ", type=" + type +
                            ", popularTitle='" + popularTitle + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", title='" + title + '\'' +
                            '}';
                }

                public void clear() {
                    this.setTitle(null);
                    this.setShortTitle(null);
                    this.setPopularTitle(null);
                    this.setCode(null);
                    this.setType(null);
                }
            }

            public static class FromSearch {
                /**
                 * code : c146
                 * type : settlement
                 * popular_title : Симферополь
                 * short_title : Симферополь
                 * title : Симферополь
                 */

                private String code = null;
                private TYPE type = null;
                private String popularTitle = null;
                private String shortTitle = null;
                private String title = null;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public TYPE getType() {
                    return type;
                }

                public void setType(TYPE type) {
                    this.type = type;
                }

                public String getPopularTitle() {
                    return popularTitle;
                }

                public void setPopularTitle(String popularTitle) {
                    this.popularTitle = popularTitle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                @Override
                public String toString() {
                    return "FromSearch{" +
                            "code='" + code + '\'' +
                            ", type=" + type +
                            ", popularTitle='" + popularTitle + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", title='" + title + '\'' +
                            '}';
                }

                public void clear() {
                    this.setTitle(null);
                    this.setShortTitle(null);
                    this.setPopularTitle(null);
                    this.setCode(null);
                    this.setType(null);
                }
            }
        }

        @Override
        public String toString() {
            return "SearchAPI{" +
                    "pagination=" + pagination +
                    ", search=" + search +
                    ", intervalSegments=" + intervalSegments +
                    ", segments=" + segments +
                    '}';
        }

        public static class Segments {

            private String exceptDays = null;
            private String arrival = null;
            private From from = new From();
            private Thread thread = new Thread();
            private String departurePlatform = null;
            private String departure = null;
            private String stops = null;
            private String days = null;
            private To to = new To();
            private String duration = null;
            private String departureTerminal = null;
            private String arrivalTerminal = null;
            private String startDate = null;
            private String arrivalPlatform = null;

            //------------------------
            private Integer id;
            private Integer track_max = 0;
            private boolean track_visible;
            private Integer position = 1;
            private String status;
            private Integer idImg;
            private int thumb;
            private TransportView transportView = new TransportView();

            public TransportView getTransportView() {
                return transportView;
            }

            public void setTransportView(TransportView transportView) {
                this.transportView = transportView;
            }

            public int getThumb() {
                return thumb;
            }

            public void setThumb(int thumb) {
                this.thumb = thumb;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getTrack_max() {
                return track_max;
            }

            public void setTrack_max(Integer track_max) {
                this.track_max = track_max;
            }

            public boolean isTrack_visible() {
                return track_visible;
            }

            public void setTrack_visible(boolean track_visible) {
                this.track_visible = track_visible;
            }

            public Integer getPosition() {
                return position;
            }

            public void setPosition(Integer position) {
                this.position = position;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Integer getIdImg() {
                return idImg;
            }

            public void setIdImg(Integer idImg) {
                this.idImg = idImg;
            }

            @Override
            public String toString() {
                return "Segments{" +
                        "exceptDays='" + exceptDays + '\'' +
                        ", arrival='" + arrival + '\'' +
                        ", from=" + from +
                        ", thread=" + thread +
                        ", departurePlatform='" + departurePlatform + '\'' +
                        ", departure='" + departure + '\'' +
                        ", stops='" + stops + '\'' +
                        ", days='" + days + '\'' +
                        ", to=" + to +
                        ", duration='" + duration + '\'' +
                        ", departureTerminal='" + departureTerminal + '\'' +
                        ", arrivalTerminal='" + arrivalTerminal + '\'' +
                        ", startDate='" + startDate + '\'' +
                        ", arrivalPlatform='" + arrivalPlatform + '\'' +
                        '}';
            }

            public String getExceptDays() {
                return exceptDays;
            }

            public void setExceptDays(String exceptDays) {
                this.exceptDays = exceptDays;
            }

            public String getArrival() {
                return arrival;
            }

            public void setArrival(String arrival) {
                this.arrival = arrival;
            }

            public From getFrom() {
                return from;
            }

            public void setFrom(From from) {
                this.from = from;
            }

            public Thread getThread() {
                return thread;
            }

            public void setThread(Thread thread) {
                this.thread = thread;
            }

            public String getDeparturePlatform() {
                return departurePlatform;
            }

            public void setDeparturePlatform(String departurePlatform) {
                this.departurePlatform = departurePlatform;
            }

            public String getDeparture() {
                return departure;
            }

            public void setDeparture(String departure) {
                this.departure = departure;
            }

            public String getStops() {
                return stops;
            }

            public void setStops(String stops) {
                this.stops = stops;
            }

            public String getDays() {
                return days;
            }

            public void setDays(String days) {
                this.days = days;
            }

            public To getTo() {
                return to;
            }

            public void setTo(To to) {
                this.to = to;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }

            public String getDepartureTerminal() {
                return departureTerminal;
            }

            public void setDepartureTerminal(String departureTerminal) {
                this.departureTerminal = departureTerminal;
            }

            public String getArrivalTerminal() {
                return arrivalTerminal;
            }

            public void setArrivalTerminal(String arrivalTerminal) {
                this.arrivalTerminal = arrivalTerminal;
            }

            public String getStartDate() {
                return startDate;
            }

            public void setStartDate(String startDate) {
                this.startDate = startDate;
            }

            public String getArrivalPlatform() {
                return arrivalPlatform;
            }

            public void setArrivalPlatform(String arrivalPlatform) {
                this.arrivalPlatform = arrivalPlatform;
            }

            public static class From {
                /**
                 * code : s9600396
                 * title : Симферополь
                 * station_type : airport
                 * popular_title :
                 * short_title :
                 * transport_type : plane
                 * station_type_name : аэропорт
                 * type : station
                 */

                private String code = null;
                private String title = null;
                private STATION_TYPE stationType = null;
                private String popularTitle = null;
                private String shortTitle = null;
                private TRANSPORT_TYPE transportType = null;
                private String stationTypeName = null;
                private TYPE type = null;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public STATION_TYPE getStationType() {
                    return stationType;
                }

                public void setStationType(STATION_TYPE stationType) {
                    this.stationType = stationType;
                }

                public String getPopularTitle() {
                    return popularTitle;
                }

                public void setPopularTitle(String popularTitle) {
                    this.popularTitle = popularTitle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public TRANSPORT_TYPE getTransportType() {
                    return transportType;
                }

                public void setTransportType(TRANSPORT_TYPE transportType) {
                    this.transportType = transportType;
                }

                public String getStationTypeName() {
                    return stationTypeName;
                }

                public void setStationTypeName(String stationTypeName) {
                    this.stationTypeName = stationTypeName;
                }

                public TYPE getType() {
                    return type;
                }

                public void setType(TYPE type) {
                    this.type = type;
                }

                @Override
                public String toString() {
                    return "From{" +
                            "code='" + code + '\'' +
                            ", title='" + title + '\'' +
                            ", stationType=" + stationType +
                            ", popularTitle='" + popularTitle + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", transportType=" + transportType +
                            ", stationTypeName='" + stationTypeName + '\'' +
                            ", type=" + type +
                            '}';
                }
            }

            public static class Thread {
                /**
                 * uid : SU-6154_7_c8565_547
                 * title : Симферополь — Москва
                 * number : SU 6154
                 * short_title : Симферополь — Москва
                 * thread_method_link : api.rasp.yandex.net/v3/thread/?date=2018-05-24&uid=SU-6154_7_c8565_547
                 * carrier : {"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""}
                 * transport_type : plane
                 * vehicle : Boeing 737-800
                 * transport_subtype : {"color":null,"code":null,"title":null}
                 * express_type : null
                 */

                private String uid = null;
                private String title = null;
                private String number = null;
                private String shortTitle = null;
                private String threadMethodLink = null;
                private Carrier carrier = new Carrier();//Информация о перевозчике.
                private TRANSPORT_TYPE transportType = null;
                private String vehicle;//Название транспортного средства.
                private TransportSubtype transportSubtype = new TransportSubtype();
                private EXPRESS_TYPE expressType = null;

                public String getUid() {
                    return uid;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getNumber() {
                    return number;
                }

                public void setNumber(String number) {
                    this.number = number;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public String getThreadMethodLink() {
                    return threadMethodLink;
                }

                public void setThreadMethodLink(String threadMethodLink) {
                    this.threadMethodLink = threadMethodLink;
                }

                public Carrier getCarrier() {
                    return carrier;
                }

                public void setCarrier(Carrier carrier) {
                    this.carrier = carrier;
                }

                public TRANSPORT_TYPE getTransportType() {
                    return transportType;
                }

                public void setTransportType(TRANSPORT_TYPE transportType) {
                    this.transportType = transportType;
                }

                @Override
                public String toString() {
                    return "Thread{" +
                            "uid='" + uid + '\'' +
                            ", title='" + title + '\'' +
                            ", number='" + number + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", threadMethodLink='" + threadMethodLink + '\'' +
                            ", carrier=" + carrier +
                            ", transportType=" + transportType +
                            ", vehicle='" + vehicle + '\'' +
                            ", transportSubtype=" + transportSubtype +
                            ", expressType=" + expressType +
                            '}';
                }

                public String getVehicle() {
                    return vehicle;
                }

                public void setVehicle(String vehicle) {
                    this.vehicle = vehicle;
                }

                public TransportSubtype getTransportSubtype() {
                    return transportSubtype;
                }

                public void setTransportSubtype(TransportSubtype transportSubtype) {
                    this.transportSubtype = transportSubtype;
                }

                public EXPRESS_TYPE getExpressType() {
                    return expressType;
                }

                public void setExpressType(EXPRESS_TYPE expressType) {
                    this.expressType = expressType;
                }

                public static class Carrier {
                    /**
                     * code : 8565
                     * contacts : Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>
                     * Представительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>
                     * <p>
                     * Телефон call-центра: 647-0-647.
                     * url : http://www.rossiya-airlines.com/
                     * logo_svg : //yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg
                     * title : Россия
                     * phone :
                     * codes : {"icao":null,"sirena":"ФВ","iata":"SU"}
                     * address : Санкт-Петербург, ул. Пилотов, д. 18
                     * logo : //yastatic.net/rasp/media/data/company/logo/logorus_1.jpg
                     * email :
                     */

                    private String code = null;
                    private String contacts = null;
                    private String url = null;
                    private String logo_svg = null;
                    private String title = null;
                    private String phone = null;
                    private Codes codes = new Codes();
                    private String address = null;
                    private String logo = null;
                    private String email = null;

                    public String getCode() {
                        return code;
                    }

                    public void setCode(String code) {
                        this.code = code;
                    }

                    public String getContacts() {
                        return contacts;
                    }

                    public void setContacts(String contacts) {
                        this.contacts = contacts;
                    }

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    public String getLogo_svg() {
                        return logo_svg;
                    }

                    public void setLogo_svg(String logo_svg) {
                        this.logo_svg = logo_svg;
                    }

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public Codes getCodes() {
                        return codes;
                    }

                    public void setCodes(Codes codes) {
                        this.codes = codes;
                    }

                    public String getAddress() {
                        return address;
                    }

                    public void setAddress(String address) {
                        this.address = address;
                    }

                    public String getLogo() {
                        return logo;
                    }

                    public void setLogo(String logo) {
                        this.logo = logo;
                    }

                    public String getEmail() {
                        return email;
                    }

                    public void setEmail(String email) {
                        this.email = email;
                    }

                    @Override
                    public String toString() {
                        return "Carrier{" +
                                "code='" + code + '\'' +
                                ", contacts='" + contacts + '\'' +
                                ", url='" + url + '\'' +
                                ", logo_svg='" + logo_svg + '\'' +
                                ", title='" + title + '\'' +
                                ", phone='" + phone + '\'' +
                                ", codes=" + codes +
                                ", address='" + address + '\'' +
                                ", logo='" + logo + '\'' +
                                ", email='" + email + '\'' +
                                '}';
                    }
                }

                public static class TransportSubtype {
                    /**
                     * color : null
                     * code : null
                     * title : null
                     */

                    private String color = null;
                    private String code = null;
                    private String title = null;

                    public String getColor() {
                        return color;
                    }

                    public void setColor(String color) {
                        this.color = color;
                    }

                    public String getCode() {
                        return code;
                    }

                    public void setCode(String code) {
                        this.code = code;
                    }

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }

                    @Override
                    public String toString() {
                        return "TransportSubtype{" +
                                "color='" + color + '\'' +
                                ", code='" + code + '\'' +
                                ", title='" + title + '\'' +
                                '}';
                    }
                }


            }

            public static class To {
                /**
                 * code : s9600215
                 * title : Внуково
                 * station_type : airport
                 * popular_title :
                 * short_title :
                 * transport_type : plane
                 * station_type_name : аэропорт
                 * type : station
                 */

                private String code = null;
                private String title = null;
                private STATION_TYPE stationType = null;
                private String popularTitle = null;
                private String shortTitle = null;
                private TRANSPORT_TYPE transportType = null;
                private String stationTypeName = null;
                private TYPE type = null;

                public String getCode() {
                    return code;
                }

                public TRANSPORT_TYPE getTransportType() {
                    return transportType;
                }

                public TYPE getType() {
                    return type;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public STATION_TYPE getStationType() {
                    return stationType;
                }

                public void setStationType(STATION_TYPE stationType) {
                    this.stationType = stationType;
                }

                public void setTransportType(TRANSPORT_TYPE transportType) {
                    this.transportType = transportType;
                }

                public void setType(TYPE type) {
                    this.type = type;
                }

                public String getPopularTitle() {
                    return popularTitle;
                }

                public void setPopularTitle(String popularTitle) {
                    this.popularTitle = popularTitle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public String getStationTypeName() {
                    return stationTypeName;
                }

                public void setStationTypeName(String stationTypeName) {
                    this.stationTypeName = stationTypeName;
                }

                @Override
                public String toString() {
                    return "To{" +
                            "code='" + code + '\'' +
                            ", title='" + title + '\'' +
                            ", stationType=" + stationType +
                            ", popularTitle='" + popularTitle + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", transportType=" + transportType +
                            ", stationTypeName='" + stationTypeName + '\'' +
                            ", type=" + type +
                            '}';
                }
            }

            public static class TransportView {

                private Integer driveImg = R.color.proz;//активный тран
                private Integer driveOffImg = null;//уехал тран
                private Integer driveWillImg = null;// поедет тран
                //------------------------
                private Integer id = null;
                private Integer track_max = 0;
                private boolean track_visible;
                private Integer position = 1;
                private String status = null;
                private Integer idImg = driveWillImg;
                private Integer thumb = null;
                private Integer colorStatusTitle = R.color.proz;


                public void setThumb(Integer thumb) {
                    this.thumb = thumb;
                }

                public int getDriveImg() {
                    return driveImg;
                }

                public void setDriveImg(Integer driveImg) {
                    this.driveImg = driveImg;
                }

                public int getDriveOffImg() {
                    return driveOffImg;
                }

                public void setDriveOffImg(Integer driveOffImg) {
                    this.driveOffImg = driveOffImg;
                }

                public int getDriveWillImg() {
                    return driveWillImg;
                }

                public void setDriveWillImg(Integer driveWillImg) {
                    this.driveWillImg = driveWillImg;
                }

                public TransportView() {
                }

                private void setIdImg(Integer idImg) {
                    this.idImg = idImg;
                }

                public Integer getColorStatusTitle() {
                    return colorStatusTitle;
                }

                public void setColorStatusTitle(Integer colorStatusTitle) {
                    this.colorStatusTitle = colorStatusTitle;
                }

                public void setStatusDrive(TRANSPORT_TYPE transportType) {
                    switch (transportType) {
                        case helicopter:
                            driveImg = R.drawable.helicopter_yellow;
                            driveOffImg = R.drawable.helicopter_red;
                            driveWillImg = R.drawable.helicopter_green;
                            setIdImg(driveWillImg);
                            break;
                        case suburban:
                            driveImg = R.drawable.surban_yellow;
                            driveOffImg = R.drawable.surban_red;
                            driveWillImg = R.drawable.surban_green;
                            setIdImg(driveWillImg);
                            break;
                        case water:
                            driveImg = R.drawable.water_yellow;
                            driveOffImg = R.drawable.water_red;
                            driveWillImg = R.drawable.water_green;
                            setIdImg(driveWillImg);
                            break;
                        case train:
                            driveImg = R.drawable.train_yellow;
                            driveOffImg = R.drawable.train_red;
                            driveWillImg = R.drawable.train_green;
                            setIdImg(driveWillImg);
                            break;
                        case plane:
                            driveImg = R.drawable.fly_yellow;
                            driveOffImg = R.drawable.fly_red;
                            driveWillImg = R.drawable.fly_green;
                            setIdImg(driveWillImg);
                            break;
                        case bus:
                            driveImg = R.drawable.buses_yellow;
                            driveOffImg = R.drawable.buses_red;
                            driveWillImg = R.drawable.buses_green;
                            setIdImg(driveWillImg);
                            break;
                        default:
                            driveImg = R.drawable.preview_logo;
                            driveOffImg = R.drawable.preview_logo;
                            driveWillImg = R.drawable.preview_logo;
                            setIdImg(driveWillImg);
                            break;
                    }
                }

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public Integer getTrack_max() {
                    return track_max;
                }

                public void setTrack_max(Integer track_max) {
                    this.track_max = track_max;
                }

                public boolean isTrack_visible() {
                    return track_visible;
                }

                public void setTrack_visible(boolean track_visible) {
                    this.track_visible = track_visible;
                }

                public Integer getPosition() {
                    return position;
                }

                public void setPosition(Integer position) {
                    this.position = position;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public Integer getIdImg() {
                    return idImg;
                }

                public void setIdImg(StatusTransportView statusTransportView) {
                    switch (statusTransportView) {
                        case driveImg:
                            this.idImg = getDriveImg();
                            break;
                        case driveOffImg:
                            this.idImg = getDriveOffImg();
                            break;
                        case driveWillImg:
                            this.idImg = getDriveWillImg();
                            break;
                        default:
                            this.idImg = getDriveWillImg();
                            break;
                    }
                }

                public int getThumb() {
                    return thumb;
                }

                public void setThumb(int thumb) {
                    this.thumb = thumb;
                }


            }
        }

        public void clearAll() {
            this.getSegments().clear();
            this.getIntervalSegments().clear();
            this.getPagination().clear();
            this.getSearch().clear();
        }
    }

    //Список станций следования
    public static class ThreadAPI {
        private String exceptDays = null;
        private String arrivalDate = null;
        private String from = null;
        private String uid = null;
        private String title = null;
        private String departureDate = null;
        private String startTime = null;
        private String number = null;
        private String shortTitle = null;
        private String days = null;
        private String to = null;
        private Carrier carrier = new Carrier();
        private TRANSPORT_TYPE transportType;
        private List<Stops> stops = new ArrayList<>();
        private String vehicle = null;
        private String startDate = null;
        private TransportSubtype transportSubtype = new TransportSubtype();
        private EXPRESS_TYPE expressType = null;

        @Override
        public String toString() {
            return "ThreadAPI{" +
                    "exceptDays='" + exceptDays + '\'' +
                    ", arrivalDate='" + arrivalDate + '\'' +
                    ", from='" + from + '\'' +
                    ", uid='" + uid + '\'' +
                    ", title='" + title + '\'' +
                    ", departureDate='" + departureDate + '\'' +
                    ", startTime='" + startTime + '\'' +
                    ", number='" + number + '\'' +
                    ", shortTitle='" + shortTitle + '\'' +
                    ", days='" + days + '\'' +
                    ", to='" + to + '\'' +
                    ", carrier=" + carrier +
                    ", transportType=" + transportType +
                    ", stops=" + stops +
                    ", vehicle='" + vehicle + '\'' +
                    ", startDate='" + startDate + '\'' +
                    ", transportSubtype=" + transportSubtype +
                    ", expressType=" + expressType +
                    '}';
        }

        public String getExceptDays() {
            return exceptDays;
        }

        public void setExceptDays(String exceptDays) {
            this.exceptDays = exceptDays;
        }

        public String getArrivalDate() {
            return arrivalDate;
        }

        public void setArrivalDate(String arrivalDate) {
            this.arrivalDate = arrivalDate;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDepartureDate() {
            return departureDate;
        }

        public void setDepartureDate(String departureDate) {
            this.departureDate = departureDate;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getShortTitle() {
            return shortTitle;
        }

        public void setShortTitle(String shortTitle) {
            this.shortTitle = shortTitle;
        }

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public Carrier getCarrier() {
            return carrier;
        }

        public void setCarrier(Carrier carrier) {
            this.carrier = carrier;
        }

        public TRANSPORT_TYPE getTransportType() {
            return transportType;
        }

        public void setTransportType(TRANSPORT_TYPE transportType) {
            this.transportType = transportType;
        }

        public List<Stops> getStops() {
            return stops;
        }

        public void setStops(List<Stops> stops) {
            this.stops = stops;
        }

        public String getVehicle() {
            return vehicle;
        }

        public void setVehicle(String vehicle) {
            this.vehicle = vehicle;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public TransportSubtype getTransportSubtype() {
            return transportSubtype;
        }

        public void setTransportSubtype(TransportSubtype transportSubtype) {
            this.transportSubtype = transportSubtype;
        }

        public EXPRESS_TYPE getExpressType() {
            return expressType;
        }

        public void setExpressType(EXPRESS_TYPE expressType) {
            this.expressType = expressType;
        }

        public static class Carrier {
            /**
             * code : 153
             * offices : []
             * codes : {"icao":null,"sirena":null,"iata":null}
             * title : Центральная пригородная пассажирская компания
             */

            private String code = null;
            private Codes codes = new Codes();
            private String title = null;
            private List<?> offices = new ArrayList<>();

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public Codes getCodes() {
                return codes;
            }

            public void setCodes(Codes codes) {
                this.codes = codes;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public List<?> getOffices() {
                return offices;
            }

            public void setOffices(List<?> offices) {
                this.offices = offices;
            }

            public void clear()
            {
                this.code = null;
                this.title = null;
                this.offices.clear();
                this.codes.clear();
            }

            @Override
            public String toString() {
                return "Carrier{" +
                        "code='" + code + '\'' +
                        ", codes=" + codes +
                        ", title='" + title + '\'' +
                        ", offices=" + offices +
                        '}';
            }
        }

        public static class Stops {
            /**
             * arrival : null
             * departure : 2018-01-06 03:38:00
             * terminal : null
             * platform :
             * station : {"code":"s9600786","title":"Рязань-1","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"}
             * stop_time : null
             * duration : 0
             */

            private String arrival = null;
            private String departure = null;
            private String terminal = null;
            private String platform = null;
            private Station station = new Station();
            private String stopTime = null;
            private String duration = null;

            @Override
            public String toString() {
                return "Stops{" +
                        "arrival='" + arrival + '\'' +
                        ", departure='" + departure + '\'' +
                        ", terminal='" + terminal + '\'' +
                        ", platform='" + platform + '\'' +
                        ", station=" + station +
                        ", stopTime='" + stopTime + '\'' +
                        ", duration='" + duration + '\'' +
                        '}';
            }

            public String getArrival() {
                return arrival;
            }

            public void setArrival(String arrival) {
                this.arrival = arrival;
            }

            public String getDeparture() {
                return departure;
            }

            public void setDeparture(String departure) {
                this.departure = departure;
            }

            public String getTerminal() {
                return terminal;
            }

            public void setTerminal(String terminal) {
                this.terminal = terminal;
            }

            public String getPlatform() {
                return platform;
            }

            public void setPlatform(String platform) {
                this.platform = platform;
            }

            public Station getStation() {
                return station;
            }

            public void setStation(Station station) {
                this.station = station;
            }

            public String getStopTime() {
                return stopTime;
            }

            public void setStopTime(String stopTime) {
                this.stopTime = stopTime;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }
        }

        public void clearAll() {
            this.stops.clear();
        }
    }

    //Расписание рейсов по станции
    public static class ScheduleAPI {
        private Pagination pagination;
        private List<Schedule> schedule = new ArrayList<>();
        private Station station = new Station();
        private String date;
        private List<Object> intervalSchedule = new ArrayList<>();
        private EVENT event;

        public Pagination getPagination() {
            return pagination;
        }

        public void setPagination(Pagination pagination) {
            this.pagination = pagination;
        }

        public List<Schedule> getSchedule() {
            return schedule;
        }

        public void setSchedule(List<Schedule> schedule) {
            this.schedule = schedule;
        }

        public Station getStation() {
            return station;
        }

        public void setStation(Station station) {
            this.station = station;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public List<Object> getIntervalSchedule() {
            return intervalSchedule;
        }

        public void setIntervalSchedule(List<Object> intervalSchedule) {
            this.intervalSchedule = intervalSchedule;
        }

        public EVENT getEvent() {
            return event;
        }

        public void setEvent(EVENT event) {
            this.event = event;
        }

        public static class Schedule {

            private String exceptDays;
            private String arrival;
            private Thread thread = new Thread();
            private String isFuzzy;
            private String days;
            private String stops;
            private String departure;
            private String terminal;
            private String platform;


            public String getArrival() {
                return arrival;
            }

            public void setArrival(String arrival) {
                this.arrival = arrival;
            }

            public Thread getThread() {
                return thread;
            }

            public void setThread(Thread thread) {
                this.thread = thread;
            }

            public String getExceptDays() {
                return exceptDays;
            }

            public void setExceptDays(String exceptDays) {
                this.exceptDays = exceptDays;
            }

            public String getFuzzy() {
                return isFuzzy;
            }

            public void setFuzzy(String fuzzy) {
                isFuzzy = fuzzy;
            }

            public String getDays() {
                return days;
            }

            public void setDays(String days) {
                this.days = days;
            }

            public String getStops() {
                return stops;
            }

            public void setStops(String stops) {
                this.stops = stops;
            }

            public String getDeparture() {
                return departure;
            }

            public void setDeparture(String departure) {
                this.departure = departure;
            }

            public String getTerminal() {
                return terminal;
            }

            public void setTerminal(String terminal) {
                this.terminal = terminal;
            }

            public String getPlatform() {
                return platform;
            }

            public void setPlatform(String platform) {
                this.platform = platform;
            }

            @Override
            public String toString() {
                return "Schedule{" +
                        "exceptDays=" + exceptDays +
                        ", arrival='" + arrival + '\'' +
                        ", thread=" + thread +
                        ", isFuzzy=" + isFuzzy +
                        ", days='" + days + '\'' +
                        ", stops='" + stops + '\'' +
                        ", departure='" + departure + '\'' +
                        ", terminal=" + terminal +
                        ", platform='" + platform + '\'' +
                        '}';
            }

            public static class Thread {
                /**
                 * uid : 6991_0_9600786_g18_4
                 * title : Рязань-1 — Москва (Казанский вокзал)
                 * number : 6991
                 * short_title : Рязань-1 — М-Казанская
                 * carrier : {"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"}
                 * transport_type : suburban
                 * vehicle : null
                 * transport_subtype : {"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"}
                 * express_type : null
                 */

                private String uid;
                private String title;
                private String number;
                private String shortTitle;
                private Carrier carrier = new Carrier();
                private TRANSPORT_TYPE transportType = null;
                private String vehicle;
                private TransportSubtype transportSubtype = new TransportSubtype();
                private EXPRESS_TYPE expressType;

                public String getUid() {
                    return uid;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getNumber() {
                    return number;
                }

                public void setNumber(String number) {
                    this.number = number;
                }


                public Carrier getCarrier() {
                    return carrier;
                }

                public void setCarrier(Carrier carrier) {
                    this.carrier = carrier;
                }

                public String getVehicle() {
                    return vehicle;
                }

                public void setVehicle(String vehicle) {
                    this.vehicle = vehicle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public TRANSPORT_TYPE getTransportType() {
                    return transportType;
                }

                public void setTransportType(TRANSPORT_TYPE transportType) {
                    this.transportType = transportType;
                }

                public TransportSubtype getTransportSubtype() {
                    return transportSubtype;
                }

                public void setTransportSubtype(TransportSubtype transportSubtype) {
                    this.transportSubtype = transportSubtype;
                }

                public EXPRESS_TYPE getExpressType() {
                    return expressType;
                }

                @Override
                public String toString() {
                    return "Thread{" +
                            "uid='" + uid + '\'' +
                            ", title='" + title + '\'' +
                            ", number='" + number + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", carrier=" + carrier +
                            ", transportType='" + transportType + '\'' +
                            ", vehicle=" + vehicle +
                            ", transportSubtype=" + transportSubtype +
                            ", expressType=" + expressType +
                            '}';
                }

                public void setExpressType(EXPRESS_TYPE expressType) {
                    this.expressType = expressType;
                }

                public static class Carrier {
                    @Override
                    public String toString() {
                        return "Carrier{" +
                                "code=" + code +
                                ", codes=" + codes +
                                ", title='" + title + '\'' +
                                '}';
                    }

                    /**
                     * code : 153
                     * codes : {"icao":null,"sirena":null,"iata":null}
                     * title : Центральная пригородная пассажирская компания
                     */

                    private String code;
                    private Codes codes;
                    private String title;

                    public String getCode() {
                        return code;
                    }

                    public void setCode(String code) {
                        this.code = code;
                    }

                    public Codes getCodes() {
                        return codes;
                    }

                    public void setCodes(Codes codes) {
                        this.codes = codes;
                    }

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }
                }
            }
        }
    }

    //-------------------------------вспомогательные классы-------------------------------------------//
    public static class TransportSubtype {
        /**
         * color : #FF7F44
         * code : suburban
         * title : Пригородный поезд
         */

        private String color = null;
        private String code;
        private String title = "";

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void clear()
        {
            this.code = null;
            this.color = null;
            this.title = null;
        }

        @Override
        public String toString() {
            return "TransportSubtype{" +
                    "color='" + color + '\'' +
                    ", code='" + code + '\'' +
                    ", title='" + title + '\'' +
                    '}';
        }
    }

    public static class Codes {
        /**
         * icao : null
         * sirena : ФВ
         * iata : SU
         */

        private String icao;
        private String sirena;
        private String iata;

        public String getIcao() {
            return icao;
        }

        public void setIcao(String icao) {
            this.icao = icao;
        }

        public String getSirena() {
            return sirena;
        }

        public void setSirena(String sirena) {
            this.sirena = sirena;
        }

        public String getIata() {
            return iata;
        }

        public void setIata(String iata) {
            this.iata = iata;
        }

        public void clear() {
            this.iata = null;
            this.icao = null;
            this.sirena = null;
        }
    }

    public static class Pagination {
        @Override
        public String toString() {
            return "Pagination{" +
                    "total=" + total +
                    ", limit=" + limit +
                    ", offset=" + offset +
                    '}';
        }

        /**
         * total : 26
         * limit : 100
         * offset : 0
         */

        private String total;
        private String limit;
        private String offset;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public String getOffset() {
            return offset;
        }

        public void setOffset(String offset) {
            this.offset = offset;
        }

        public void clear() {
            this.setTotal("");
            this.setLimit("");
            this.setOffset("");

        }
    }

    public static class Station {
        /**
         * code : s9600726
         * title : Луховицы
         * station_type : train_station
         * popular_title :
         * short_title :
         * transport_type : train
         * station_type_name : вокзал
         * type : station
         */

        private String code = null;
        private String title = null;
        private STATION_TYPE stationType = null;
        private String popularTitle = null;
        private String shortTitle = null;
        private TRANSPORT_TYPE transportType = null;
        private String stationTypeName = null;
        private TYPE type = null;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        @Override
        public String toString() {
            return "Station{" +
                    "code='" + code + '\'' +
                    ", title='" + title + '\'' +
                    ", stationType='" + stationType + '\'' +
                    ", popularTitle='" + popularTitle + '\'' +
                    ", shortTitle='" + shortTitle + '\'' +
                    ", transportType='" + transportType + '\'' +
                    ", stationTypeName='" + stationTypeName + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public STATION_TYPE getStationType() {
            return stationType;
        }

        public void setStationType(STATION_TYPE stationType) {
            this.stationType = stationType;
        }

        public String getPopularTitle() {
            return popularTitle;
        }

        public void setPopularTitle(String popularTitle) {
            this.popularTitle = popularTitle;
        }

        public String getShortTitle() {
            return shortTitle;
        }

        public void setShortTitle(String shortTitle) {
            this.shortTitle = shortTitle;
        }

        public TRANSPORT_TYPE getTransportType() {
            return transportType;
        }

        public void setTransportType(TRANSPORT_TYPE transportType) {
            this.transportType = transportType;
        }

        public String getStationTypeName() {
            return stationTypeName;
        }

        public void setStationTypeName(String stationTypeName) {
            this.stationTypeName = stationTypeName;
        }

        public TYPE getType() {
            return type;
        }

        public void setType(TYPE type) {
            this.type = type;
        }
    }

    public static String isFuzzy(String fuzzy) {
        String b;
        switch (fuzzy) {
            case "true":
                b = "точное";
                break;
            case "false":
                b = "неточное";
                break;
            default:
                b = "неточное";
        }
        return b;
    }

    public static int getImgTypeTransport(Yandex.TRANSPORT_TYPE transportType) {
        int idImg = R.drawable.buses_green;
        switch (transportType) {
            case bus:
                idImg = R.drawable.buses_green;
                break;
            case plane:
                idImg = R.drawable.fly_green;
                break;
            case train:
                idImg = R.drawable.train_green;
                break;
            case water:
                idImg = R.drawable.water_green;
                break;
            case suburban:
                idImg = R.drawable.surban_green;
                break;
            case helicopter:
                idImg = R.drawable.helicopter_green;
                break;
            default:
                idImg = R.drawable.buses_green;
        }
        return idImg;
    }


    public static int getImgTypeTransport(String typeTransport) {
        int idImg;
        switch (typeTransport) {
            case "plane":
                idImg = R.drawable.fly_green;
                break;
            case "train":
                idImg = R.drawable.train_green;
                break;
            case "suburban":
                idImg = R.drawable.surban_green;
                break;
            case "bus":
                idImg = R.drawable.buses_green;
                break;
            case "water":
                idImg = R.drawable.water_green;
                break;
            case "helicopter":
                idImg = R.drawable.helicopter_green;
                break;
            default:
                idImg = R.drawable.bus_logo;
                break;
        }
        return idImg;
    }

    public static int getThumb(TRANSPORT_TYPE transportType) {
        int thumb;
        switch (transportType) {
            case helicopter:
                thumb = R.drawable.seek_thumb_heicopter;
                break;
            case suburban:
                thumb = R.drawable.seek_thumb_surban;
                break;
            case water:
                thumb = R.drawable.seek_thumb_water;
                break;
            case train:
                thumb = R.drawable.seek_thumb_train;
                break;
            case plane:
                thumb = R.drawable.seek_thumb_fly;
                break;
            case bus:
                thumb = R.drawable.seek_thumb_bus;
                break;
            default:
                thumb = R.drawable.seek_thumb_bus;
                break;
        }
        return thumb;
    }

    public static int getImgTypeTransportLogo(TRANSPORT_TYPE transportType) {
        int idImg;
        switch (transportType) {
            case helicopter:
                idImg = R.drawable.helicopter_logo;
                break;
            case suburban:
                idImg = R.drawable.surban_logo;
                break;
            case water:
                idImg = R.drawable.water_logo;
                break;
            case train:
                idImg = R.drawable.train_logo;
                break;
            case plane:
                idImg = R.drawable.fly_logo;
                break;
            case bus:
                idImg = R.drawable.bus_logo;
                break;
            default:
                idImg = R.drawable.preview_logo;
                break;
        }
        return idImg;
    }

    public static int getImgTypeTransportLogo(String transportType) {
        int idImg;
        switch (transportType) {
            case "helicopter":
                idImg = R.drawable.helicopter_logo;
                break;
            case "suburban":
                idImg = R.drawable.surban_logo;
                break;
            case "water":
                idImg = R.drawable.water_logo;
                break;
            case "train":
                idImg = R.drawable.train_logo;
                break;
            case "plane":
                idImg = R.drawable.fly_logo;
                break;
            case "bus":
                idImg = R.drawable.bus_logo;
                break;
            default:
                idImg = R.drawable.preview_logo;
                break;
        }
        return idImg;
    }

    //----------------------------------------------------------------------------------------------//
    public static String getTypeStation(String typeStation) {
        switch (typeStation) {
            case "bus_stop":
                typeStation = "автобусная остановка";
                break;
            case "bus_station":
                typeStation = "автовокзал";
                break;
            case "train_station":
                typeStation = "вокзал";
                break;
            case "station":
                typeStation = "остановка";
                break;
            case "airport":
                typeStation = "аэропорт";
                break;
            case "marine_station":
                typeStation = "морской вокзал";
                break;
            case "stop":
                typeStation = "остановка";
                break;
            case "unknown":
                typeStation = "станция без типа";
                break;
            case "river_port":
                typeStation = "речной вокзал";
                break;
            case "platform":
                typeStation = "платформа";
                break;
            case "port":
                typeStation = "порт";
                break;
            case "post":
                typeStation = "пост";
                break;
            case "wharf":
                typeStation = "пристань";
                break;
            case "crossing":
                typeStation = "разъезд";
                break;
            case "checkpoint":
                typeStation = "блок-пост";
                break;
            case "overtaking_point":
                typeStation = "обгонный пункт";
                break;
            case "port_point":
                typeStation = "портпункт";
                break;
            default:
                typeStation = "";
        }
        return typeStation;
    }

    public static String getTypeStation(STATION_TYPE type) {
        String typeStation = null;
        switch (type) {
            case bus_stop:
                typeStation = "автобусная остановка";
                break;
            case bus_station:
                typeStation = "автовокзал";
                break;
            case train_station:
                typeStation = "вокзал";
                break;
            case station:
                typeStation = "остановка";
                break;
            case airport:
                typeStation = "аэропорт";
                break;
            case marine_station:
                typeStation = "морской вокзал";
                break;
            case stop:
                typeStation = "остановка";
                break;
            case unknown:
                typeStation = "...";
                break;
            case river_port:
                typeStation = "речной вокзал";
                break;
            case platform:
                typeStation = "платформа";
                break;
            case port:
                typeStation = "порт";
                break;
            case post:
                typeStation = "пост";
                break;
            case wharf:
                typeStation = "пристань";
                break;
            case crossing:
                typeStation = "разъезд";
                break;
            case checkpoint:
                typeStation = "блок-пост";
                break;
            case overtaking_point:
                typeStation = "обгонный пункт";
                break;
            case port_point:
                typeStation = "портпункт";
                break;
            default:
                typeStation = "";
        }
        return typeStation;
    }

    public static STATION_TYPE setTypeStation(String typeStation) {
        STATION_TYPE type = null;
        switch (typeStation) {
            case "bus_stop":
                type = STATION_TYPE.bus_stop;
                break;
            case "bus_station":
                type = STATION_TYPE.bus_station;
                break;
            case "train_station":
                type = STATION_TYPE.train_station;
                break;
            case "station":
                type = STATION_TYPE.station;
                break;
            case "airport":
                type = STATION_TYPE.airport;
                break;
            case "marine_station":
                type = STATION_TYPE.marine_station;
                break;
            case "stop":
                type = STATION_TYPE.stop;
                break;
            case "unknown":
                type = STATION_TYPE.unknown;
                break;
            case "river_port":
                type = STATION_TYPE.river_port;
                break;
            case "platform":
                type = STATION_TYPE.platform;
                break;
            case "port":
                type = STATION_TYPE.port;
                break;
            case "post":
                type = STATION_TYPE.post;
                break;
            case "wharf":
                type = STATION_TYPE.wharf;
                break;
            case "crossing":
                type = STATION_TYPE.crossing;
                break;
            case "checkpoint":
                type = STATION_TYPE.checkpoint;
                break;
            case "overtaking_point":
                type = STATION_TYPE.overtaking_point;
                break;
            case "port_point":
                type = STATION_TYPE.port_point;
                break;
            default:
                typeStation = "";
        }
        return type;
    }

    public static String getTransportType(String typeStation) {
        switch (typeStation) {
            case "plane":
                typeStation = "самолет";
                break;
            case "train":
                typeStation = "поезд";
                break;
            case "suburban":
                typeStation = "электричка";
                break;
            case "bus":
                typeStation = "автобус";
                break;
            case "water":
                typeStation = "водный транспорт";
                break;
            case "helicopter":
                typeStation = "вертолет";
                break;
            default:
                typeStation = "";
        }
        return typeStation;
    }

    public static String getEvent(EVENT event)
    {
        String result = "";
        switch (event)
        {
            case arrival:
                result = "прибывающие на станцию";
                break;
            case departure:
                result = "отправляющие на станцию";
                default:
        }
        return result;
    }

    public static String getTransportType(TRANSPORT_TYPE transportType) {
        String type = "";
        switch (transportType) {
            case plane:
                type = "самолет";
                break;
            case train:
                type = "поезд";
                break;
            case suburban:
                type = "электричка";
                break;
            case bus:
                type = "автобус";
                break;
            case water:
                type = "водный транспорт";
                break;
            case helicopter:
                type = "вертолет";
                break;
            default:
                type = "";
        }
        return type;
    }

    public static TRANSPORT_TYPE setTransportType(String typeStation) {
        TRANSPORT_TYPE type = null;
        switch (typeStation) {
            case "plane":
                type = TRANSPORT_TYPE.plane;
                break;
            case "train":
                type = TRANSPORT_TYPE.train;
                break;
            case "suburban":
                type = TRANSPORT_TYPE.suburban;
                break;
            case "bus":
                type = TRANSPORT_TYPE.bus;
                break;
            case "water":
                type = TRANSPORT_TYPE.water;
                break;
            case "helicopter":
                type = TRANSPORT_TYPE.helicopter;
                break;
            default:
                break;
        }
        return type;
    }

    public static TYPE setType(String type) {
        TYPE t = null;
        switch (type) {
            case "station":
                t = TYPE.station;
                break;
            case "settlement":
                t = TYPE.settlement;
                break;
            default:
                break;
        }
        return t;
    }

    public static String getType(TYPE type) {
        String t = null;
        switch (type) {
            case station:
                t = "станция";
                break;
            case settlement:
                t = "населенный пункт";
                break;
            default:
                break;
        }
        return t;
    }

    public static String getType(String type) {
        String t = null;
        switch (type) {
            case "station":
                t = "станция";
                break;
            case "settlement":
                t = "населенный пункт";
                break;
            default:
                break;
        }
        return t;
    }

    public static EXPRESS_TYPE setExpressType(String type) {
        EXPRESS_TYPE t = null;
        switch (type) {
            case "aeroexpress":
                t = EXPRESS_TYPE.aeroexpress;
                break;
            case "express":
                t = EXPRESS_TYPE.express;
                break;
            default:
                break;
        }
        return t;
    }

    public static EVENT setEvent(String event) {
        EVENT event1;
        switch (event) {
            case "departure":
                event1 = EVENT.departure;
                break;
            case "arrival":
                event1 = EVENT.arrival;
                break;
            default:
                event1 = EVENT.departure;
        }
        return event1;
    }

    public static String getExpressType(EXPRESS_TYPE expressType) {
        String type = null;
        switch (expressType) {
            case aeroexpress:
                type = "aeroexpress";
                break;
            case express:
                type = "express";
                break;
            default:
                break;
        }
        return type;
    }

    public static int getImgTypeTransportHint(String typeTransport) {
        int idImg;
        switch (typeTransport) {
            case "plane":
                idImg = R.drawable.fly_logo;
                break;
            case "train":
                idImg = R.drawable.train_logo;
                break;
            case "suburban":
                idImg = R.drawable.surban_logo;
                break;
            case "bus":
                idImg = R.drawable.bus_logo;
                break;
            case "water":
                idImg = R.drawable.water_logo;
                break;
            case "helicopter":
                idImg = R.drawable.helicopter_logo;
                break;
            default:
                idImg = R.color.white;
                break;
        }
        return idImg;
    }

}
