package com.app.nosenko.withtran;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.maps.model.LatLng;
import com.sdsmdg.tastytoast.TastyToast;
import com.xw.repo.BubbleSeekBar;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Adapter.AdapterNearestStation;
import Adapter.AdapterTypeStation;
import Adapter.AdapterTypeTransport;
import DrawableStyle.TransportDrawable;
import Model.NearestStation;
import Model.TypeStation;
import Model.TypeTransport;

public class ActivityNearestStations extends AppCompatActivity implements LocationListener {

    private Spinner spinner;
    private AdapterTypeTransport adapterTypeTransport;
    private List<TypeTransport> transportList;
    private List<TypeStation> typeStations;
    private RecyclerView recyclerView;
    private AdapterNearestStation adapterNearestStation;
    private List<NearestStation> stationList;
    private RecyclerView.LayoutManager layoutManager;
    private Download download;
    private Thread myThread;
    private TextView lan;
    private TextView lng;
    private TextView provider;
    private BubbleSeekBar bubbleSeekBar;
    private LocationManager locationManager;
    private double longitude = 0;
    private double latitude = 0;
    private String nameProvider;
    private PullRefreshLayout mPullToRefreshView;
    private AutoCompleteTextView inputAdres;
    private LinearLayout linearLayout;
    private AutoCompleteTextView autoCompleteTextView;
    private LinearLayout.LayoutParams params;
    private final int FINE_LOCATION_PERMISSION = 9999;
    private Yandex.TRANSPORT_TYPE transportType = null;
    private Spinner spinnerTypeStation;
    private AdapterTypeStation adapterTypeStation;
    private Yandex.STATION_TYPE stationType = null;
    private AdView mAdView;
    private NestedScrollView nestedScrollView;
    private boolean isShowHinte = true;
    private boolean isSearchAdressEdit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearest_stations);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        lan = findViewById(R.id.myLan);
        lng = findViewById(R.id.myLng);
        provider = findViewById(R.id.provider);
        bubbleSeekBar = findViewById(R.id.seekBar);
        bubbleSeekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListenerAdapter() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                bubbleSeekBar.setBubbleColor(Color.HSVToColor(new float[]{57 - (progress + 5), 10, 100}));
                bubbleSeekBar.setSecondTrackColor(Color.HSVToColor(new float[]{57 - (progress + 10), 100, 100}));

                if (isShowHinte) {
                    TastyToast.makeText(getApplicationContext(), "Большой радиус поиска\n может повлиять на производительность", TastyToast.LENGTH_SHORT, TastyToast.INFO).show();
                }

                if ((40 < progress) | (progress < 50)) {
                    isShowHinte = false;
                }
            }
        });
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        mPullToRefreshView = findViewById(R.id.pull_to_refresh);
        mPullToRefreshView.setRefreshDrawable(new TransportDrawable(getApplicationContext(), mPullToRefreshView, getWindowManager().getDefaultDisplay()));
        mPullToRefreshView.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isSearchAdressEdit) {
                            if (isOnline3() & (longitude != 0) & (latitude != 0)) {
                                transportType = ((TypeTransport) spinner.getSelectedItem()).getTransportType();
                                stationType = ((TypeStation) spinnerTypeStation.getSelectedItem()).getStationType();
                                download = new Download(getApplicationContext(), latitude, longitude, bubbleSeekBar.getProgress(), stationType, transportType);
                                download.execute();
                                threadNotifyRecycler();
                            } else
                                TastyToast.makeText(getApplicationContext(), "нет интернета...", Toast.LENGTH_SHORT, TastyToast.ERROR).show();
                            mPullToRefreshView.setRefreshing(false);
                        } else {
                            if (isOnline3()) {
                                Geocoder geocoder = new Geocoder(getApplicationContext());
                                try {
                                    List<Address> addresses = new ArrayList<>();
                                    addresses = geocoder.getFromLocationName(autoCompleteTextView.getText().toString(), 100);
                                    Log.d("addresses", "" + addresses.size() + " " + addresses.get(0).getCountryName()
                                            + " " + addresses.get(0).getAddressLine(0)
                                            + " " + addresses.get(0).getAdminArea());

                                    if (!addresses.isEmpty()) {
                                        download = new Download(getApplicationContext(), addresses.get(0).getLatitude(), addresses.get(0).getLongitude(),
                                                bubbleSeekBar.getProgress(), stationType, transportType);
                                        download.execute();
                                        threadNotifyRecycler();
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
//                                DownloadLocale downloadLocale = new DownloadLocale(autoCompleteTextView.getText().toString());
//                                downloadLocale.execute();
                            } else {
                                TastyToast.makeText(getApplicationContext(), "нет интернета...", Toast.LENGTH_SHORT, TastyToast.ERROR).show();
                            }
                            mPullToRefreshView.setRefreshing(false);
                        }
                    }
                }, 3000);

                nestedScrollView.removeAllViews();
                nestedScrollView.addView(recyclerView);
            }
        });

        nestedScrollView = findViewById(R.id.NSV);

        spinner = findViewById(R.id.spinner);
        spinnerTypeStation = findViewById(R.id.spinnerTypeStation);
        recyclerView = findViewById(R.id.recyclerStations);

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        stationList = new ArrayList<>();

        adapterNearestStation = new AdapterNearestStation(stationList, getApplicationContext());
        recyclerView.setAdapter(adapterNearestStation);

        createSpinnerTypeTransport();
        createSpinnerTypeStation();


        linearLayout = findViewById(R.id.container);

        createInputSearch();

        priview();
//        MobileAds.initialize(this, getResources().getString(R.string.YOUR_ADMOB_APP_ID));
//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("2276B596B2758694F89FA2AACDB23424").build();
//        mAdView.loadAd(adRequest);
    }

    private void priview() {
        nestedScrollView.removeAllViews();
        View instructionView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.priview_instructions, null);
        nestedScrollView.addView(instructionView);
    }

    private void createSpinnerTypeStation() {
        typeStations = new ArrayList<>();
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.bus_station), 0, Yandex.STATION_TYPE.bus_station, 0));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.train_station), 0, Yandex.STATION_TYPE.train_station, 1));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.station), 0, Yandex.STATION_TYPE.station, 2));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.airport), 0, Yandex.STATION_TYPE.airport, 3));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.bus_stop), 0, Yandex.STATION_TYPE.bus_stop, 4));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.marine_station), 0, Yandex.STATION_TYPE.marine_station, 5));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.checkpoint), 0, Yandex.STATION_TYPE.checkpoint, 6));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.crossing), 0, Yandex.STATION_TYPE.crossing, 7));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.overtaking_point), 0, Yandex.STATION_TYPE.overtaking_point, 8));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.platform), 0, Yandex.STATION_TYPE.platform, 9));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.port), 0, Yandex.STATION_TYPE.port, 10));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.river_port), 0, Yandex.STATION_TYPE.river_port, 11));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.stop), 0, Yandex.STATION_TYPE.stop, 12));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.post), 0, Yandex.STATION_TYPE.post, 13));
        typeStations.add(new TypeStation(Yandex.getTypeStation(Yandex.STATION_TYPE.unknown), 0, Yandex.STATION_TYPE.unknown, 14));
        typeStations.add(new TypeStation("любая станция", 0, null, 15));
        adapterTypeStation = new AdapterTypeStation(getApplicationContext(), typeStations);
        spinnerTypeStation.setAdapter(adapterTypeStation);
        spinnerTypeStation.setSelection(15);

    }

    private void createSpinnerTypeTransport() {
        transportList = new ArrayList<>();
        transportList.add(new TypeTransport("автобус", R.drawable.buses_red, Yandex.TRANSPORT_TYPE.bus, 0));
        transportList.add(new TypeTransport("поезд", R.drawable.train_red, Yandex.TRANSPORT_TYPE.train, 1));
        transportList.add(new TypeTransport("электричка", R.drawable.surban_red, Yandex.TRANSPORT_TYPE.suburban, 2));
        transportList.add(new TypeTransport("самолет", R.drawable.fly_red, Yandex.TRANSPORT_TYPE.plane, 3));
        transportList.add(new TypeTransport("вертолет", R.drawable.helicopter_red, Yandex.TRANSPORT_TYPE.helicopter, 4));
        transportList.add(new TypeTransport("водное судно", R.drawable.ship_red, Yandex.TRANSPORT_TYPE.water, 5));
        transportList.add(new TypeTransport("весь транспорт ", R.drawable.all, null, 6));

        adapterTypeTransport = new AdapterTypeTransport(getApplicationContext(), transportList);
        spinner.setAdapter(adapterTypeTransport);
    }


    private void createInputSearch() {
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        autoCompleteTextView = new AutoCompleteTextView(getApplicationContext());
        autoCompleteTextView.setLayoutParams(params);
        autoCompleteTextView.setHint("Введите адрес");
        autoCompleteTextView.setBackground(getResources().getDrawable(R.drawable.background_search));
        autoCompleteTextView.setTextColor(getResources().getColor(R.color.black));
        autoCompleteTextView.setHintTextColor(getResources().getColor(R.color.silver));
        autoCompleteTextView.setPadding(10, 5, 10, 5);

        final Bitmap original = BitmapFactory.decodeResource(getResources(), R.drawable.clear);
        final Bitmap b = Bitmap.createScaledBitmap(original, 40, 28, false);
        final Drawable x = new BitmapDrawable(getResources(), b);
        x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());

        autoCompleteTextView.setCompoundDrawables(null, null, autoCompleteTextView.getText().toString().equals("") ? null : x, null);
        autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (autoCompleteTextView.getCompoundDrawables()[2] == null) {
                    return false;
                }
                if (event.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }
                if (event.getX() > autoCompleteTextView.getWidth() - autoCompleteTextView.getPaddingRight() - x.getIntrinsicWidth()) {
                    autoCompleteTextView.setText("");
                    autoCompleteTextView.setCompoundDrawables(null, null, null, null);
                }
                return false;
            }
        });
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                autoCompleteTextView.setCompoundDrawables(null, null, autoCompleteTextView.getText().toString().equals("") ? null : x, null);
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });
//        LinearLayout.LayoutParams params = null;
//        params.setMargins(0, 10, 0, 10);
//        autoCompleteTextView.setLayoutParams(params);
    }

    private void threadNotifyRecycler() {
        myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    Log.d("myThread+", "Привет из потока " + Thread.currentThread().getName());

                    if (download.isSet) {
                        stationList.clear();
                        try {
                            for (int i = 0; i < download.stations.size(); i++) {
                                stationList.add(download.stations.get(i));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ActivityNearestStations.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.getAdapter().notifyDataSetChanged();
                                Log.d("myThread+", "Привет из потока runOnUiThread");
                            }
                        });

                        myThread.interrupt();

                        Log.d("myThread+", "сдохни " + Thread.currentThread().getName());
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        });
        myThread.start();
    }

    protected boolean isOnline3() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        return cm.getActiveNetworkInfo() != null;
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_PERMISSION);
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        getLocation(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        provider.setText(nameProvider);
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void formatLocation(Location location) {
        longitude = location.getLongitude();
        Log.d("locale_lon", String.valueOf(location.getLongitude()));
        lng.setText(String.valueOf(location.getLongitude()));
        latitude = location.getLatitude();
        Log.d("locale_lan", String.valueOf(location.getLatitude()));
        lan.setText(String.valueOf(location.getLatitude()));
        nameProvider = location.getProvider();
        provider.setText(nameProvider);
        provider.setTextColor(getResources().getColor(R.color.lime));
    }

    void getLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            formatLocation(location);
        } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
            formatLocation(location);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_get_gps:

                if (item.isChecked()) {
                    item.setChecked(false);
                    isSearchAdressEdit = true;
                    linearLayout.addView(autoCompleteTextView, 6);
                } else {
                    item.setChecked(true);
                    isSearchAdressEdit = false;
                    linearLayout.removeView(autoCompleteTextView);
                }
                return true;
            case R.id.action_sort:

                createDialogCustom();
                return true;
            case R.id.action_all_station_to_maps:
                Intent intent = new Intent(getApplicationContext(), ActivityMaps.class);
                intent.putExtra("nearestAllStation", (Serializable) stationList);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void createDialogCustom() {

        View body = LayoutInflater.from(getApplicationContext()).inflate(R.layout.sort_layout, null);
        final CheckBox checkBox = body.findViewById(R.id.checkBox);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    checkBox.setChecked(true);
                    checkBox.setText("по возрвствнию");
                } else {
                    checkBox.setChecked(false);
                    checkBox.setText("по убыванию");
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.bus_station);
        builder.setTitle("Сортировка")
                .setView(body)
                .setCancelable(false)
                .setItems(new CharSequence[]{"название станции", "тип станции", "дистанция"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                Collections.sort(stationList, new Comparator<NearestStation>() {
                                    @Override
                                    public int compare(NearestStation t1, NearestStation t2) {
                                        if (checkBox.isChecked())
                                            return t1.getTitle().compareTo(t2.getTitle());
                                        else
                                            return t2.getTitle().compareTo(t1.getTitle());
                                    }
                                });
                                recyclerView.getAdapter().notifyDataSetChanged();
                                break;
                            case 1:
                                Collections.sort(stationList, new Comparator<NearestStation>() {
                                    @Override
                                    public int compare(NearestStation t1, NearestStation t2) {
                                        if (checkBox.isChecked())
                                            return t1.getStationType().compareTo(t2.getStationType());
                                        else
                                            return t2.getStationType().compareTo(t1.getStationType());
                                    }
                                });
                                recyclerView.getAdapter().notifyDataSetChanged();
                                break;
                            case 2:
                                Collections.sort(stationList, new Comparator<NearestStation>() {
                                    @Override
                                    public int compare(NearestStation t1, NearestStation t2) {
                                        if (checkBox.isChecked())
                                            return t1.getDistance().compareTo(t2.getDistance());
                                        else
                                            return t2.getDistance().compareTo(t1.getDistance());
                                    }
                                });
                                recyclerView.getAdapter().notifyDataSetChanged();
                                break;
                            default:
                                break;
                        }
                    }
                }).setNegativeButton("отмена", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int n) {
                dialogInterface.cancel();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_nearest_stations, menu);
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int position = -1;
        try {
            position = ((AdapterNearestStation) recyclerView.getAdapter()).getPosition();
        } catch (Exception e) {
            Log.d("nearestactivity", e.getLocalizedMessage(), e);
            return super.onContextItemSelected(item);
        }

        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_link_nearest_station:
                intent = new Intent(getApplicationContext(), ActivityWebView.class);
                Log.d("nearestLink", stationList.get(position).getTouchUrl());
                intent.putExtra("nearestLink", stationList.get(position).getTouchUrl());
                startActivity(intent);
                break;
            case R.id.action_nearest_station_map:
                intent = new Intent(getApplicationContext(), ActivityMaps.class);
                Log.d("nearestLinkMap", stationList.get(position).getTransportType());
                LatLng latLng = new LatLng(Double.valueOf(stationList.get(position).getLat()), Double.valueOf(stationList.get(position).getLng()));
                intent.putExtra("lanlngMap", latLng);
                intent.putExtra("nameStationMap", stationList.get(position).getTitle());
                intent.putExtra("typeStationMap", stationList.get(position).getStationType());
                intent.putExtra("distanceRadius", stationList.get(position).getDistance());

                startActivity(intent);
                break;
            case R.id.action_add_bookmarks_station:

                break;
        }

        return super.onContextItemSelected(item);
    }
}
