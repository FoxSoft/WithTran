package com.app.nosenko.withtran;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.nosenko.withtran.DBAdapter;
import com.nex3z.notificationbadge.NotificationBadge;

import Adapter.AdapterPage;

public class ActivityBookMark extends AppCompatActivity {

    private AdapterPage adapterPager;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private HelperTabTitle helperTabTitle1;
    private HelperTabTitle helperTabTitle2;
    private DBAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_mark);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        adapterPager = new AdapterPage(getSupportFragmentManager());
        viewPager = findViewById(R.id.viewPager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setCustomView(R.layout.title_tab);
        tabLayout.getTabAt(1).setCustomView(R.layout.title_tab);

        dbAdapter = new DBAdapter(getApplicationContext());

        helperTabTitle1 = new HelperTabTitle(tabLayout.getTabAt(0).getCustomView());
        Log.d("BookMark", String.valueOf(dbAdapter.getBookMark()));
        helperTabTitle1.setNumberBadge(dbAdapter.getBookMark().size());
        helperTabTitle1.setImg(R.drawable.route);
        helperTabTitle1.setTitle("рейсы");

        helperTabTitle2 = new HelperTabTitle(tabLayout.getTabAt(1).getCustomView());
        helperTabTitle2.setImg(R.drawable.station);
        helperTabTitle2.setNumberBadge(0);
        helperTabTitle2.setTitle("станции");

    }


    private void setupViewPager(ViewPager viewPager) {
        adapterPager = new AdapterPage(getSupportFragmentManager());
        adapterPager.addFragment(new Tab1(), "Рейсы");
        adapterPager.addFragment(new Tab2(), "tab2");
        viewPager.setAdapter(adapterPager);
    }

    private class HelperTabTitle {
        private NotificationBadge badge;
        private int click = 0;
        private ImageView img;
        private int number;
        private RelativeLayout relativeLayout;
        private TextView title;
        private View view;

        public HelperTabTitle(View v) {
            view = v;
            badge = view.findViewById(R.id.badge);
            title = view.findViewById(R.id.nameStation);
            img = view.findViewById(R.id.icon);
            relativeLayout = view.findViewById(R.id.RL);
        }

        public void setNumberBadge(int n) {
            badge.setNumber(n);
            number = n;
        }

        public void incNumerBadge() {
            NotificationBadge notificationBadge = badge;
            int i = number + 1;
            number = i;
            notificationBadge.setNumber(i);
        }

        public void setImg(int idImg) {
            img.setImageResource(idImg);
        }

        public void setTitle(String s) {
            title.setText(s);
        }
    }


}
