package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.app.nosenko.withtran.R;
import com.app.nosenko.withtran.Yandex;
import com.xw.repo.BubbleSeekBar;

/**
 * Created by eminem on 06.02.2018.
 */

public class AdapterListSearch extends RecyclerView.Adapter<AdapterListSearch.ViewHolder> {

    private Yandex.SearchAPI searchAPI;
    private Context context;
    private LayoutInflater inflater;
    private int position;


    public int getPosition() {
        return position;
    }

    public void setSearchAPI(Yandex.SearchAPI searchAPI) {
        this.searchAPI = searchAPI;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public AdapterListSearch(Context context, Yandex.SearchAPI searchAPI) {
        this.inflater = LayoutInflater.from(context);
        this.searchAPI = searchAPI;
        this.context = context;
    }

    @Override
    public boolean onFailedToRecycleView(ViewHolder holder) {
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.row, parent, false);
            return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        viewHolder.itemView.setId(position);
        viewHolder.TNumber.setText(searchAPI.getSegments().get(position).getThread().getNumber());
        viewHolder.TFromTime.setText(searchAPI.getSegments().get(position).getDeparture());
        viewHolder.TFrom.setText(searchAPI.getSegments().get(position).getFrom().getTitle());
        viewHolder.TTo.setText(searchAPI.getSegments().get(position).getTo().getTitle());
        viewHolder.TToTime.setText(searchAPI.getSegments().get(position).getArrival());
        viewHolder.TDuration.setText(searchAPI.getSegments().get(position).getDuration());
       // viewHolder.TDays.setText(searchAPI.getSegments().get(position).getDays());
        viewHolder.TNameReys.setText((searchAPI.getSegments().get(position)).getThread().getTitle());
        viewHolder.TStatus.setText(searchAPI.getSegments().get(position).getTransportView().getStatus());
        viewHolder.Img.setImageResource(searchAPI.getSegments().get(position).getTransportView().getIdImg());

        if ((searchAPI.getSegments().get(position).getTransportView().isTrack_visible())) {
            View track = inflater.inflate(R.layout.track, null);
            ViewHolderTrack viewHolderTrack = new ViewHolderTrack(track);

            viewHolderTrack.startTime.setText("0 мин.");
            viewHolderTrack.finishTime.setText(searchAPI.getSegments().get(position).getTransportView().getTrack_max().toString() + " мин.");

            viewHolderTrack.seekBar.setVisibility(View.VISIBLE);
            viewHolderTrack.bubbleSeekBar.setVisibility(View.VISIBLE);
            viewHolderTrack.startTime.setVisibility(View.VISIBLE);
            viewHolderTrack.finishTime.setVisibility(View.VISIBLE);

            viewHolderTrack.seekBar.setMax(searchAPI.getSegments().get(position).getTransportView().getTrack_max());
            viewHolderTrack.seekBar.setProgress(searchAPI.getSegments().get(position).getTransportView().getPosition());
            //рабоатет смена ползунка при разном типе транспорта
            viewHolderTrack.seekBar.setThumb(context.getResources().getDrawable(searchAPI.getSegments().get(position).getTransportView().getThumb()));
            viewHolderTrack.bubbleSeekBar.getConfigBuilder().max(searchAPI.getSegments().get(position).getTransportView().getTrack_max()).build();
            viewHolderTrack.bubbleSeekBar.setProgress(searchAPI.getSegments().get(position).getTransportView().getPosition());

            viewHolder.TStatus.setBackgroundResource(R.color.color_ride);
            viewHolder.linearLayout.addView(track, 2);
        } else {
//            viewHolder.seekBar.setVisibility(View.INVISIBLE);
//            viewHolder.bubbleSeekBar.setVisibility(View.INVISIBLE);
//            viewHolder.startTime.setVisibility(View.INVISIBLE);
//            viewHolder.finishTime.setVisibility(View.INVISIBLE);
        }

        viewHolder.TStatus.setBackgroundResource(searchAPI.getSegments().get(position).getTransportView().getColorStatusTitle());

        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setPosition(viewHolder.getPosition());
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchAPI.getSegments().size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        private ImageView Img;
     //   private TextView TDays;
        private TextView TDuration;
        private TextView TFrom;
        private TextView TFromTime;
        private TextView TNameReys;
        private TextView TNumber;
        private TextView TStatus;
        private TextView TTo;
        private TextView TToTime;
        private LinearLayout linearLayout;

        public void refreshBlockOverlay(int position) {
            notifyItemChanged(position);
        }

        public ViewHolder(View view) {
            super(view);
            this.TFrom = view.findViewById(R.id.fromStation);
            this.TTo = view.findViewById(R.id.toStation);
            this.TToTime = view.findViewById(R.id.arrival);
            this.TFromTime = view.findViewById(R.id.fromTime);
            this.TStatus = view.findViewById(R.id.status);
           // this.TDays = view.findViewById(R.id.days);
            this.TDuration = view.findViewById(R.id.duration);
            this.TNameReys = view.findViewById(R.id.nameStation);
            this.TNumber = view.findViewById(R.id.number);
            this.Img = view.findViewById(R.id.imageView);
            this.linearLayout = view.findViewById(R.id.linnaer);
            view.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            MenuInflater menuInflater = new MenuInflater(context);
            menuInflater.inflate(R.menu.popup_menu_search, menu);

            View header = inflater.inflate(R.layout.title_popmenu_search, null);
            TextView titleReys = header.findViewById(R.id.nameStation);
            TextView number = header.findViewById(R.id.number);
            titleReys.setText(TNameReys.getText());
            number.setBackground(TStatus.getBackground());
            number.append(TNumber.getText() + " ");
            menu.setHeaderView(header);
        }
    }

    public class ViewHolderTrack{
        private BubbleSeekBar bubbleSeekBar;
        private TextView finishTime;
        private SeekBar seekBar;
        private TextView startTime;

        public ViewHolderTrack(View view) {
            this.bubbleSeekBar = view.findViewById(R.id.track_distation);
            this.seekBar = view.findViewById(R.id.track_bus);
            this.startTime = view.findViewById(R.id.t1);
            this.finishTime = view.findViewById(R.id.t2);
        }
    }
}
