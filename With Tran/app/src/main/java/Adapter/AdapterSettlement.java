package Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.nosenko.withtran.R;
import com.app.nosenko.withtran.Yandex;

import java.util.List;

import Model.Settlement;

/**
 * Created by eminem on 18.12.2017.
 */

public class AdapterSettlement extends BaseAdapter {
    private List<Settlement> settlements;
    private Context context;

    public AdapterSettlement(Context context, List<Settlement> settlements) {
        this.context = context;
        this.settlements = settlements;
    }

    @Override
    public int getCount() {
        return settlements.size();
    }

    @Override
    public Object getItem(int i) {
        return settlements.get(i);
    }

    @Override
    public long getItemId(int i) {
        return settlements.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ViewHolder viewHolder;
        if(view == null) {
            v = View.inflate(this.context, R.layout.item_settlement, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)view.getTag();
        }

        viewHolder.distance.setText(String.valueOf(settlements.get(i).getDistance()));
        viewHolder.lng.setText(String.valueOf(settlements.get(i).getLng()));
        viewHolder.lat.setText(String.valueOf(settlements.get(i).getLat()));
        viewHolder.title.setText(settlements.get(i).getTitle());
        viewHolder.type.setText(Yandex.getType(settlements.get(i).getType()));
        return v;
    }

    private class ViewHolder {

        public TextView distance;
        public TextView lat;
        public TextView lng;
        public TextView type;
        public TextView title;

        public ViewHolder(View view) {
            this.distance = view.findViewById(R.id.distance);
            this.lat = view.findViewById(R.id.lat);
            this.lng = view.findViewById(R.id.lng);
            this.type = view.findViewById(R.id.type);
            this.title = view.findViewById(R.id.nameStation);

        }
    }
}
