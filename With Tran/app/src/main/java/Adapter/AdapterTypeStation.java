package Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.nosenko.withtran.R;

import java.util.List;

import Model.TypeStation;

/**
 * Created by eminem on 17.03.2018.
 */

public class AdapterTypeStation extends BaseAdapter {
    private Context context;
    private List<TypeStation> stationList;

    public AdapterTypeStation(Context context, List<TypeStation> stationList) {
        this.context = context;
        this.stationList = stationList;
    }

    @Override
    public int getCount() {
        return stationList.size();
    }

    @Override
    public Object getItem(int i) {
        return stationList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return stationList.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ViewHolder viewHolder;
        if(view == null) {
            v = View.inflate(this.context, R.layout.row_spinner_type_station, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)view.getTag();
        }
        viewHolder.title.setText(stationList.get(i).getTitle());
        return v;
    }

    private class ViewHolder {
        public TextView title;

        public ViewHolder(View view) {
            this.title = view.findViewById(R.id.titleStation);
        }
    }
}
