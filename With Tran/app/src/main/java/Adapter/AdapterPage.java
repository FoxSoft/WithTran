package Adapter;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eminem on 30.12.2017.
 */

public class AdapterPage extends FragmentPagerAdapter {

    private Context context;
    private List<Fragment> fragmentLIst = new ArrayList();
    private List<Integer> iconPager = new ArrayList();
    private TabLayout tabLayout;
    private List<String> titlePager = new ArrayList();

    public AdapterPage(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentLIst.get(position);
    }

    public void addFragment(Fragment fragment, String title) {
        this.fragmentLIst.add(fragment);
        this.titlePager.add(title);
    }

    public CharSequence getPageTitle(int position) {
        return titlePager.get(position);
    }

    @Override
    public int getCount() {
        return fragmentLIst.size();
    }
}
