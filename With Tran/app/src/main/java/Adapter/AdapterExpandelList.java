package Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.nosenko.withtran.R;
import com.app.nosenko.withtran.Yandex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Model.GroupItem;


/**
 * Created by eminem on 18.11.2017.
 */

public class AdapterExpandelList extends BaseExpandableListAdapter {

    private final Context context;
    private Activity activity;
    private HashMap<GroupItem, List<Yandex.SearchAPI.Segments>> childtems = new HashMap<>();
    private LayoutInflater inflater;
    public List<GroupItem> parentItems = new ArrayList<>();
    private AdapterExpandelList adapterExpandelList = null;

    public AdapterExpandelList(Context context, List<GroupItem> parents, HashMap<GroupItem, List<Yandex.SearchAPI.Segments>> childern) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.parentItems = parents;
        this.childtems = childern;
    }

    public void setInflater(LayoutInflater inflater, Activity activity) {
        this.inflater = inflater;
        this.activity = activity;
    }

    @Override
    public int getGroupCount() {
        return parentItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childtems.get(parentItems.get(groupPosition)).size();
    }

    @Override
    public GroupItem getGroup(int groupPosition) {
        return parentItems.get(groupPosition);
    }

    @Override
    public Yandex.SearchAPI.Segments getChild(int groupPosition, int childPosition) {
        return childtems.get(parentItems.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_activity_list_group_item, null);
        }

        final ImageView image = convertView.findViewById(R.id.sample_activity_list_group_item_image);
        image.setImageResource(parentItems.get(groupPosition).getIdImg());

        final TextView text = convertView.findViewById(R.id.sample_activity_list_group_item_text);
        text.setText(parentItems.get(groupPosition).getNameReys());

        final TextView titleGroup = convertView.findViewById(R.id.countReys);
        titleGroup.setText(parentItems.get(groupPosition).getCount());

//        convertView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                parentItems.remove(groupPosition);
//                notifyDataSetChanged();
//                return false;
//            }
//        });

        return convertView;
    }

    public void removeGroup(int position)
    {
        parentItems.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        List<Yandex.SearchAPI.Segments> child = childtems.get(parentItems.get(groupPosition));
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sample_activity_list_child_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.nameReys.setText(child.get(childPosition).getThread().getTitle());

        viewHolder.arrival.setText(child.get(childPosition).getArrival());

        viewHolder.departure.setText(child.get(childPosition).getDeparture());

        viewHolder.duration.setText(child.get(childPosition).getDuration());

        viewHolder.number.setText("№: " + child.get(childPosition).getThread().getNumber());

        viewHolder.img.setImageResource(Yandex.getImgTypeTransport(child.get(childPosition).getThread().getTransportType()));

        return convertView;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class ViewHolder  {
        private TextView number;
        private TextView duration;
        private TextView departure;
        private TextView arrival;
        private TextView nameReys;
        private ImageView img;

        public ViewHolder(View v) {
            nameReys = v.findViewById(R.id.nameStation);
            arrival = v.findViewById(R.id.arrival);
            departure = v.findViewById(R.id.departure);
            number = v.findViewById(R.id.number);
            duration = v.findViewById(R.id.duration);
            img = v.findViewById(R.id.typeTransport);
        }
    }
}
