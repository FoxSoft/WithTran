package Adapter;

        import android.content.Context;
        import android.support.v7.widget.RecyclerView;
        import android.view.ContextMenu;
        import android.view.LayoutInflater;
        import android.view.MenuInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.TextView;
        import com.app.nosenko.withtran.R;
        import java.util.List;
        import Model.NearestStation;

/**
 * Created by eminem on 25.12.2017.
 */

public class AdapterNearestStation extends RecyclerView.Adapter<AdapterNearestStation.ViewHolder> {

    private List<NearestStation> stations;
    private Context context;
    private LayoutInflater inflater;
    private int position;

    public AdapterNearestStation(List<NearestStation> stations, Context context) {
        this.stations = stations;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public boolean onFailedToRecycleView(ViewHolder holder) {
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_nearest_station, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
//        viewHolder.stationType.setText(Yandex.getTypeStation(stations.get(position).getStationType()));
        viewHolder.nameStation.setText(stations.get(position).getTitle());
        viewHolder.distance.setText(stations.get(position).getDistance());
        viewHolder.lat.setText(stations.get(position).getLat());
        viewHolder.lng.setText(stations.get(position).getLng());
        viewHolder.majority.setText(stations.get(position).getMajority());
        viewHolder.stationTypeName.setText(stations.get(position).getStationTypeName());
        viewHolder.transportTypeImg.setImageResource((stations.get(position)).getTypeTransportImg());



        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setPosition(viewHolder.getPosition());
                return false;
            }
        });

        //     Intent intent = new Intent(context.getApplicationContext(), ActivityWebView.class);
                    //    intent.putExtra("link", stations.get(position).getTouchUrl());
                   //     context.startActivity(intent.setFlags(FLAG_ACTIVITY_NEW_TASK));
    }

    public int getPosition() {
        return position;
    }

    private void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        private TextView distance;
        private TextView nameStation;
      //  private TextView stationType;
        private TextView majority;
        private TextView lat;
        private TextView lng;
        private ImageView transportTypeImg;
        private TextView stationTypeName;



        public ViewHolder(final View view) {
            super(view);
            this.distance = view.findViewById(R.id.distance);
            this.nameStation = view.findViewById(R.id.nameStation);
           // this.stationType = (TextView) view.findViewById(R.id.stationType);
            this.majority = view.findViewById(R.id.majority);
            this.lat = view.findViewById(R.id.lat);
            this.lng = view.findViewById(R.id.lng);
            this.transportTypeImg = view.findViewById(R.id.transportTypeImg);
            this.stationTypeName = view.findViewById(R.id.stationTypeName);
            this.lat = view.findViewById(R.id.lat);
            view.setOnCreateContextMenuListener(this);
        }


        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            MenuInflater menuInflater = new MenuInflater(context);
            menuInflater.inflate(R.menu.popup_menu_nearest_stations, contextMenu);
            View header = inflater.inflate(R.layout.title_popmenu_nearest_stations, null);
            TextView nameStationTitle = header.findViewById(R.id.nameStation);
            nameStationTitle.setText(nameStation.getText());
            contextMenu.setHeaderView(header);
        }
    }
}
