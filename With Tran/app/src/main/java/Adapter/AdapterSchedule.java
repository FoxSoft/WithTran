package Adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.nosenko.withtran.R;
import com.app.nosenko.withtran.Yandex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Model.ReysSchedule;

/**
 * Created by eminem on 04.03.2018.
 */

public class AdapterSchedule extends BaseExpandableListAdapter {

    private Context context;
    public List<ReysSchedule> expListTitle;
    private HashMap<ReysSchedule, Yandex.ScheduleAPI.Schedule> expListDetail;
    private Yandex.ScheduleAPI scheduleAPI;
    private String startTime;
    private ExpandableListView listView;

    public AdapterSchedule(Context context, Yandex.ScheduleAPI scheduleAPI, ExpandableListView listView) {
        this.context = context;
        this.scheduleAPI = scheduleAPI;
        this.listView = listView;

        expListTitle = new ArrayList<>();
        expListDetail = new HashMap<>();

        for (int i = 0; i < scheduleAPI.getSchedule().size(); i++) {
            expListTitle.add(new ReysSchedule(scheduleAPI.getSchedule().get(i).getThread().getTitle(),
                    Yandex.getTransportType(scheduleAPI.getSchedule().get(i).getThread().getTransportType()),
                    scheduleAPI.getSchedule().get(i).getThread().getNumber(), scheduleAPI.getSchedule().get(i).getThread().getUid(),
                    Yandex.getImgTypeTransportLogo(scheduleAPI.getSchedule().get(i).getThread().getTransportType())));
            expListDetail.put(expListTitle.get(i), scheduleAPI.getSchedule().get(i));
        }
    }

    public String getStartTime() {
        return startTime;
    }

    private void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public List<ReysSchedule> getExpListTitle() {
        return expListTitle;
    }

    public void setExpListTitle(List<ReysSchedule> expListTitle) {
        this.expListTitle = expListTitle;
    }

    @Override
    public int getGroupCount() {
        return expListTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return expListTitle.get(groupPosition);
    }

    @Override
    public Yandex.ScheduleAPI.Schedule getChild(int groupPosition, int childPosition) {
        return expListDetail.get(expListTitle.get(groupPosition));
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        // получаем родительский элемент

        ReysSchedule title = (ReysSchedule) getGroup(groupPosition);
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_group_schedule, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.nameReys.setText(title.getNameReys());
        viewHolder.typeTransport.setText(title.getTypeTransport());
        viewHolder.number.setText(title.getNumber());
        viewHolder.imgTypeTransport.setImageResource(title.getTypeTransportImg());

        final int resId = isExpanded ? R.drawable.colopse_off : R.drawable.colopse_on;
        viewHolder.expandedImage.setImageResource(resId);

        return convertView;
    }



    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // получаем дочерний элемент
        Yandex.ScheduleAPI.Schedule expListText = getChild(groupPosition, childPosition);
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_cheild_schedule, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.stopTime.setText(expListText.getStops());
        viewHolder.arrival.setText(expListText.getArrival());
        viewHolder.departure.setText(expListText.getDeparture());
        viewHolder.isFuzzy.setText(Yandex.isFuzzy(expListText.getFuzzy()));
        viewHolder.days.setText(expListText.getDays());
        viewHolder.carrierTitle.setText(expListText.getThread().getCarrier().getTitle());
        viewHolder.titleTransportSubtype.setText(Html.fromHtml(expListText.getThread().getTransportSubtype().getTitle()));
        viewHolder.img.setImageResource(Yandex.getImgTypeTransportLogo(expListText.getThread().getTransportType()));

        try {
            if (expListText.getThread().getTransportSubtype().getColor() != null)
                viewHolder.color.setBackgroundColor(Color.parseColor(expListText.getThread().getTransportSubtype().getColor()));
        } catch (Exception e){
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class ViewHolder{
        private TextView departure;
        private ImageView expandedImage;
        private TextView number;
        private TextView nameReys;
        private TextView arrival;
        private ImageView img;
        private ImageView imgTypeTransport;
        private TextView stopTime;
        private TextView titleTransportSubtype;
        private TextView typeTransport;
        private TextView days;
        private TextView isFuzzy;
        private TextView carrierTitle;
        private ImageView color;

        public ViewHolder(View v) {
            nameReys = v.findViewById(R.id.nameStation);
            arrival = v.findViewById(R.id.arrival);
            departure = v.findViewById(R.id.departure);
            stopTime = v.findViewById(R.id.stops);
            titleTransportSubtype = v.findViewById(R.id.titleTransport);
            number = v.findViewById(R.id.number);
            typeTransport = v.findViewById(R.id.typeTransport);
            days = v.findViewById(R.id.days);
            isFuzzy = v.findViewById(R.id.fuzzy);
            color = v.findViewById(R.id.color);
            carrierTitle = v.findViewById(R.id.carrierTitle);
            img = v.findViewById(R.id.transportTypeImg);
            imgTypeTransport = v.findViewById(R.id.img);
            expandedImage = (ImageView) v.findViewById(R.id.imgExpan);
        }
    }
}
