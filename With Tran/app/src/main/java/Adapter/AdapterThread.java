package Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.text.Html;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.nosenko.withtran.Format;
import com.app.nosenko.withtran.R;
import com.app.nosenko.withtran.Yandex;
import com.github.vipulasri.timelineview.TimelineView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Model.StationThread;


public class AdapterThread extends BaseExpandableListAdapter {

    private Context context;
    public List<StationThread> expListTitle;
    private HashMap<StationThread, Yandex.ThreadAPI.Stops> expListDetail;
    private Yandex.ThreadAPI threadAPI;
    private String startTime;
    private int position = 0;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public AdapterThread(Context context, Yandex.ThreadAPI threadAPI) {
        this.context = context;
        this.threadAPI = threadAPI;

        expListTitle = new ArrayList<>();
        expListDetail = new HashMap<>();

        for (int i = 0; i < threadAPI.getStops().size(); i++) {
            expListTitle.add(new StationThread(threadAPI.getStops().get(i).getStation().getTitle(),
                    threadAPI.getStops().get(i).getStation().getStationTypeName(), threadAPI.getStops().get(i).getStation().getCode()));
            expListDetail.put(expListTitle.get(i), threadAPI.getStops().get(i));
        }

        setStartTime(threadAPI.getStartTime());
    }

    public String getStartTime() {
        return startTime;
    }

    private void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public List<StationThread> getExpListTitle() {
        return expListTitle;
    }

    public void setExpListTitle(List<StationThread> expListTitle) {
        this.expListTitle = expListTitle;
    }

    public void notyfyData()
    {
        for (int i = 0; i < threadAPI.getStops().size(); i++) {
            expListTitle.add(new StationThread(threadAPI.getStops().get(i).getStation().getTitle(),
                    threadAPI.getStops().get(i).getStation().getStationTypeName(), threadAPI.getStops().get(i).getStation().getCode()));
            expListDetail.put(expListTitle.get(i), threadAPI.getStops().get(i));
        }
    }

    @Override
    public int getGroupCount() {
        return expListTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return expListTitle.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return expListDetail.get(expListTitle.get(groupPosition));
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        // получаем родительский элемент

        StationThread title = (StationThread) getGroup(groupPosition);
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_thread_group, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.nameStation.setText(title.getNameStation());
        viewHolder.typeStation.setText(title.getTypeStation());
        viewHolder.titleStatus.setText(title.getStatus());
        viewHolder.frameLayout.getBackground().setColorFilter(Color.parseColor(title.getColorBackground()), PorterDuff.Mode.DARKEN);

        final int resId = isExpanded ? R.drawable.plus : R.drawable.minus;
        viewHolder.expandedImage.setImageResource(resId);

        return convertView;
    }



    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // получаем дочерний элемент
        Yandex.ThreadAPI.Stops expListText = (Yandex.ThreadAPI.Stops) getChild(groupPosition, childPosition);
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_thread_cheild, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.stopTime.setText(expListText.getStopTime());
        viewHolder.arrival.setText(expListText.getArrival());
        viewHolder.departure.setText(expListText.getDeparture());
        viewHolder.duration.setText(Format.FormatDurationTotime(expListText.getDuration()));
        viewHolder.titleTransportSubtype.setText(Html.fromHtml(threadAPI.getTransportSubtype().getTitle()));
        viewHolder.img.setImageResource(Yandex.getImgTypeTransportLogo(threadAPI.getTransportType()));

        try {
            if (threadAPI.getTransportSubtype().getColor() != null)
                viewHolder.color.setBackgroundColor(Color.parseColor(threadAPI.getTransportSubtype().getColor()));
        } catch (Exception e){
            e.printStackTrace();
        }
//        viewHolder.typeStation.setText(expListText.getStation().getStationTypeName());
//        viewHolder.timelineView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context.getApplicationContext(), "ddd", Toast.LENGTH_SHORT).show();
//            }
//        });


        //    viewHolder.img.setColorFilter(Color.parseColor(expListTitle.getTransportSubtype().getColor()));
        //   viewHolder.titleTransportSubtype.setText(expListTitle.getTransportSubtype().getTitle());

        // Drawable drawable = img.getDrawable();
        // img.setBackgroundColor(Color.parseColor("#FF7F44"));
        // img.setColorFilter(Color.parseColor("#14bdff"));
        //drawable.setColorFilter(Color.parseColor("#14bdff"), PorterDuff.Mode.XOR);
        //img.setImageDrawable(drawable);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class ViewHolder implements View.OnCreateContextMenuListener{
        private TextView departure;
        private ImageView expandedImage;
        private TimelineView timelineView;
        private TextView duration;
        private TextView nameStation;
        private TextView arrival;
        private ImageView img;
        private TextView stopTime;
        private TextView titleTransportSubtype;
        private TextView typeStation;
        private ImageView color;
        private TextView titleStatus;
        private FrameLayout frameLayout;

        public ViewHolder(View v) {
            nameStation = v.findViewById(R.id.nameStation);
            arrival = v.findViewById(R.id.arrival);
            departure = v.findViewById(R.id.departure);
            stopTime = v.findViewById(R.id.stopTime);
            titleTransportSubtype = v.findViewById(R.id.titleTransport);
            duration = v.findViewById(R.id.duration);
            timelineView = v.findViewById(R.id.time_marker);
            typeStation = v.findViewById(R.id.typeStation);
            color = v.findViewById(R.id.color);
            img = v.findViewById(R.id.transportTypeImg);
            expandedImage = v.findViewById(R.id.sample_activity_list_group_expanded_image);

            frameLayout = v.findViewById(R.id.franmeL);
            titleStatus = v.findViewById(R.id.titleStatus);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            MenuInflater menuInflater = new MenuInflater(context);
            menuInflater.inflate(R.menu.popup_menu_thread, menu);

            LayoutInflater inflater =  LayoutInflater.from(context);
            View header = inflater.inflate(R.layout.title_popmenu_thread, null);
            TextView titleReys = header.findViewById(R.id.nameStation);
            TextView number = header.findViewById(R.id.number);
            titleReys.setText(nameStation.getText());
            //number.setBackground(TStatus.getBackground());
            number.append(typeStation.getText());
            menu.setHeaderView(header);
        }
    }
}
