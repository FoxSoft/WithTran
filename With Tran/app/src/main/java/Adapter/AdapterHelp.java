package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.nosenko.withtran.R;

import java.util.ArrayList;
import java.util.List;

import Model.Help;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by eminem on 17.03.2018.
 */

public class AdapterHelp extends RecyclerView.Adapter<AdapterHelp.ViewHolder> {
    private LayoutInflater inflater;
    private List<Help> helpList = new ArrayList<>();
    private Context context;

    public AdapterHelp(Context context, List<Help> helpList) {
        this.helpList = helpList;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterHelp.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_help, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterHelp.ViewHolder viewHolder, int position) {
        viewHolder.title.setText(helpList.get(position).getTitle());
        viewHolder.text.setText(helpList.get(position).getText());

        viewHolder.img.setLayoutParams(helpList.get(position).getParams());
        viewHolder.img.setImageResource(helpList.get(position).getIdImg());
    }

    @Override
    public boolean onFailedToRecycleView(ViewHolder holder) {
        return true;
    }

    @Override
    public int getItemCount() {
        return helpList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private GifImageView img;
        private TextView text;
        private TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            this.title = itemView.findViewById(R.id.title);
            this.text = itemView.findViewById(R.id.text);
            this.img = itemView.findViewById(R.id.img);
        }
    }
}
