package Model;

/**
 * Created by eminem on 04.03.2018.
 */

public class ReysSchedule {
    private String nameReys;
    private String typeTransport;
    private String number;
    private String uid;
    private Integer typeTransportImg;

    public ReysSchedule(String nameReys, String typeTransport, String number, String uid, Integer typeTransportImg) {
        this.nameReys = nameReys;
        this.typeTransport = typeTransport;
        this.number = number;
        this.uid = uid;
        this.typeTransportImg = typeTransportImg;
    }

    public String getNameReys() {
        return nameReys;
    }

    public void setNameReys(String nameReys) {
        this.nameReys = nameReys;
    }

    public String getTypeTransport() {
        return typeTransport;
    }

    public void setTypeTransport(String typeTransport) {
        this.typeTransport = typeTransport;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getTypeTransportImg() {
        return typeTransportImg;
    }

    public void setTypeTransportImg(Integer typeTransportImg) {
        this.typeTransportImg = typeTransportImg;
    }
}
