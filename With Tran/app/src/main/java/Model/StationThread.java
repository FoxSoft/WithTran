package Model;

/**
 * Created by eminem on 01.03.2018.
 */

public class StationThread {
    private String nameStation;
    private String typeStation;
    private String codeStation;
    private String status;
    private String colorBackground = "#ff7371";

    public String getColorBackground() {
        return colorBackground;
    }

    public void setColorBackground(String colorBackground) {
        this.colorBackground = colorBackground;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StationThread(String nameStation, String typeStation) {
        this.nameStation = nameStation;
        this.typeStation = typeStation;
    }

    public StationThread(String nameStation, String typeStation, String codeStation) {
        this.nameStation = nameStation;
        this.typeStation = typeStation;
        this.codeStation = codeStation;
    }

    public String getNameStation() {
        return nameStation;
    }

    public void setNameStation(String nameStation) {
        this.nameStation = nameStation;
    }

    public String getTypeStation() {
        return typeStation;
    }

    public void setTypeStation(String typeStation) {
        this.typeStation = typeStation;
    }

    public String getCodeStation() {
        return codeStation;
    }

    public void setCodeStation(String codeStation) {
        this.codeStation = codeStation;
    }
}
