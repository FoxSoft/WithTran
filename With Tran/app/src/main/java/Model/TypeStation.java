package Model;

import com.app.nosenko.withtran.Yandex;

/**
 * Created by eminem on 17.03.2018.
 */

public class TypeStation {
    private String title;
    private int img;
    private int id;
    private Yandex.STATION_TYPE stationType = null;

    public TypeStation(String title, int img, Yandex.STATION_TYPE stationType, int id) {
        this.title = title;
        this.img = img;
        this.stationType = stationType;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Yandex.STATION_TYPE getStationType() {
        return stationType;
    }

    public void setStationType(Yandex.STATION_TYPE stationType) {
        this.stationType = stationType;
    }
}
