package Model;

/**
 * Created by eminem on 12.11.2017.
 */

public class GroupItem {
    private String nameReys;
    private String count;
    private String typeTransport;
    private int idImg;
    private int id;

    public GroupItem() {
    }

    public GroupItem(String nameReys, String count, String typeTransport, int idImg, int id) {

        this.nameReys = nameReys;
        this.count = count;
        this.typeTransport = typeTransport;
        this.idImg = idImg;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getIdImg() {
        return idImg;
    }

    public void setIdImg(int idImg) {
        this.idImg = idImg;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameReys() {
        return nameReys;
    }

    public void setNameReys(String nameReys) {
        this.nameReys = nameReys;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTypeTransport() {
        return typeTransport;
    }

    public void setTypeTransport(String typeTransport) {
        this.typeTransport = typeTransport;
    }
}
