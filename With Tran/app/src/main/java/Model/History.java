package Model;

public class History {
    private String nameReys;
    private String fromStation;
    private String toStation;
    private String fromCodeStation;
    private String toCodeStation;
    private String nameReysOriginal;

    public History() {
    }

    public History(String nameReys, String fromStation, String toStation, String fromCodeStation, String toCodeStation) {
        this.nameReys = nameReys;
        this.fromStation = fromStation;
        this.toStation = toStation;
        this.fromCodeStation = fromCodeStation;
        this.toCodeStation = toCodeStation;
    }

    public String getNameReysOriginal() {
        return nameReysOriginal;
    }

    public void setNameReysOriginal(String nameReysOriginal) {
        this.nameReysOriginal = nameReysOriginal;
    }

    public String getNameReys() {
        return nameReys;
    }

    public void setNameReys(String nameReys) {
        this.nameReys = nameReys;
    }

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    public String getFromCodeStation() {
        return fromCodeStation;
    }

    public void setFromCodeStation(String fromCodeStation) {
        this.fromCodeStation = fromCodeStation;
    }

    public String getToCodeStation() {
        return toCodeStation;
    }

    public void setToCodeStation(String toCodeStation) {
        this.toCodeStation = toCodeStation;
    }
}