package Model;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by eminem on 17.03.2018.
 */

public class Help {
    private Integer idImg;
    private String text;
    private String title;
    private LinearLayout.LayoutParams params;

    public Help(String title, String text, Integer idImg, LinearLayout.LayoutParams params) {
        this.idImg = idImg;
        this.text = text;
        this.title = title;
        this.params = params;
    }

    public LinearLayout.LayoutParams getParams() {
        return params;
    }

    public void setParams(LinearLayout.LayoutParams params) {
        this.params = params;
    }

    public Integer getIdImg() {
        return idImg;
    }

    public void setIdImg(Integer idImg) {
        this.idImg = idImg;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
